﻿#include "histogram.h"

histogram::histogram(int type, QCustomPlot *customPlot0, QWidget *parent):
    QWidget(parent)
{
    customPlot = customPlot0;
    binSettedFlag = false;
    totalHits = 0;
    signalHits = 0;
    TYPE = type;
    hist = new TH1F();
    hist->SetTitle("fit");
    hist->SetName("fit");
    fit = new TF1();
    totalEventNumber = 0;
    triggeredEventNumber = 0;
    EffBinNO = 120;
    EffShowNO = 100;
}
void histogram::init(double st, double ed, double binwidth, size_t layerno)
{
    layerNO = layerno;
    binGap = binwidth;
    startPoint = st;
    endPoint = ed;
    binNumber = (endPoint - startPoint)/binGap + 1;
    x1.resize(binNumber);y1.resize(binNumber);
    x2.resize(binNumber);y2.resize(binNumber);
    y3.resize(binNumber);  //For Channel Ref Efficiency
    for (int i = 0; i < binNumber; i ++)
    {
        x1[i] = startPoint + binGap * i;
        x2[i] = startPoint + binGap * i;
        y1[i] = 0;
        y2[i] = 0;
    }
    endPoint += binGap/2.0;
    startPoint -= binGap/2.0;

    // initial title
    customPlot->plotLayout()->insertRow(0);
    title = new QCPTextElement(customPlot, " ");
    customPlot->plotLayout()->addElement(0, 0, title);
    //title->setAutoMargins(QCP::msNone);
    //title->setMargins(QMargins(0,0,0,0));
    customPlot->xAxis->setLabelPadding(0);
    customPlot->xAxis->setTickLabelPadding(0);

    // create and configure plottables:
    bars1 = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    bars1->setWidth(binGap);
    bars1->setData(x1, y1);
    bars1->setPen(Qt::NoPen);
    bars1->setBrush(QColor(10, 140, 70, 160));


    if(TYPE == TYPE_LE || TYPE == TYPE_TOT || TYPE == TYPE_PTriLE || TYPE == TYPE_PDouLE)
    {
        graph1 = customPlot->addGraph();
        graph1->setData(x2, y2);
        graph1->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));
        graph1->setPen(QPen(QColor(120, 120, 120), 2));
    }


    // move bars above graphs and grid below bars:
    customPlot->addLayer("abovemain", customPlot->layer("main"), QCustomPlot::limAbove);
    customPlot->addLayer("belowmain", customPlot->layer("main"), QCustomPlot::limBelow);
    if(TYPE == TYPE_EFF)
    {
        customPlot->xAxis->setLabel("Event Number");
        customPlot->yAxis->setLabel("Trigger Efficiency [%]");
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(100.0);
    }
    // set some pens, brushes and backgrounds:
    customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->replot();

    binSettedFlag = true;
}
void histogram::init(double st, double ed, double binwidth, size_t layerno, bool PhiorEta)
{
    PHIorETA = PhiorEta;
    layerNO = layerno;
    binGap = binwidth;
    startPoint = st;
    endPoint = ed;
    binNumber = (endPoint - startPoint)/binGap + 1;
    x1.resize(binNumber);y1.resize(binNumber);
    x2.resize(binNumber);y2.resize(binNumber);
    y3.resize(binNumber);  //For Channel Ref Efficiency
    for (int i = 0; i < binNumber; i ++)
    {
        x1[i] = startPoint + binGap * i;
        x2[i] = startPoint + binGap * i;
        y1[i] = 0;
        y2[i] = 0;
    }
    endPoint += binGap/2.0;
    startPoint -= binGap/2.0;

    // initial title
    customPlot->plotLayout()->insertRow(0);
    title = new QCPTextElement(customPlot, " ");
    customPlot->plotLayout()->addElement(0, 0, title);

    // create and configure plottables:
    bars1 = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    bars1->setWidth(binGap);
    bars1->setData(x1, y1);
    bars1->setPen(Qt::NoPen);
    bars1->setBrush(QColor(10, 140, 70, 160));


    if(TYPE == TYPE_LE || TYPE == TYPE_TOT || TYPE == TYPE_PTriLE || TYPE == TYPE_PDouLE)
    {
        graph1 = customPlot->addGraph();
        graph1->setData(x2, y2);
        graph1->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross, QPen(Qt::black, 1.5), QBrush(Qt::white), 9));
        graph1->setPen(QPen(QColor(120, 120, 120), 2));
    }


    // move bars above graphs and grid below bars:
    customPlot->addLayer("abovemain", customPlot->layer("main"), QCustomPlot::limAbove);
    customPlot->addLayer("belowmain", customPlot->layer("main"), QCustomPlot::limBelow);
    if(TYPE == TYPE_LE)
        graph1->setLayer("abovemain");
    customPlot->xAxis->grid()->setLayer("belowmain");
    customPlot->yAxis->grid()->setLayer("belowmain");

    // set axis labels
    if(TYPE == TYPE_LE)
    {
        if(PHIorETA == TYPE_PHI)
            customPlot->xAxis->setLabel("Phi LeadingEdge [ns]");
        else
            customPlot->xAxis->setLabel("Eta LeadingEdge [ns]");
        customPlot->yAxis->setLabel("Entries");
        setTitle();
    }
    else if(TYPE == TYPE_FE)
    {
        if(PHIorETA == TYPE_PHI)
            customPlot->xAxis->setLabel("Phi FallingEdge [ns]");
        else
            customPlot->xAxis->setLabel("Eta FallingEdge [ns]");
        customPlot->yAxis->setLabel("Entries");
    }
    else if(TYPE == TYPE_TOT)
    {
        if(PHIorETA == TYPE_PHI)
            customPlot->xAxis->setLabel("Phi TOT distribution [ns]");
        else
            customPlot->xAxis->setLabel("Eta TOT distribution [ns]");
        customPlot->yAxis->setLabel("Entries");
    }
    else if(TYPE == TYPE_CS)
    {
        if(PHIorETA == TYPE_PHI)
            customPlot->xAxis->setLabel("Phi Cluster Size");
        else
            customPlot->xAxis->setLabel("Eta Cluster Size");
        customPlot->yAxis->setLabel("Entries");
    }
    else if(TYPE == TYPE_EFF)
    {
        customPlot->xAxis->setLabel("Event Number");
        if(PHIorETA == TYPE_ETA){
            customPlot->yAxis->setLabel("Eta Efficiency [%]");
        }
        else{
            customPlot->yAxis->setLabel("Phi Efficiency [%]");
        }
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(100.0);
    }
    else if(TYPE == TYPE_PEFF)
    {
        customPlot->xAxis->setLabel("Event Number");
        if(PhiorEta == TYPE_PHI)
            customPlot->yAxis->setLabel("Phi Tripple & Efficiency [%]");
        else
            customPlot->yAxis->setLabel("Eta Tripple & Efficiency [%]");
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(100.0);
    }
    else if(TYPE == TYPE_PTriLE)
    {
        customPlot->yAxis->setLabel("Entries");
        if(PhiorEta == TYPE_PHI)
            customPlot->xAxis->setLabel("inter-RPCs Tri-AND: Phi LE distribution [ns]");
        else
            customPlot->xAxis->setLabel("inter-RPCs Tri-AND: Eta LE distribution [ns]");
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(100.0);
    }
    else if(TYPE == TYPE_PDouLE)
    {
        customPlot->yAxis->setLabel("Entries");
        if(PhiorEta == TYPE_PHI)
            customPlot->xAxis->setLabel("inter-RPCs Dou-AND: Phi LE distribution [ns]");
        else
            customPlot->xAxis->setLabel("inter-RPCs Dou-AND: Eta LE distribution [ns]");
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(100.0);
    }
    else if(TYPE == TYPE_Hit)
    {
        customPlot->yAxis->setLabel("Entries");
        if(PhiorEta == TYPE_PHI)
            customPlot->xAxis->setLabel("Phi Channel Hit");
        else
            customPlot->xAxis->setLabel("Eta Channel Hit");
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(5.0);
    }
    else if(TYPE == TYPE_ChRefEff){
        graph1 = customPlot->addGraph();
        errorBars = new QCPErrorBars(customPlot->xAxis, customPlot->yAxis);
        errorBars->setDataPlottable(bars1);
        errorBars->setPen(QPen(Qt::black));
        error.resize(binNumber);
        for (int i = 0; i < binNumber; i ++){
            x2[i] = 0;
            y2[i] = 0;
            error[i] = 0;
        }
        customPlot->yAxis->setLabel("Reference Efficiency [%]");
        if(PhiorEta == TYPE_PHI)
            customPlot->xAxis->setLabel("Phi Channels Reference Efficiency");
        else
            customPlot->xAxis->setLabel("Eta Channels Reference Efficiency");
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(100.0);
        errorBars->setData(error);
    }
    else if(TYPE == TYPE_ChTOT)
    {
        graph1 = customPlot->addGraph();
        errorBars = new QCPErrorBars(customPlot->xAxis, customPlot->yAxis);
        errorBars->setDataPlottable(bars1);
        errorBars->setPen(QPen(Qt::black));
        error.resize(binNumber);
        for (int i = 0; i < binNumber; i ++){
            error[i] = 0;
        }
        customPlot->yAxis->setLabel("Average TOT [ns]");
        if(PhiorEta == TYPE_PHI){
            customPlot->xAxis->setLabel("Average TOT: Phi Channels");
            stat.resize(binNumber);
        }
        else{
            customPlot->xAxis->setLabel("Average TOT: Eta Channels");
            stat.resize(binNumber);
        }
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(40.0);
        errorBars->setData(error);
    }
    else if(TYPE == TYPE_ChTOF)
    {
        graph1 = customPlot->addGraph();
        errorBars = new QCPErrorBars(customPlot->xAxis, customPlot->yAxis);
        errorBars->setDataPlottable(bars1);
        errorBars->setPen(QPen(Qt::black));
        error.resize(binNumber);
        for (int i = 0; i < binNumber; i ++){
            error[i] = 0;
        }
        customPlot->yAxis->setLabel("Average TOF [ns]");
        y2 = {-20};
        if(PhiorEta == TYPE_PHI){
            customPlot->xAxis->setLabel("Average TOF: Phi Channels");
            stat.resize(binNumber);
        }
        else{
            customPlot->xAxis->setLabel("Average TOF: Eta Channels");
            stat.resize(binNumber);
        }
        customPlot->yAxis->setRangeLower(-10.0);
        customPlot->yAxis->setRangeUpper(10.0);
        errorBars->setData(error);
    }
    // set some pens, brushes and backgrounds:
    customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->replot();

    binSettedFlag = true;
}
void histogram::init(double st, double ed, double binwidth, size_t layerno, bool PhiorEta, size_t totallayerno)
{
    totalLayerNO = totallayerno;
    PHIorETA = PhiorEta;
    layerNO = layerno;
    binGap = binwidth;
    startPoint = st;
    endPoint = ed;
    binNumber = (endPoint - startPoint)/binGap + 1;
    x1.resize(binNumber);y1.resize(binNumber);
    x2.resize(binNumber);y2.resize(binNumber);
    y3.resize(binNumber);  //For Channel Ref Efficiency
    for (int i = 0; i < binNumber; i ++)
    {
        x1[i] = startPoint + binGap * i;
        x2[i] = startPoint + binGap * i;
        y1[i] = 0;
        y2[i] = 0;
    }
    endPoint += binGap/2.0;
    startPoint -= binGap/2.0;

    // initial title
    customPlot->plotLayout()->insertRow(0);
    title = new QCPTextElement(customPlot, " ");
    customPlot->plotLayout()->addElement(0, 0, title);

    // create and configure plottables:
    bars1 = new QCPBars(customPlot->xAxis, customPlot->yAxis);
    bars1->setWidth(binGap);
    bars1->setData(x1, y1);
    bars1->setPen(Qt::NoPen);
    bars1->setBrush(QColor(10, 140, 70, 160));

    // move bars above graphs and grid below bars:
    customPlot->addLayer("abovemain", customPlot->layer("main"), QCustomPlot::limAbove);
    customPlot->addLayer("belowmain", customPlot->layer("main"), QCustomPlot::limBelow);
    customPlot->xAxis->grid()->setLayer("belowmain");
    customPlot->yAxis->grid()->setLayer("belowmain");

    // set axis labels
   if(TYPE == TYPE_RefEFF)
    {
        customPlot->xAxis->setLabel("Event Number");
        QString label;
        if(PHIorETA == TYPE_ETA){
            label.append("Eta");
        }
        else{
            label.append("Phi");
        }
        label.append(" Efficiency under L");
        label.append(QString::number((layerno+1)%totalLayerNO));
        label.append(" [%]");
        customPlot->yAxis->setLabel(label);
        customPlot->yAxis->setRangeLower(0.0);
        customPlot->yAxis->setRangeUpper(100.0);
    }
    // set some pens, brushes and backgrounds:
    customPlot->xAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setPen(QPen(QColor(140, 140, 140), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->yAxis->grid()->setSubGridPen(QPen(QColor(80, 80, 80), 1, Qt::DotLine));
    customPlot->xAxis->grid()->setSubGridVisible(true);
    customPlot->yAxis->grid()->setSubGridVisible(true);
    customPlot->replot();

    binSettedFlag = true;
}
void histogram::clearDisplay()
{
    totalHits = 0;
    signalHits = 0;
    totalEventNumber = 0;
    triggeredEventNumber = 0;
    if( binSettedFlag == true){
        x1.resize(binNumber);y1.resize(binNumber);
        x2.resize(binNumber);y2.resize(binNumber);
        y3.resize(binNumber);  //For Channel Ref Efficiency
        for (int i = 0; i < binNumber; i ++)
        {
            x1[i] = startPoint + binGap * i;
            x2[i] = startPoint + binGap * i;
            y1[i] = 0;
            y2[i] = 0;
            y3[i] = 0;
        }

        if(TYPE == TYPE_LE){
            bars1->setData(x1, y1);
            customPlot->rescaleAxes();
        }
        else if(TYPE == TYPE_FE)
        {
            bars1->setData(x1, y1);
            customPlot->rescaleAxes();
        }
        else if(TYPE == TYPE_TOT)
        {
            bars1->setData(x1, y1);
            customPlot->rescaleAxes();
        }
        else if(TYPE == TYPE_CS)
        {
            bars1->setData(x1, y1);
            customPlot->rescaleAxes();
            setTitle(0);
        }
        else if(TYPE == TYPE_EFF)
        {
            bars1->setData(x1, y1);
            setEffTitle(0);
            customPlot->rescaleAxes();
            customPlot->yAxis->setRangeLower(0.0);
            customPlot->yAxis->setRangeUpper(100.0);
        }
        else if(TYPE == TYPE_PEFF)
        {
            bars1->setData(x1, y1);
            setEffTitle(0);
            customPlot->rescaleAxes();
            customPlot->yAxis->setRangeLower(0.0);
            customPlot->yAxis->setRangeUpper(100.0);
        }
        else if(TYPE == TYPE_PTriLE)
        {
            bars1->setData(x1, y1);
            graph1->setData(x2, y2);
            setTitle(0, 0);
            customPlot->rescaleAxes();
        }
        else if(TYPE == TYPE_PDouLE)
        {
            bars1->setData(x1, y1);
            graph1->setData(x2, y2);
            setTitle(0, 0);
            customPlot->rescaleAxes();
        }
        else if(TYPE == TYPE_RefEFF)
        {
            bars1->setData(x1, y1);
            setTitle(0);
            customPlot->rescaleAxes();
        }
        else if(TYPE == TYPE_Hit)
        {
            bars1->setData(x1, y1);
            customPlot->rescaleAxes();
        }
        else if(TYPE == TYPE_ChRefEff)
        {
            bars1->setData(x1, y1);
            for(int i = 0; i < error.size();i++){
                error[i] = 0;
            }
            errorBars->setData(error);
            customPlot->rescaleAxes();
            customPlot->yAxis->setRangeUpper(100.0);
        }
        else if(TYPE == TYPE_ChTOT)
        {
            bars1->setData(x1, y1);
            for(int i = 0; i < error.size();i++){
                error[i] = 0;
            }
            errorBars->setData(error);
            customPlot->rescaleAxes();
        }
        else if(TYPE == TYPE_ChTOF)
        {
            bars1->setData(x1, y1);
            for(int i = 0; i < error.size();i++){
                error[i] = 0;
            }
            errorBars->setData(error);
            customPlot->rescaleAxes();
        }

        customPlot->replot();
    }
}
void histogram::refreshLE(PROCESSEDData *Data, int EventStartNo, int EventEndNo)
{
    if (binSettedFlag)
    {
        for (Uint i = EventStartNo; i < EventEndNo; i++)
        {
            totalHits += 1;
            double temp;
            if(PHIorETA == false)
                temp =double(Data->PhiSignalLE[layerNO]->at(i));
            else
                temp =double(Data->EtaSignalLE[layerNO]->at(i));
            if(temp >= startPoint && temp < endPoint)
            {
                int No = int((temp - startPoint)/binGap);
                y1[No] += 1.0;
                signalHits += 1;
            }
        }
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }

    else
        qDebug() << "Bins haven't been set";
}
void histogram::refreshFE(PROCESSEDData *Data, int EventStartNo, int EventEndNo)
{
    if (binSettedFlag)
    {
        for (Uint i = EventStartNo; i < EventEndNo; i++)
        {
            totalHits += 1;
            double temp;
            if(PHIorETA == false)
                temp =double(Data->PhiSignalFE[layerNO]->at(i));
            else
                temp =double(Data->EtaSignalFE[layerNO]->at(i));
            if(temp >= startPoint && temp < endPoint)
            {
                int No = int((temp - startPoint)/binGap);
                y1[No] += 1.0;
                signalHits += 1;
            }
        }
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }

    else
        qDebug() << "Bins haven't been set";
}

void histogram::refreshPerfDouLE(PROCESSEDData *Data, int EventStartNo, int EventEndNo)
{
    if (binSettedFlag)
    {
        for (int i = EventStartNo; i < EventEndNo; i++)
        {
            float temp = 1.0;
            if(singleTriggered(Data, i, 0)&&singleTriggered(Data, i, 1))
            {
                totalHits += 1;
                {
                    temp =DoubletLE(Data, i, 0, 1);
                    if(temp >= startPoint && temp < endPoint)
                    {
                        int No = int((temp - startPoint)/binGap);
                        if(No > 0 && No < binNumber){
                            y1[No] += 1.0;
                            signalHits += 1;
                        }
                        //if(temp >= 0.2 && temp <= 0.4)
                            //qDebug() << "0204EntryNO =" << Data->EventNo->at(i);
                    }
                }
            }

        }
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();

        if(signalHits > 200)
        {
            hist->Reset();
            hist->SetBins(binNumber, double(startPoint), double(endPoint));
            for(int i = 1; i < binNumber; i++)
            {
                hist->AddBinContent(i, y1[i-1]);
            }
            hist->Fit("gaus", "RQ", "", double(startPoint), double(endPoint));
            fit = hist->GetFunction("gaus");
            double p0 = fit->GetParameter(0);
            double p1 = fit->GetParameter(1);
            double p2 = fit->GetParameter(2);
            for (int i = 0; i < binNumber; i ++)
            {

                y2[i] = p0*exp(-0.5*((x2[i]-p1)/p2)*((x2[i]-p1)/p2));
            }
            graph1->setData(x2, y2);
            setTitle(p1, p2);
        }
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }

    else
        qDebug() << "Bins haven't been set";
}
void histogram::refreshPerfTriLE(PROCESSEDData *Data, int EventStartNo, int EventEndNo)
{
    if (binSettedFlag)
    {
        for (int i = EventStartNo; i < EventEndNo; i++)
        {
            totalHits += 1;
            float temp = 0;
            if(trippleTriggered(Data, i) == true)
            {
                temp =TripletLE(Data, i);
                if(temp >= startPoint && temp < endPoint)
                {
                    int No = int((temp - startPoint)/binGap);
                    if(No > 0 && No < binNumber){
                        y1[No] += 1.0;
                        signalHits += 1;
                    }
                }
            }

        }
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();

        if(signalHits > 200)
        {
            hist->Reset();
            hist->SetBins(binNumber, double(startPoint), double(endPoint));
            for(int i = 0; i < binNumber; i++)
            {
                hist->AddBinContent(i, y1[i-1]);
            }
            hist->Fit("gaus", "RQ", "", double(startPoint), double(endPoint));
            fit = hist->GetFunction("gaus");
            double p0 = fit->GetParameter(0);
            double p1 = fit->GetParameter(1);
            double p2 = fit->GetParameter(2);
            for (int i = 0; i < binNumber; i ++)
            {

                y2[i] = p0*exp(-0.5*((x2[i]-p1)/p2)*((x2[i]-p1)/p2));
            }
            graph1->setData(x2, y2);
            setTitle(p1, p2);
        }
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }

    else
        qDebug() << "Bins haven't been set";
}
void histogram::refreshTriEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //The function used to refresh each singlet
{
    EffBinNO = binNumber;
    EffShowNO = ((EventEndNo - EventStartNo) + totalEventNumber) % EffBinNO;
    if (EffShowNO == 0)
        EffShowNO = EffBinNO;
    int carryNO = int((float(EventEndNo - EventStartNo + totalEventNumber - 1))/float(EffBinNO));
    bool carry = carryNO > int(float(totalEventNumber)/float(EffBinNO));
    if(totalEventNumber%EffBinNO == 0 && totalEventNumber > 0){
        carry = true;
    }
    if(carry)
    {
        for(int i = 0; i < binNumber; i++)
        {
            y1[i] = 0;
            x1[i] = i + EffBinNO * carryNO + 1;
        }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            if (Data->PhiSignalCh[layerNO]->at(i) >= 0 && Data->EtaSignalCh[layerNO]->at(i) >= 0)
                triggeredEventNumber +=1;
            float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
            y1[totalEventNumber%EffBinNO] = tempEff * 100.0;
            y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
            totalEventNumber ++;
        }
    }
    else
    {
        if(y1[EffBinNO - 1] >= 0.001)  // to handle the overlap between loop
            for(int i = 0; i < binNumber; i++)
            {
                y1[i] = 0;
            }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            if (Data->PhiSignalCh[layerNO]->at(i) >= 0 && Data->EtaSignalCh[layerNO]->at(i) >= 0)
                triggeredEventNumber +=1;
            float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
            y1[totalEventNumber%EffBinNO] = tempEff * 100.0;
            y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
            totalEventNumber ++;
        }
    }
    bars1->setData(x1, y1);
    setTriEffTitle(y1[EffShowNO - 1]);
    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0.0);
    customPlot->yAxis->setRangeUpper(100.0);
    customPlot->replot(QCustomPlot::rpQueuedReplot);
}
void histogram::refreshEtaOrPhiEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //The function used to refresh each singlet
{
    EffBinNO = binNumber;
    EffShowNO = ((EventEndNo - EventStartNo) + totalEventNumber) % EffBinNO;
    if (EffShowNO == 0)
        EffShowNO = EffBinNO;
    int carryNO = int((float(EventEndNo - EventStartNo + totalEventNumber - 1))/float(EffBinNO));
    bool carry = carryNO > int((totalEventNumber)/(EffBinNO));
    if(totalEventNumber%EffBinNO == 0 && totalEventNumber > 0){
        carry = true;
    }
    if(carry)
    {
        for(int i = 0; i < binNumber; i++)
        {
            y1[i] = 0;
            x1[i] = i + EffBinNO * carryNO + 1;
        }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            if(PHIorETA == TYPE_ETA)
            {
                if (Data->EtaClusterSize[layerNO]->at(i) >= 1){
                    triggeredEventNumber +=1;
                }
            }
            else if(PHIorETA == TYPE_PHI)
            {
                if (Data->PhiClusterSize[layerNO]->at(i) >= 1){
                    triggeredEventNumber +=1;
                }
            }
            if((EventEndNo - i) <= EffShowNO){
                float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
                y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
            }
            totalEventNumber ++;
        }
    }
    else
    {
        if(y1[EffBinNO - 1] >= 0.001)  // to handle the overlap between loop
            for(int i = 0; i < binNumber; i++)
            {
                y1[i] = 0;
            }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            if(PHIorETA == TYPE_ETA)
            {
                if (Data->EtaClusterSize[layerNO]->at(i) >= 1){
                    triggeredEventNumber +=1;
                }
            }
            else if(PHIorETA == TYPE_PHI)
            {
                if (Data->PhiClusterSize[layerNO]->at(i) >= 1){
                    triggeredEventNumber +=1;
                }
            }
            float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
            y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
            y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
            totalEventNumber ++;
        }
    }
    bars1->setData(x1, y1);
    setEffTitle(y1[EffShowNO - 1]);
    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0.0);
    customPlot->yAxis->setRangeUpper(100.0);
    customPlot->replot(QCustomPlot::rpQueuedReplot);
}

void histogram::refreshEtaOrPhiHit(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //The function used to refresh each singlet
{
    if(binSettedFlag){
        for(ulong i = uint(EventStartNo); i < uint(EventEndNo); i++){
            std::map<size_t, size_t>::iterator it;
            ulong jmax = 0;
            if(PHIorETA == TYPE_PHI){
                jmax = Data->LayerList[layerNO]->PhiLECh->at(i).size();
            }
            else{
                jmax = Data->LayerList[layerNO]->EtaLECh->at(i).size();
            }
            for(uint j = 0; j < jmax;j++){
                if(PHIorETA == TYPE_PHI){
                    it = PHISTRIPMAP.at(layerNO).find(Data->LayerList[layerNO]->PhiLECh->at(i).at(j));
                }
                else{
                    it = ETASTRIPMAP.at(layerNO).find(Data->LayerList[layerNO]->EtaLECh->at(i).at(j));
                }
                if(it != ETASTRIPMAP.at(layerNO).end() && it != PHISTRIPMAP.at(layerNO).end()){
                    ulong ch = (*it).first;
                    if(PHIorETA == TYPE_PHI){
                        ch-=(PHISTARTSTRIPNO-1);
                    }
                    else{
                        ch-=(ETASTARTSTRIPNO-1);
                    }
                    y1[5*ch] += 1.0;
                    y1[5*ch+1] += 1.0;
                    y1[5*ch+2] += 1.0;
                }
            }
        }
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
    else{
        qDebug()<<"HitMap Bin not set";
    }
}

void histogram::refreshEtaOrPhiChTOT(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //The function used to refresh TOT of each singlet
{
    if(binSettedFlag){
        for(ulong i = uint(EventStartNo); i < uint(EventEndNo); i++){
            double temp = 20;
            int ch = -20;
            if(PHIorETA == TYPE_PHI){
                temp = (Data->PhiSignalFE[layerNO]->at(i)) - (Data->PhiSignalLE[layerNO]->at(i));
                ch = Data->PhiSignalCh[layerNO]->at(i) - (PHISTARTSTRIPNO-1);
            }
            else{
                temp = (Data->EtaSignalFE[layerNO]->at(i)) - (Data->EtaSignalLE[layerNO]->at(i));
                ch = Data->EtaSignalCh[layerNO]->at(i) - (ETASTARTSTRIPNO-1);
            }
            if(temp >= 0 && temp <= 100 && ch > -1)
            {
                y3[ch] += 1;
                y2[ch] += temp;
                stat[ch].push_back(temp);
                float avg = float(y2[ch])/float(y3[ch]);
                y1[5*ch] = avg;
                y1[5*ch+1] = avg;
                y1[5*ch+2] = avg;
            }
        }
        //Calculating the variance
        int stripno =(PHIorETA==TYPE_PHI? PHISTRIPNOS:ETASTRIPNOS);
        for(int i = 0; i < stripno; i++){
            float var = 0;
            for(int j = 0; j < stat.at(i).size(); j++){
                var += (y1[5*i] - stat.at(i).at(j))*(y1[5*i] - stat.at(i).at(j));
            }
            error[5*i+1]=sqrt(var/y3[i]);
        }
        errorBars->setData(error);

        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
    else{
        qDebug()<<"HitMap Bin not set";
    }
}

void histogram::refreshEtaOrPhiChTOF(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //The function used to refresh TOT of each singlet
{
    if(binSettedFlag){
        for(ulong i = uint(EventStartNo); i < uint(EventEndNo); i++){
            double temp = 20;
            int ch = -1;
            int chDist = 100;
            if(singleTriggered(Data, i, layerNO)&&singleTriggered(Data, i, LAYERREFERENCE[layerNO])){
                temp = DoubletLE(Data, i, layerNO, LAYERREFERENCE[layerNO]);
                if(PHIorETA == TYPE_PHI){
                    ch = Data->PhiSignalCh[layerNO]->at(i) - (PHISTARTSTRIPNO-1);
                    chDist = std::abs(Data->PhiSignalCh[layerNO]->at(i) - Data->PhiSignalCh[LAYERREFERENCE[layerNO]]->at(i));
                }
                else{
                    ch = Data->EtaSignalCh[layerNO]->at(i) - (ETASTARTSTRIPNO-1);
                    chDist = std::abs(Data->EtaSignalCh[layerNO]->at(i) - Data->EtaSignalCh[LAYERREFERENCE[layerNO]]->at(i));
                }

            }
            if(temp >= -10 && temp <= 10 && ch > -1 && chDist < 2)
            {
                if(y2[ch]<-19){
                    y2[ch]=0;
                }
                y3[ch] += 1;
                y2[ch] += temp;
                stat[ch].push_back(temp);
                float avg = float(y2[ch])/float(y3[ch]);
                y1[5*ch] = avg;
                y1[5*ch+1] = avg;
                y1[5*ch+2] = avg;
            }
        }
        //Calculating the variance
        int stripno =(PHIorETA==TYPE_PHI? PHISTRIPNOS:ETASTRIPNOS);
        for(int i = 0; i < stripno; i++){
            float var = 0;
            for(int j = 0; j < stat.at(i).size(); j++){
                var += (y1[5*i] - stat.at(i).at(j))*(y1[5*i] - stat.at(i).at(j));
            }
            error[5*i+1]=sqrt(var/y3[i]);
        }
        errorBars->setData(error);

        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
    else{
        qDebug()<<"HitMap Bin not set";
    }
}

void histogram::refreshEtaOrPhiChannelRefEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //
{
    if(binSettedFlag){
        for(ulong i = uint(EventStartNo); i < uint(EventEndNo); i++){
            std::map<size_t, size_t>::iterator it;
            int deltaCh = 0;
            int refCh = 0;
            int Ch = 0;
            float deltaLE = 0;
            float refLE = 0;
            float LE = 0;
            if(PHIorETA == TYPE_PHI){
                refCh = Data->PhiSignalCh[LAYERREFERENCE[layerNO]]->at(i) - int(PHISTARTSTRIPNO) + 1;
                refLE = Data->PhiSignalLE[LAYERREFERENCE[layerNO]]->at(i);
                Ch = Data->PhiSignalCh[layerNO]->at(i) - int(PHISTARTSTRIPNO) + 1;
                LE = Data->PhiSignalLE[layerNO]->at(i);
                deltaCh = refCh - Ch;
                deltaLE = refLE - LE;
            }
            else{
                refCh = Data->EtaSignalCh[LAYERREFERENCE[layerNO]]->at(i) - int(ETASTARTSTRIPNO) + 1;
                refLE = Data->EtaSignalLE[LAYERREFERENCE[layerNO]]->at(i);
                Ch = Data->EtaSignalCh[layerNO]->at(i) - int(ETASTARTSTRIPNO) + 1;
                LE = Data->EtaSignalLE[layerNO]->at(i);
                deltaCh = refCh - Ch;
                deltaLE = refLE - LE;
            }
            if(Ch > 0 && refCh > 0 && refLE < LeadingEdgeEd && refLE > LeadginEdgeSt){
                y3[Ch] ++; //Using y3 to store demominator
                if(abs(deltaCh) <= 2 && abs(deltaLE) <= 10.0){
                    y2[Ch] ++; //Using y2 to store numerator
                }
                double eff = float(y2[Ch]) / float(y3[Ch]);
                y1[5*Ch] = eff*100;
                y1[5*Ch+1] = eff*100;
                y1[5*Ch+2] = eff*100;
                error[5*Ch+1]= sqrt(eff*(1-eff)/double(y3[Ch]))*100.0;
            }
        }
        errorBars->setData(error);
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->yAxis->setRangeUpper(100.0);
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
    else{
        qDebug()<<"HitMap Bin not set";
    }
}

void histogram::refreshEtaOrPhiRefEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //The function used to refresh each singlet
{
    float latencyConstraint = 10;
    int refLayerNO = LAYERREFERENCE[layerNO];
    EffBinNO = binNumber;
    int refTriggeredEvents = 0;
    for(int i = EventStartNo; i < EventEndNo; i++){
        if(PHIorETA == TYPE_ETA){
            if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1){
                refTriggeredEvents++;
            }
        }
        else if(PHIorETA == TYPE_PHI){
            if(Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
                refTriggeredEvents++;
            }
        }
    }
    EffShowNO = (refTriggeredEvents + totalEventNumber) % EffBinNO;
    int localCounts = 0;
    if (EffShowNO == 0)
        EffShowNO = EffBinNO;
    int carryNO = int((float(refTriggeredEvents + totalEventNumber - 1))/float(EffBinNO));
    bool carry = carryNO > int((totalEventNumber)/(EffBinNO));
    if(totalEventNumber%EffBinNO == 0 && totalEventNumber > 0){
        carry = true;
    }
    if(carry)
    {
        for(int i = 0; i < binNumber; i++)
        {
            y1[i] = 0;
            x1[i] = i + EffBinNO * carryNO + 1;
        }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            if(PHIorETA == TYPE_ETA)            {
                if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1){
                    localCounts ++;
                    if (Data->EtaClusterSize[layerNO]->at(i) >= 1){
                        int refCh = Data->EtaSignalCh[refLayerNO]->at(i);
                        for(int targetCh = refCh - 1; targetCh <= refCh + 1; targetCh ++){
                            std::vector<int>::iterator targetIter = std::find(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), Data->LayerList[layerNO]->EtaLECh->at(i).end(), targetCh);
                            if(targetIter != Data->LayerList[layerNO]->EtaLECh->at(i).end()){
                                int distance = std::distance(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), targetIter);
                                float timeGap = Data->EtaSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->EtaLE->at(i).at(distance);
                                if(timeGap < latencyConstraint && timeGap > -latencyConstraint){
                                    triggeredEventNumber +=1;
                                    break;
                                }
                            }
                        }
                    }
                    if((refTriggeredEvents - localCounts) < EffShowNO){
                        float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                        y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
                        y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
                    }
                    totalEventNumber ++;
                }
            }
            else if(PHIorETA == TYPE_PHI)            {
                if(Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
                    localCounts ++;
                    if (Data->PhiClusterSize[layerNO]->at(i) >= 1){
                        int refCh = Data->PhiSignalCh[refLayerNO]->at(i);
                        for(int targetCh = refCh - 1; targetCh <= refCh + 1; targetCh ++){
                            std::vector<int>::iterator targetIter = std::find(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), Data->LayerList[layerNO]->PhiLECh->at(i).end(), targetCh);
                            if(targetIter != Data->LayerList[layerNO]->PhiLECh->at(i).end()){
                                int distance = std::distance(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), targetIter);
                                float timeGap = Data->PhiSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->PhiLE->at(i).at(distance);
                                if(timeGap < latencyConstraint && timeGap > -latencyConstraint){
                                    triggeredEventNumber +=1;
                                    break;
                                }
                            }
                        }
                    }
                    if((refTriggeredEvents - localCounts) < EffShowNO){
                        float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                        y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
                        y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
                    }
                    totalEventNumber ++;
                }
            }
        }
    }
    else
    {
        if(y1[EffBinNO - 1] >= 0.001)  // to handle the overlap between loop
            for(int i = 0; i < binNumber; i++)
            {
                y1[i] = 0;
            }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            if(PHIorETA == TYPE_ETA)            {
                if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1){
                    localCounts ++;
                    if (Data->EtaClusterSize[layerNO]->at(i) >= 1){
                        int refCh = Data->EtaSignalCh[refLayerNO]->at(i);
                        for(int targetCh = refCh - 1; targetCh <= refCh + 1; targetCh ++){
                            std::vector<int>::iterator targetIter = std::find(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), Data->LayerList[layerNO]->EtaLECh->at(i).end(), targetCh);
                            if(targetIter != Data->LayerList[layerNO]->EtaLECh->at(i).end()){
                                int distance = std::distance(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), targetIter);
                                float timeGap = Data->EtaSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->EtaLE->at(i).at(distance);
                                if(timeGap < latencyConstraint && timeGap > -latencyConstraint){
                                    triggeredEventNumber +=1;
                                    break;
                                }
                            }
                        }
                    }
                    if((refTriggeredEvents - localCounts) < EffShowNO){
                        float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                        y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
                        y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
                    }
                    totalEventNumber ++;
                }
            }
            else if(PHIorETA == TYPE_PHI)
            {
                if(Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
                    localCounts ++;
                    if (Data->PhiClusterSize[layerNO]->at(i) >= 1 && Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
                        int refCh = Data->PhiSignalCh[refLayerNO]->at(i);
                        for(int targetCh = refCh - 1; targetCh <= refCh + 1; targetCh ++){
                            std::vector<int>::iterator targetIter = std::find(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), Data->LayerList[layerNO]->PhiLECh->at(i).end(), targetCh);
                            if(targetIter != Data->LayerList[layerNO]->PhiLECh->at(i).end()){
                                int distance = std::distance(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), targetIter);
                                float timeGap = Data->PhiSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->PhiLE->at(i).at(distance);
                                if(timeGap < latencyConstraint && timeGap > -latencyConstraint){
                                    triggeredEventNumber +=1;
                                    break;
                                }
                            }
                        }
                    }
                    if((refTriggeredEvents - localCounts) < EffShowNO){
                        float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                        y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
                        y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
                    }
                    totalEventNumber ++;
                }
            }
            //y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
            //totalEventNumber ++;
        }
    }
    bars1->setData(x1, y1);
    setEffTitle(y1[EffShowNO - 1]);
    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0.0);
    customPlot->yAxis->setRangeUpper(100.0);
    customPlot->replot(QCustomPlot::rpQueuedReplot);
}

void histogram::refreshChamberRefEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //The function used to refresh each singlet
{
    float latencyConstraint = 10;
    int refLayerNO = LAYERREFERENCE[layerNO];
    EffBinNO = binNumber;
    int refTriggeredEvents = 0;
    for(int i = EventStartNo; i < EventEndNo; i++){
        if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1 && Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
            refTriggeredEvents++;
        }
    }
    EffShowNO = (refTriggeredEvents + totalEventNumber) % EffBinNO;
    int localCounts = 0;
    if (EffShowNO == 0)
        EffShowNO = EffBinNO;
    int carryNO = int((float(refTriggeredEvents + totalEventNumber - 1))/float(EffBinNO));
    bool carry = carryNO > int((totalEventNumber)/(EffBinNO));
    if(totalEventNumber%EffBinNO == 0 && totalEventNumber > 0){
        carry = true;
    }
    if(carry)
    {
        for(int i = 0; i < binNumber; i++)
        {
            y1[i] = 0;
            x1[i] = i + EffBinNO * carryNO + 1;
        }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            bool triggeredFlag = false;
            if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1 && Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
                localCounts ++;
                int refChETA = Data->EtaSignalCh[refLayerNO]->at(i);
                int refChPHI = Data->PhiSignalCh[refLayerNO]->at(i);
                for(int targetChETA = refChETA - 1; targetChETA <= refChETA + 1; targetChETA ++){
                    if(triggeredFlag){
                        break;
                    }
                    for(int targetChPHI = refChPHI - 1; targetChPHI <= refChPHI + 1; targetChPHI ++){
                        std::vector<int>::iterator targetIterETA = std::find(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), Data->LayerList[layerNO]->EtaLECh->at(i).end(), targetChETA);
                        std::vector<int>::iterator targetIterPHI = std::find(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), Data->LayerList[layerNO]->PhiLECh->at(i).end(), targetChPHI);
                        if(targetIterETA != Data->LayerList[layerNO]->EtaLECh->at(i).end() && targetIterPHI != Data->LayerList[layerNO]->PhiLECh->at(i).end()){
                            int distanceETA = std::distance(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), targetIterETA);
                            int distancePHI = std::distance(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), targetIterPHI);
                            float timeGapETA = Data->EtaSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->EtaLE->at(i).at(distanceETA);
                            float timeGapPHI = Data->PhiSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->PhiLE->at(i).at(distancePHI);
                            if(timeGapETA < latencyConstraint && timeGapETA > -latencyConstraint && timeGapPHI < latencyConstraint && timeGapPHI > -latencyConstraint){
                                triggeredEventNumber +=1;
                                triggeredFlag = true;
                                break;
                            }
                        }
                    }
                }
                if((refTriggeredEvents - localCounts) < EffShowNO){
                    float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                    y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
                    y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
                }
                totalEventNumber ++;
            }
        }
    }
    else
    {
        if(y1[EffBinNO - 1] >= 0.001)  // to handle the overlap between loop
            for(int i = 0; i < binNumber; i++)
            {
                y1[i] = 0;
            }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            bool triggeredFlag = false;
            if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1 && Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
                localCounts ++;
                int refChETA = Data->EtaSignalCh[refLayerNO]->at(i);
                int refChPHI = Data->PhiSignalCh[refLayerNO]->at(i);
                for(int targetChETA = refChETA - 1; targetChETA <= refChETA + 1; targetChETA ++){
                    if(triggeredFlag){
                        break;
                    }
                    for(int targetChPHI = refChPHI - 1; targetChPHI <= refChPHI + 1; targetChPHI ++){
                        std::vector<int>::iterator targetIterETA = std::find(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), Data->LayerList[layerNO]->EtaLECh->at(i).end(), targetChETA);
                        std::vector<int>::iterator targetIterPHI = std::find(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), Data->LayerList[layerNO]->PhiLECh->at(i).end(), targetChPHI);
                        if(targetIterETA != Data->LayerList[layerNO]->EtaLECh->at(i).end() && targetIterPHI != Data->LayerList[layerNO]->PhiLECh->at(i).end()){
                            int distanceETA = std::distance(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), targetIterETA);
                            int distancePHI = std::distance(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), targetIterPHI);
                            float timeGapETA = Data->EtaSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->EtaLE->at(i).at(distanceETA);
                            float timeGapPHI = Data->PhiSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->PhiLE->at(i).at(distancePHI);
                            if(timeGapETA < latencyConstraint && timeGapETA > -latencyConstraint && timeGapPHI < latencyConstraint && timeGapPHI > -latencyConstraint){
                                triggeredEventNumber +=1;
                                triggeredFlag = true;
                                break;
                            }
                        }
                    }
                }
                if((refTriggeredEvents - localCounts) < EffShowNO){
                    float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                    y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
                    y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
                }
                totalEventNumber ++;
            }
            //qDebug()<<"Event" << i << " triggeredEventNumber = " << triggeredEventNumber <<" totalEventNumber = " <<totalEventNumber;
        }
    }
    bars1->setData(x1, y1);
    setChamberRefEffTitle(y1[EffShowNO - 1]);
    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0.0);
    customPlot->yAxis->setRangeUpper(100.0);
    customPlot->replot(QCustomPlot::rpQueuedReplot);
}


void histogram::refreshGapRefEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo) //The function used to refresh each singlet
{
    float latencyConstraint = 10;
    int refLayerNO = LAYERREFERENCE[layerNO];
    EffBinNO = binNumber;
    int refTriggeredEvents = 0;
    for(int i = EventStartNo; i < EventEndNo; i++){
        if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1 || Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
            refTriggeredEvents++;
        }
    }
    EffShowNO = (refTriggeredEvents + totalEventNumber) % EffBinNO;
    int localCounts = 0;
    if (EffShowNO == 0)
        EffShowNO = EffBinNO;
    int carryNO = int((float(refTriggeredEvents + totalEventNumber - 1))/float(EffBinNO));
    bool carry = carryNO > int((totalEventNumber)/(EffBinNO));
    if(totalEventNumber%EffBinNO == 0 && totalEventNumber > 0){
        carry = true;
    }
    if(carry)
    {
        for(int i = 0; i < binNumber; i++)
        {
            y1[i] = 0;
            x1[i] = i + EffBinNO * carryNO + 1;
        }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            bool triggeredFlag = false;
            if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1 || Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
                localCounts ++;
                int refChETA = Data->EtaSignalCh[refLayerNO]->at(i);
                int refChPHI = Data->PhiSignalCh[refLayerNO]->at(i);
                for(int targetChETA = refChETA - 1; targetChETA <= refChETA + 1; targetChETA ++){
                    if(triggeredFlag){
                        break;
                    }
                    for(int targetChPHI = refChPHI - 1; targetChPHI <= refChPHI + 1; targetChPHI ++){
                        std::vector<int>::iterator targetIterETA = std::find(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), Data->LayerList[layerNO]->EtaLECh->at(i).end(), targetChETA);
                        std::vector<int>::iterator targetIterPHI = std::find(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), Data->LayerList[layerNO]->PhiLECh->at(i).end(), targetChPHI);
                        if(targetIterETA != Data->LayerList[layerNO]->EtaLECh->at(i).end()){
                            int distanceETA = std::distance(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), targetIterETA);
                            float timeGapETA = Data->EtaSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->EtaLE->at(i).at(distanceETA);
                            if(abs(timeGapETA) < latencyConstraint){
                                triggeredEventNumber +=1;
                                triggeredFlag = true;
                                break;
                            }
                        }
                        else if(targetIterPHI != Data->LayerList[layerNO]->PhiLECh->at(i).end()){
                            int distancePHI = std::distance(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), targetIterPHI);
                            float timeGapPHI = Data->PhiSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->PhiLE->at(i).at(distancePHI);
                            if(abs(timeGapPHI) < latencyConstraint ){
                                triggeredEventNumber +=1;
                                triggeredFlag = true;
                                break;
                            }
                        }
                    }
                }
                if((refTriggeredEvents - localCounts) < EffShowNO){
                    float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                    y1[totalEventNumber%EffBinNO] = tempEff * 100.0;
                    y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
                }
                totalEventNumber ++;
            }
        }
    }
    else
    {
        if(y1[EffBinNO - 1] >= 0.001)  // to handle the overlap between loop
            for(int i = 0; i < binNumber; i++)
            {
                y1[i] = 0;
            }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            bool triggeredFlag = false;
            if(Data->EtaClusterSize[refLayerNO]->at(i) >= 1 || Data->PhiClusterSize[refLayerNO]->at(i) >= 1){
                localCounts ++;
                int refChETA = Data->EtaSignalCh[refLayerNO]->at(i);
                int refChPHI = Data->PhiSignalCh[refLayerNO]->at(i);
                for(int targetChETA = refChETA - 1; targetChETA <= refChETA + 1; targetChETA ++){
                    if(triggeredFlag){
                        break;
                    }
                    for(int targetChPHI = refChPHI - 1; targetChPHI <= refChPHI + 1; targetChPHI ++){
                        std::vector<int>::iterator targetIterETA = std::find(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), Data->LayerList[layerNO]->EtaLECh->at(i).end(), targetChETA);
                        std::vector<int>::iterator targetIterPHI = std::find(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), Data->LayerList[layerNO]->PhiLECh->at(i).end(), targetChPHI);
                        if(targetIterETA != Data->LayerList[layerNO]->EtaLECh->at(i).end()){
                            int distanceETA = std::distance(Data->LayerList[layerNO]->EtaLECh->at(i).begin(), targetIterETA);
                            float timeGapETA = Data->EtaSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->EtaLE->at(i).at(distanceETA);
                            if(abs(timeGapETA) < latencyConstraint){
                                triggeredEventNumber +=1;
                                triggeredFlag = true;
                                break;
                            }
                        }
                        else if(targetIterPHI != Data->LayerList[layerNO]->PhiLECh->at(i).end()){
                            int distancePHI = std::distance(Data->LayerList[layerNO]->PhiLECh->at(i).begin(), targetIterPHI);
                            float timeGapPHI = Data->PhiSignalLE[refLayerNO]->at(i) - Data->LayerList[layerNO]->PhiLE->at(i).at(distancePHI);
                            if(abs(timeGapPHI) < latencyConstraint ){
                                triggeredEventNumber +=1;
                                triggeredFlag = true;
                                break;
                            }
                        }
                    }
                }
                if((refTriggeredEvents - localCounts) < EffShowNO){
                    float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
                    y1[totalEventNumber%EffBinNO] = tempEff * 100.0;
                    y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
                }
                totalEventNumber ++;
            }
            //qDebug()<<"Event" << i << " triggeredEventNumber = " << triggeredEventNumber <<" totalEventNumber = " <<totalEventNumber;
        }
    }
    bars1->setData(x1, y1);
    setGapRefEffTitle(y1[EffShowNO - 1]);
    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0.0);
    customPlot->yAxis->setRangeUpper(100.0);
    customPlot->replot(QCustomPlot::rpQueuedReplot);
}



void histogram::refreshPerfEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo)
{
    EffBinNO = binNumber - 1;
    EffShowNO = ((EventEndNo - EventStartNo) + totalEventNumber) % EffBinNO;
    if (EffShowNO == 0)
        EffShowNO = EffBinNO;
    int carryNO = int((float(EventEndNo - EventStartNo + totalEventNumber - 1))/float(EffBinNO));
    bool carry = carryNO > int(float(totalEventNumber)/float(EffBinNO));
    if(carry)
    {
        for(int i = 0; i < binNumber; i++)
        {
            y1[i] = 0;
            x1[i] = i + EffBinNO * carryNO + 1;
        }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            totalEventNumber ++;
            if (trippleTriggered(Data, i))
                triggeredEventNumber +=1;
            if((EventEndNo - i) <= EffShowNO)
            {
                float tempEff = float(triggeredEventNumber)/float(totalEventNumber);
                y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber) * 100.0;
                y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber))* 100.0;
            }
        }
    }
    else
    {
        if(y1[EffBinNO - 1] >= 0.001)  // to handle the overlap between loop
            for(int i = 0; i < binNumber; i++)
            {
                y1[i] = 0;
            }
        for(int i = EventStartNo; i < EventEndNo; i++)
        {
            if (trippleTriggered(Data, i))
                triggeredEventNumber +=1;
            float tempEff = float(triggeredEventNumber)/float(totalEventNumber+1);
            y1[totalEventNumber%EffBinNO] = float(triggeredEventNumber)/float(totalEventNumber+1) * 100.0;
            y1Error = sqrt(tempEff * (1.0 - tempEff) /(totalEventNumber+1))* 100.0;
            totalEventNumber ++;
        }
    }
    bars1->setData(x1, y1);
    setEffTitle(y1[EffShowNO - 1]);
    customPlot->rescaleAxes();
    customPlot->yAxis->setRangeLower(0.0);
    customPlot->yAxis->setRangeUpper(100.0);
    customPlot->replot(QCustomPlot::rpQueuedReplot);
}

void histogram::refreshTOT(PROCESSEDData *Data, int EventStartNo, int EventEndNo)
{
    if (binSettedFlag)
    {
        for (int i = EventStartNo; i < EventEndNo; i++)
        {
            double temp = 20;
            if(PHIorETA == TYPE_PHI)
                temp = (Data->PhiSignalFE[layerNO]->at(i)) - (Data->PhiSignalLE[layerNO]->at(i));
            else
                temp = (Data->EtaSignalFE[layerNO]->at(i)) - (Data->EtaSignalLE[layerNO]->at(i));
            if(temp >= 0 && temp <= 100)
            {
                int No = int((temp - startPoint)/binGap);
                y1[No] += 1.0;
            }
        }
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
        //customPlot->repaint();
        //emit sendReplot(layerNO, PHIorETA);
        /*if(layerNO == 0 && PHIorETA == TYPE_PHI)
            timeCost[5][importLoopNO] = mainClock.elapsed();*/
    }
    else
        qDebug() << "Bins haven't been set";
}

void histogram::refreshCS(PROCESSEDData *Data, int EventStartNo, int EventEndNo)
{
    if (binSettedFlag)
    {
        for (int i = EventStartNo; i < EventEndNo; i++)
        {
            int temp;
            if(PHIorETA == TYPE_PHI)
                temp =Data->PhiClusterSize[layerNO]->at(i);
            else
                temp =Data->EtaClusterSize[layerNO]->at(i);
            if(temp >= startPoint && temp < endPoint)
            {
                y1[temp] += 1.0;
            }
        }
        double temp_mean = 0;
        int sum = 0;
        for (int i =1; i < binNumber; i++) //y1 is initialized by 'y1.resize[binNumber]
        {
            temp_mean += i * y1[i];
            sum += y1[i];
        }
        setTitle(temp_mean / sum);
        bars1->setData(x1, y1);
        customPlot->rescaleAxes();
        //customPlot->repaint();
        customPlot->replot(QCustomPlot::rpQueuedReplot);
    }
    else
        qDebug() << "Bins haven't been set";

}

void histogram::setTitle(double mean, double sigma)
{
    titleText.str("");
    titleText << "Mean = " << fixed << setprecision(2) << mean << "\t\tSigma = " << sigma;
    title->setText(titleText.str().c_str());
    //customPlot->plotLayout()->addElement(0, 0, title);
}
void histogram::setTitle(double mean)
{
    titleText.str("");
    titleText << "Mean = " << fixed << setprecision(2) << setw(3) << mean;
    title->setText(titleText.str().c_str());
}
void histogram::setTitle()
{
    //title->setText("Fit after 200 hits");
}
void histogram::setEffTitle(double eff)
{
    titleText.str("");
    if(PHIorETA == TYPE_ETA){
        titleText<<"ETA ";
    }
    else if(PHIorETA == TYPE_PHI){
        titleText<<"PHI ";
    }
    if(TYPE == TYPE_RefEFF){
        titleText<<"Reference ";
    }
    titleText << "Trigger Efficiency = " << fixed << setprecision(2) << setfill('0') << eff << "%(±" << std::setprecision(2) << y1Error << "%)";
    title->setText(titleText.str().c_str());
}

void histogram::setTriEffTitle(double eff)
{
    titleText.str("");
    titleText << "ETA&&PHI Trigger Efficiency = " << fixed << setprecision(2) << setfill('0') << eff << "%(±" << std::setprecision(2) << y1Error << "%)";
    title->setText(titleText.str().c_str());
}

void histogram::setChamberRefEffTitle(double eff)
{
    titleText.str("");
    titleText << "Chamber (ETA&&PHI) Ref Trigger Efficiency = " << fixed <<setprecision(2) << setfill('0') << eff << "%(±" << std::setprecision(2) << y1Error << "%)";
    title->setText(titleText.str().c_str());
}


void histogram::setGapRefEffTitle(double eff)
{
    titleText.str("");
    titleText << "Gap (ETA||PHI) Ref Trigger Efficiency = " << fixed << setprecision(2) << setfill('0') << eff << "%(±" << std::setprecision(2) << y1Error << "%)";
    title->setText(titleText.str().c_str());
}

bool histogram::trippleTriggered(PROCESSEDData *Data, int i)
{
    if (PHIorETA == TYPE_PHI)
    {
        if (Data->PhiSignalCh[0]->at(i) >= 0 && Data->PhiSignalCh[1]->at(i) >= 0 && Data->PhiSignalCh[1]->at(2) >= 0)
            if(Data->PhiSignalCh[0]->at(i) == Data->PhiSignalCh[1]->at(i) && Data->PhiSignalCh[1]->at(i) == Data->PhiSignalCh[2]->at(i))
                return true;
    }
    if (PHIorETA == TYPE_ETA)
    {
        if (Data->EtaSignalCh[0]->at(i) >= 0 && Data->EtaSignalCh[1]->at(i) >= 0 && Data->EtaSignalCh[1]->at(2) >= 0)
            if(Data->EtaSignalCh[0]->at(i) == Data->EtaSignalCh[1]->at(i) && Data->EtaSignalCh[1]->at(i) == Data->EtaSignalCh[2]->at(i))
                return true;
    }
    return false;
}

bool histogram::singleTriggered(PROCESSEDData *Data, int i, int l)
{
    if (PHIorETA == TYPE_PHI)
    {
        if(Data->PhiSignalCh[l]->at(i) >= 0)
        {
            //qDebug()<<Data->PhiSignalCh[l]->at(i);
            return true;
        }
    }
    else if(PHIorETA == TYPE_ETA)
    {
        if(Data->EtaSignalCh[l]->at(i) >= 0)
        {
            //qDebug() << Data->EtaSignalCh[l]->at(i);
            return true;
        }
    }
    return false;
}

bool histogram::doubleTriggered(PROCESSEDData *Data, int i, int l1, int l2)
{
    if (PHIorETA == TYPE_PHI)
    {
        if(Data->PhiSignalCh[l1]->at(i) >= 0 && Data->PhiSignalCh[l2]->at(i) >= 0){
            int chGap = Data->PhiSignalCh[l2]->at(i) - Data->PhiSignalCh[l1]->at(i);
            if(chGap >= -1 && chGap <= 1){
                return true;
            }
        }
    }
    else if(PHIorETA == TYPE_ETA)
    {
        if(Data->EtaSignalCh[l1]->at(i) >= 0 && Data->EtaSignalCh[l2]->at(i) >= 0){
            int chGap = Data->EtaSignalCh[l2]->at(i) - Data->EtaSignalCh[l1]->at(i);
            if(chGap >= -1 && chGap <= 1){
                return true;
            }
        }
    }
    return false;
}
float histogram::TripletLE(PROCESSEDData *Data, int i)
{
    float temp = 0;
    if(PHIorETA == TYPE_PHI)
    {
        temp = (Data->PhiSignalLE[0]->at(i) + Data->PhiSignalLE[2]->at(i)) / 2 - Data->PhiSignalLE[1]->at(i);
    }
    else
    {
        temp = (Data->EtaSignalLE[0]->at(i) + Data->EtaSignalLE[2]->at(i)) / 2 - Data->EtaSignalLE[1]->at(i);
    }
    return temp;
}
float histogram::DoubletLE(PROCESSEDData *Data, int i, int l1, int l2)
{
    float temp = 1.0;
    if(PHIorETA == TYPE_PHI)
    {
        temp = (Data->PhiSignalLE[l1]->at(i) - Data->PhiSignalLE[l2]->at(i)) ;
    }
    else if(PHIorETA == TYPE_ETA)
    {
        temp = (Data->EtaSignalLE[l1]->at(i) - Data->EtaSignalLE[l2]->at(i));

    }
    return temp;
}
