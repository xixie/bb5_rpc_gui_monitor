/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Charts module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "splineChart.h"
#include <QtCharts/QAbstractAxis>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QValueAxis>
#include <QtCore/QRandomGenerator>
#include <QtCore/QDebug>

splineChart::splineChart(int layerno, QGraphicsItem *parent, Qt::WindowFlags wFlags):
    QChart(QChart::ChartTypeCartesian, parent, wFlags),
    moveFlag(true),
    m_series(nullptr),
    m_axis(new QValueAxis),
    m_step(0),
    m_x(0),
    m_y(0)
{
    layerNO = layerno;
    m_series = new QSplineSeries(this);
    QPen green(Qt::red);
    green.setWidth(0);
    m_series->setPen(green);
    m_series->append(m_x, m_y);
    addSeries(m_series);
    createDefaultAxes();
    setAxisX(m_axis, m_series);
    m_axis->setTickCount(11);
    axisX()->setRange(0, 100);
    axisY()->setRange(0, 100);
    this->setTitle("Efficiency[%] VS EventNumber");
    this->setAnimationOptions(QChart::NoAnimation);

    this->legend()->setAlignment(Qt::AlignBottom);
    triggeredEventNumber = 0;
    totalEventNumber = 0;
}

splineChart::~splineChart()
{

}

void splineChart::refresh(PROCESSEDData* Data,  int EventStartNo, int EventEndNo)
{
    if(moveFlag)
    {
        qreal x = plotArea().width() / m_axis->tickCount();
        qreal delta_x = 1;
        for (int i = EventStartNo; i < EventEndNo; i++)
        {
            m_x += delta_x;
            totalEventNumber += 1;
            if (Data->PhiSignalCh[layerNO]->at(i) >= 0 && Data->EtaSignalCh[layerNO]->at(i) >= 0)
                triggeredEventNumber +=1;
            m_y = float(triggeredEventNumber)/float(totalEventNumber) * float(100.0);
            m_series->append(m_x, m_y);
        }
        if (m_axis->max() - m_x <= 20 * delta_x)
            scroll(6 * x, 0);
    }
}

void splineChart::setMoveFlag(bool flag)
{
    moveFlag = flag;
}
