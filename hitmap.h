#ifndef HITTINGMAP_H
#define HITTINGMAP_H

#include "qcustomplot.h"
#include <QWidget>
#include "v1190a.h"
#include "hitreconstructor.h"
#include "mapsettings.h"

class hitMap : public QCPColorMap
{
    Q_OBJECT
private:
    uint layerNO;
    QCPColorGradient *m_ColorBar;
    QCustomPlot *customPlot;
    QCPColorMapData* auxNo;
    QCPColorMapData* auxSum;

public:
    explicit hitMap(QCustomPlot *customPlot0, QCPAxis* xAxis, QCPAxis* yAxis);
    ~hitMap();
    void init(uint l);

signals:
    void sendReplot(uint l);

public slots:
    void refreshNoiseMap(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshHitMap(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshTOTMap(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
};



#endif
