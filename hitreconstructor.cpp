#include "hitreconstructor.h"

const int track_StripNOGap = 3;
using namespace std;
hitReconstructor::hitReconstructor(QObject *parent) : QObject(parent)
{
    processedData.EventNo = new vector<int>;
    processedData.NHits = new vector<int>;
    for(ulong i = 0; i < 3; i++)
    {
        processedData.PhiSignalCh[i] = new vector<int>;
        processedData.PhiSignalLE[i] = new vector<float>;
        processedData.PhiSignalFE[i] = new vector<float>;
        processedData.EtaSignalCh[i] = new vector<int>;
        processedData.EtaSignalLE[i] = new vector<float>;
        processedData.EtaSignalFE[i] = new vector<float>;
        processedData.PhiClusterSize[i] = new vector<int>;
        processedData.EtaClusterSize[i] = new vector<int>;
        processedData.LayerList[i] = new SINGLETData;
        processedData.LayerList[i]->PhiLECh = new vector<vector<int>>;
        processedData.LayerList[i]->PhiLE = new vector<vector<float>>;
        processedData.LayerList[i]->PhiFECh = new vector<vector<int>>;
        processedData.LayerList[i]->PhiFE = new vector<vector<float>>;

        processedData.LayerList[i]->EtaLECh = new vector<vector<int>>;
        processedData.LayerList[i]->EtaLE = new vector<vector<float>>;
        processedData.LayerList[i]->EtaFECh = new vector<vector<int>>;
        processedData.LayerList[i]->EtaFE = new vector<vector<float>>;

        processedData.LayerList[i]->PhiPaired = new vector<vector<bool>>;
        processedData.LayerList[i]->EtaPaired = new vector<vector<bool>>;
    }
    clear();
    temp_PhiLECh = new vector<int>;
    temp_PhiLE = new vector<float>;
    temp_PhiFECh = new vector<int>;
    temp_PhiFE = new vector<float>;

    temp_EtaLECh = new vector<int>;
    temp_EtaLE = new vector<float>;
    temp_EtaFECh = new vector<int>;
    temp_EtaFE = new vector<float>;

    temp_PhiPaired = new vector<bool>;
    temp_EtaPaired = new vector<bool>;
    temp_clear();
    EventNumber = 0;
    TriggeredNumber = 0;
    outFile.open("classifiedData.csv", ios::out);
}
void hitReconstructor::mapinit()
{
    /*qDebug()<<"hitReco.. mapinit";
    qDebug()<<"Phi_st_list[0]" << Phi_st_list[0];
    qDebug()<<"Phi_ed_list[0]" << Phi_ed_list[0];
    qDebug()<<"Eta_st_list[0]" << Eta_st_list[0];
    qDebug()<<"Eta_ed_list[0]" << Eta_ed_list[0];
    qDebug()<<"hitReco.. mapinit complete";*/

}
void hitReconstructor::process(RAWData* TDCData, size_t event_st, size_t event_ed)
{
    //temp_clear();
    clear();
    Event_ST = event_st;
    Event_ED = event_ed;
    reconstruct(TDCData);
    calculate();
    tracking();
    emit SendProcessedData(&processedData, 0, Event_ED - Event_ST);
}

void hitReconstructor::reconstruct(RAWData* TDCData)
{
    map<size_t,size_t>::iterator PhiTDCIter;
    map<size_t,size_t>::iterator EtaTDCIter;
    std::vector<size_t> temp_MapFlag;
    size_t temp_TDCCh = 0;
    //Layer l       Event  i        Hit  j
    for(size_t i = Event_ST; i < Event_ED ; i++)
    {
        temp_clear();
        int Temp_EventNO = TDCData->EventList->at(i);
        int Temp_Nhits =TDCData->NHitsList->at(i);
        temp_MapFlag.clear();
        for(int t = 0; t<Temp_Nhits;t++){
            temp_MapFlag.push_back(0);
        }
        processedData.EventNo->push_back(Temp_EventNO);
        processedData.NHits->push_back(Temp_Nhits);
        outFile <<"EventNO" << Temp_EventNO << endl;
        outFile << "Hit,Ch,TS,SS,layer,Eta or Phi"<<endl;
        for(size_t l = 0; l < LAYERNOS; l ++)
        {
            for(size_t j = 0; j < static_cast<size_t>(Temp_Nhits); j++)
            {
                temp_TDCCh = size_t(TDCData->ChannelList->at(i).at(j));

                int temp_flag=0;
                //Using temp_flag as flag, to check if this hit could be found in the mapping,
                //otherwise continue, pass this hit and send the warning
                for(size_t temp_l = 0; temp_l < LAYERNOS; temp_l++){
                    PhiTDCIter = PHITDCMAP[temp_l].find(temp_TDCCh);
                    EtaTDCIter = ETATDCMAP[temp_l].find(temp_TDCCh);
                    if(EtaTDCIter == ETATDCMAP[temp_l].end() && PhiTDCIter == PHITDCMAP[temp_l].end() ){
                        temp_flag++;
                    }
                }
                if(temp_flag == LAYERNOS){
                    MSG_INFO("[MAP]Warning! TDCCh=%zu doesn't have corresponding strip\n", temp_TDCCh);
                    continue;
                }

                int temp_stamp = TDCData->StyleStampList->at(i).at(j);
                float temp_time = TDCData->TimeStampList->at(i).at(j);
                PhiTDCIter = PHITDCMAP[l].find(temp_TDCCh);
                EtaTDCIter = ETATDCMAP[l].find(temp_TDCCh);
                if( PhiTDCIter != PHITDCMAP[l].end())
                {
                    //All the hits are reserved for the noise map
                    //So that the more constraints are deleted
                    if(temp_stamp == LEFlag)// && temp_time >= LeadginEdgeSt &&temp_time <= LeadingEdgeEd)
                    {
                        temp_PhiLECh->push_back(int(PhiTDCIter->second));
                        temp_PhiLE->push_back(temp_time);
                        outFile << j <<',' << PhiTDCIter->second << ',' << temp_time << ',' << temp_stamp << ',' << l << ",PHI" << endl;
                        continue;
                    }
                    else if(temp_stamp == FEFlag )//&& temp_time >= LeadginEdgeSt)
                    {
                        temp_PhiFECh->push_back(int(PhiTDCIter->second));
                        temp_PhiFE->push_back(temp_time);
                        outFile << j <<',' << PhiTDCIter->second << ',' << temp_time << ',' << temp_stamp << ',' << l << ",PHI" << endl;
                        continue;
                    }
                }
                else if( EtaTDCIter != ETATDCMAP[l].end())
                {
                    if(temp_stamp == LEFlag)// && temp_time >= LeadginEdgeSt &&temp_time <= LeadingEdgeEd)
                    {
                        temp_EtaLECh->push_back(int(EtaTDCIter->second));
                        temp_EtaLE->push_back(temp_time);
                        outFile << j <<',' << EtaTDCIter->second << ',' << temp_time << ',' << temp_stamp << ',' << l << ",ETA"<< endl;
                        continue;
                    }
                    else if(temp_stamp == FEFlag)// && temp_time >= LeadginEdgeSt)
                    {
                        temp_EtaFECh->push_back(int(EtaTDCIter->second));
                        temp_EtaFE->push_back(temp_time);
                        outFile << j <<',' << EtaTDCIter->second << ',' << temp_time << ',' << temp_stamp << ',' << l << ",ETA"<< endl;
                        continue;
                    }
                }
                else{
                    //temp_MapFlag.at(j) = l+1;
                }

            }
            //Fill the classified data of one event and one layer
            processedData.LayerList[l]->PhiLE->push_back(*temp_PhiLE);
            processedData.LayerList[l]->PhiLECh->push_back(*temp_PhiLECh);
            processedData.LayerList[l]->PhiFE->push_back(*temp_PhiFE);
            processedData.LayerList[l]->PhiFECh->push_back(*temp_PhiFECh);

            processedData.LayerList[l]->EtaLE->push_back(*temp_EtaLE);
            processedData.LayerList[l]->EtaLECh->push_back(*temp_EtaLECh);
            processedData.LayerList[l]->EtaFE->push_back(*temp_EtaFE);
            processedData.LayerList[l]->EtaFECh->push_back(*temp_EtaFECh);
            temp_clear();
        }
        outFile << endl;
        /*for(uint k = 0; k < temp_MapFlag.size();k++){
            if(temp_MapFlag.at(k)==LAYERNOS){
            MSG_INFO("[MAP]ERROR! TDC map isn't correct, TDCCh=%zu\n", temp_TDCCh);
            for(size_t l = 0; l < LAYERNOS; l ++){
                PhiTDCIter = PHITDCMAP[l].find(temp_TDCCh);
                EtaTDCIter = ETATDCMAP[l].find(temp_TDCCh);
                if(EtaTDCIter != ETATDCMAP[l].end() || PhiTDCIter != PHITDCMAP[l].end()){
                    MSG_INFO("Found in layer %zu \n", l);
                    }
                }
            }
        }*/
    }
}

void hitReconstructor::calculate()
{
    temp_clear(); //temp vectors used as pointers instead of containers;
    //Find the earliest LE, record the ch and timestamp, and filter the rest LE(time gap <= 5ns)
    for(size_t l = 0; l < LAYERNOS; l++)
    {
        for(size_t i = 0; i < Event_ED - Event_ST; i++)
        {
            //qDebug() << "Event " << i << ":";
            std::vector<float> tmpLE;
            std::vector<int> tmpLECh;
            for(int k = 0; k < 2; k++){ //Eta 0 , Phi 1
                tmpLE.clear();
                tmpLECh.clear();
                //Allign shortcuts
                if(k == 0){
                    SC_LE                              =       &processedData.LayerList[l]->EtaLE->at(i);
                    SC_FE                              =       &processedData.LayerList[l]->EtaFE->at(i);
                    SC_LECh                            =       &processedData.LayerList[l]->EtaLECh->at(i);
                    SC_FECh                            =       &processedData.LayerList[l]->EtaFECh->at(i);
                    tmpLE = processedData.LayerList[l]->EtaLE->at(i);
                    tmpLECh = processedData.LayerList[l]->EtaLECh->at(i);
                }
                else if(k == 1){
                    SC_LE                              =       &processedData.LayerList[l]->PhiLE->at(i);
                    SC_FE                              =       &processedData.LayerList[l]->PhiFE->at(i);
                    SC_LECh                            =       &processedData.LayerList[l]->PhiLECh->at(i);
                    SC_FECh                            =       &processedData.LayerList[l]->PhiFECh->at(i);
                    tmpLE = processedData.LayerList[l]->PhiLE->at(i);
                    tmpLECh = processedData.LayerList[l]->PhiLECh->at(i);
                }
                if(SC_LE->size()!=0) //A non empty event
                {

                    int SignalCh = -1;
                    //Find earliest LE (after Leading edge start point) and corrosponding channel
                    int SC_NO    =   -1;
                    float FirstHit = LeadingEdgeEd;
                    for(size_t j = 0; j < SC_LE->size(); j++){
                        if(SC_LE->at(j) >= LeadginEdgeSt && SC_LE->at(j) < FirstHit){
                            FirstHit = SC_LE->at(j);
                            SC_NO = j;
                            SignalCh = SC_LECh->at(SC_NO);
                        }
                    }
                    if (SC_NO < 0){
                        if(k == 1){
                            processedData.PhiSignalLE[l]->push_back(0.0);
                            processedData.PhiSignalCh[l]->push_back(-1);
                            processedData.PhiClusterSize[l]->push_back(0);
                            processedData.PhiSignalFE[l]->push_back(2000);
                            continue;
                        }
                        else{
                            processedData.EtaSignalLE[l]->push_back(0.0);
                            processedData.EtaSignalCh[l]->push_back(-1);
                            processedData.EtaClusterSize[l]->push_back(0);
                            processedData.EtaSignalFE[l]->push_back(2000);
                            continue;
                        }
                    }


                    //cut the hit out of hit range/hit window int tmp vectors
                    //Because all information must be preserved so that could only delete in a duplicate
                    //Help calculate the TOT (find the FE)
                    for(size_t j = 0; j < tmpLE.size(); j++){
                        if(tmpLE.at(j) > (FirstHit + 10.0) || tmpLE.at(j) < LeadginEdgeSt){
                            tmpLE.erase(tmpLE.begin() + long(j));
                            tmpLECh.erase(tmpLECh.begin() + long(j));
                            j --;
                        }
                    }

                    float TOT = 0;
                    float FE = 2000; // a value beyond ed range

                    //CS calculating. Strict judgement: once uncontinued, break
                    int SC_CS = 0;
                    for(size_t j = SignalCh; j >= (k?PHISTARTSTRIPNO:ETASTARTSTRIPNO); j --){
                        vector<int>::iterator neighbour_it = find(tmpLECh.begin(), tmpLECh.end(), j);
                        if(neighbour_it !=tmpLECh.end()){
                            SC_CS++;
                            {//Calculate the TOT of each channel in Cluster And find the signal ch with largest TOT
                                //Find FE
                                float tmp_FE = 2000; // a value beyond ed range
                                for(size_t x = 0; x < SC_FECh->size(); x++)
                                {
                                    if(SC_FECh->at(x) == j)
                                    {
                                        // later than LE, mininum one
                                        if(SC_FE->at(x) >= FirstHit && SC_FE->at(x) < tmp_FE)
                                            tmp_FE = SC_FE->at(x);
                                    }
                                }
                                //TOT
                                float tempTOT = tmp_FE - tmpLE.at(distance(tmpLECh.begin(), neighbour_it));
                                if(tempTOT > TOT && tmp_FE < 2000){
                                    TOT = tempTOT;
                                    FE = tmp_FE;
                                    SignalCh = *neighbour_it;
                                    FirstHit = tmpLE.at(distance(tmpLECh.begin(), neighbour_it));
                                }
                            }
                        }
                        else
                            break;
                    }
                    for(size_t j = SignalCh + 1; j <= (k?(PHISTARTSTRIPNO+PHISTRIPNOS):(ETASTARTSTRIPNO+ETASTRIPNOS)); j ++){
                        vector<int>::iterator neighbour_it = find(tmpLECh.begin(), tmpLECh.end(), j);
                        if(neighbour_it != tmpLECh.end()){
                            SC_CS++;
                            {//Calculate the TOT of each channel in Cluster
                                //And find the signal ch with largest TOT
                                float tmp_FE = 2000; // a value beyond ed range
                                for(size_t x = 0; x < SC_FECh->size(); x++)
                                {
                                    if(SC_FECh->at(x) == j)
                                    {
                                        // later than LE, mininum one
                                        if(SC_FE->at(x) >= FirstHit && SC_FE->at(x) < tmp_FE)
                                            tmp_FE = SC_FE->at(x);
                                    }
                                }
                                float tempTOT = tmp_FE - tmpLE.at(distance(tmpLECh.begin(), neighbour_it));
                                if(tempTOT > TOT && tmp_FE < 2000){
                                    TOT = tempTOT;
                                    SignalCh = *neighbour_it;
                                    FirstHit = tmpLE.at(distance(tmpLECh.begin(), neighbour_it));
                                }
                            }
                        }
                        else
                            break;
                    }
                    //Fill the Event data (preparing for plotting)
                    if(k == 1){
                        processedData.PhiClusterSize[l]->push_back(SC_CS);//CS finished
                        processedData.PhiSignalLE[l]->push_back(FirstHit);
                        processedData.PhiSignalFE[l]->push_back(FE);
                        processedData.PhiSignalCh[l]->push_back(SignalCh);

                    }
                    else{
                        processedData.EtaClusterSize[l]->push_back(SC_CS);//CS finished
                        processedData.EtaSignalLE[l]->push_back(FirstHit);
                        processedData.EtaSignalFE[l]->push_back(FE);
                        processedData.EtaSignalCh[l]->push_back(SignalCh);

                    }

                }
                else{
                    if(k == 1){
                        processedData.PhiSignalLE[l]->push_back(0.0);
                        processedData.PhiSignalCh[l]->push_back(-1);
                        processedData.PhiClusterSize[l]->push_back(0);
                        processedData.PhiSignalFE[l]->push_back(2000);
                    }
                    else{
                        processedData.EtaSignalLE[l]->push_back(0.0);
                        processedData.EtaSignalCh[l]->push_back(-1);
                        processedData.EtaClusterSize[l]->push_back(0);
                        processedData.EtaSignalFE[l]->push_back(2000);
                    }
                }
            }
        }
    }
}

void hitReconstructor::tracking()
{
    vector<float> X;
    vector<float> Y;
    vector<float> Z;
    processedData.K1.clear();
    processedData.K2.clear();
    processedData.C1.clear();
    processedData.C2.clear();
    processedData.isTrack.clear();
    //float StripWidth = iniFile->floatType("PositionSettings", "StripWidth", 25.0);
    //float StripGap = iniFile->floatType("PositionSettings", "StripGap", 2.0);
    float SingletHeight = iniFile->floatType("PositionSettings", "SingletHeight", 5.0);
    float SingletGap = iniFile->floatType("PositionSettings", "SingletGap", 0.0);
    float X0 = iniFile->floatType("PositionSettings", "X0", 1000.0);
    float Y0 = iniFile->floatType("PositionSettings", "Y0", 1000.0);
    float Z0 = iniFile->floatType("PositionSettings", "Z0", 1000.0);
    float XMargin = iniFile->floatType("PositionSettings", "XMargin", 3.0);
    float YMargin = iniFile->floatType("PositionSettings", "YMargin", 3.0);
    for(uint i = 0; i < Event_ED - Event_ST;i++)
    {
        float flag = true;
        for(uint l = 0; l < LAYERNOS; l++)
        {
            //if one of the eta/phi channel of one layer isn't triggered/recorded, then set the flag as false
            if(processedData.PhiSignalCh[l]->at(i) == -1 || processedData.EtaSignalCh[l]->at(i)== -1)
                flag = false;

            //if layers < 2, not a trigger
            if(LAYERNOS < 2)
                flag = false;

            //if triggered channels of neighbors layers is too far, set the flag as false
            int EtaDelta = processedData.PhiSignalCh[l]->at(i) - processedData.PhiSignalCh[(l+1) % LAYERNOS]->at(i);
            int PhiDelta = processedData.EtaSignalCh[l]->at(i) - processedData.EtaSignalCh[(l+1) % LAYERNOS]->at(i);
            if (EtaDelta > track_StripNOGap || EtaDelta < -track_StripNOGap || PhiDelta >track_StripNOGap || PhiDelta < -track_StripNOGap)
                flag = false;
        }

        if(flag == true)
            processedData.isTrack.push_back(true);
        else
            processedData.isTrack.push_back(false);


        //fulfill the track info
        if(processedData.isTrack.at(i) == true)
        {
            X.clear();
            Y.clear();
            Z.clear();
            for(int l = 0; l < LAYERNOS; l++)
            {
                std::map<size_t, size_t>::iterator ETATDCst_it = ETASTRIPMAP.at(l).find(0);
                std::map<size_t, size_t>::iterator PHITDCst_it = ETASTRIPMAP.at(l).find(0);
                int XStripNumber = processedData.PhiSignalCh[l]->at(i);// - (*PHITDCst_it).second + PHISTARTSTRIPNO; //counting from 0
                int YStripNumber = processedData.EtaSignalCh[l]->at(i);// - (*ETATDCst_it).second + ETASTARTSTRIPNO; //counting from 0

                //The positioning part
                //Using the center position of one strip
                X.push_back(X0 + XMargin + (XStripNumber + 0.5) * (PHISTRIPWIDTH.at(l) + PHIGAPWIDTH.at(l)));
                Y.push_back(Y0 + YMargin + (YStripNumber + 0.5) * (ETASTRIPWIDTH.at(l) + ETAGAPWIDTH.at(l)));
                Z.push_back(Z0 + (2 - l + 0.5) * (SingletHeight + SingletGap)); //l0 = TOP, l1 = MID, l2 = BOT
            }

            //X = k1 * Z + c1, Y = k2 * Z + C2
            if(LAYERNOS == 2)
            {
                float k1 = (X.at(0) - X.at(1)) / (Z.at(0) - Z.at(1));
                float c1 = X.at(0) - k1 * Z.at(0);
                float k2 = (Y.at(0) - Y.at(1)) / (Z.at(0) - Z.at(1));
                float c2 = Y.at(0) - k2 * Z.at(0);
                processedData.C1.push_back(c1);
                processedData.K1.push_back(k1);
                processedData.C2.push_back(c2);
                processedData.K2.push_back(k2);
            }

            //LSE algorithm to calculate the track : X = k1 * Z + c1, Y = k2 * Z + C2
            else if(LAYERNOS >= 3)
            {
                //qDebug() << "X position:" << X;
                //qDebug() << "Y position:" << Y;
                //qDebug() << "Z position:" << Z;
                float k1 = (LAYERNOS * SumOfProduct(Z, X) - SumOfProduct(Z, 1) * SumOfProduct(X, 1)) / (LAYERNOS * SumOfProduct(Z, Z) - SumOfProduct(Z, 1) * SumOfProduct(Z, 1));
                float c1 = (SumOfProduct(X, 1) - k1 * SumOfProduct(Z, 1)) / LAYERNOS;
                float k2 = (LAYERNOS * SumOfProduct(Z, Y) - SumOfProduct(Z, 1) * SumOfProduct(Y, 1)) / (LAYERNOS * SumOfProduct(Z, Z) - SumOfProduct(Z, 1) * SumOfProduct(Z, 1));
                float c2 = (SumOfProduct(Y, 1) - k2 * SumOfProduct(Z, 1)) / LAYERNOS;
                //qDebug() << "EventNO = " << processedData.EventNo->at(i);
                for(uint l = 0; l < LAYERNOS; l++)
                {
                    //qDebug() << "L" << l << "Phi:" << processedData.PhiSignalCh[l]->at(i) - Phi_st_list[l];
                    //qDebug() << "L" << l << "Eta:" << processedData.EtaSignalCh[l]->at(i) - Eta_st_list[l];
                }
                //qDebug() << "K1, C1, K2, C2 = " << k1 << ", " << c1 << ", " << k2 << ", "  << c2;
                processedData.C1.push_back(c1);
                processedData.K1.push_back(k1);
                processedData.C2.push_back(c2);
                processedData.K2.push_back(k2);
                //qDebug() << "" ;
            }
        }
        else
        {
            processedData.K1.push_back(0.0);
            processedData.K2.push_back(0.0);
            processedData.C1.push_back(0.0);
            processedData.C2.push_back(0.0);
        }
    }
}

float hitReconstructor::SumOfProduct(std::vector<float> Var1, std::vector<float> Var2)
{
    float sum = 0;
    for(uint i = 0; i < Var1.size(); i++)
    {
        sum += Var1.at(i) * Var2.at(i);
    }
    return  sum;
}
float hitReconstructor::SumOfProduct(std::vector<float> Var1, float C)
{
    float sum = 0;
    for(uint i = 0; i < Var1.size(); i++)
    {
        sum += Var1.at(i);
    }
    return  sum;
}

void hitReconstructor::clear()
{
    for(ulong i = 0; i < 3; i++)
    {
        delete processedData.PhiSignalCh[i];
        delete processedData.PhiSignalLE[i];
        delete processedData.PhiSignalFE[i];
        delete processedData.EtaSignalCh[i];
        delete processedData.EtaSignalLE[i];
        delete processedData.EtaSignalFE[i];
        delete processedData.PhiClusterSize[i];
        delete processedData.EtaClusterSize[i];
        delete processedData.LayerList[i]->PhiLECh;
        delete processedData.LayerList[i]->PhiLE;
        delete processedData.LayerList[i]->PhiFECh;
        delete processedData.LayerList[i]->PhiFE;

        delete processedData.LayerList[i]->EtaLECh;
        delete processedData.LayerList[i]->EtaLE;
        delete processedData.LayerList[i]->EtaFECh;
        delete processedData.LayerList[i]->EtaFE;

        delete processedData.LayerList[i]->PhiPaired;
        delete processedData.LayerList[i]->EtaPaired ;

        delete processedData.LayerList[i];
    }
    delete processedData.EventNo;
    delete processedData.NHits;


    processedData.EventNo = new vector<int>;
    processedData.NHits = new vector<int>;
    for(ulong i = 0; i < 3; i++)
    {
        processedData.PhiSignalCh[i] = new vector<int>;
        processedData.PhiSignalLE[i] = new vector<float>;
        processedData.PhiSignalFE[i] = new vector<float>;
        processedData.EtaSignalCh[i] = new vector<int>;
        processedData.EtaSignalLE[i] = new vector<float>;
        processedData.EtaSignalFE[i] = new vector<float>;
        processedData.PhiClusterSize[i] = new vector<int>;
        processedData.EtaClusterSize[i] = new vector<int>;
        processedData.LayerList[i] = new SINGLETData;
        processedData.LayerList[i]->PhiLECh = new vector<vector<int>>;
        processedData.LayerList[i]->PhiLE = new vector<vector<float>>;
        processedData.LayerList[i]->PhiFECh = new vector<vector<int>>;
        processedData.LayerList[i]->PhiFE = new vector<vector<float>>;

        processedData.LayerList[i]->EtaLECh = new vector<vector<int>>;
        processedData.LayerList[i]->EtaLE = new vector<vector<float>>;
        processedData.LayerList[i]->EtaFECh = new vector<vector<int>>;
        processedData.LayerList[i]->EtaFE = new vector<vector<float>>;

        processedData.LayerList[i]->PhiPaired = new vector<vector<bool>>;
        processedData.LayerList[i]->EtaPaired = new vector<vector<bool>>;
    }

    processedData.EventNo->clear();
    processedData.NHits->clear();
    for(size_t l = 0; l < LAYERNOS; l++)
    {
        processedData.PhiClusterSize[l]->clear();
        processedData.EtaClusterSize[l]->clear();
        processedData.LayerList[l]->PhiLE->clear();
        processedData.LayerList[l]->PhiFE->clear();
        processedData.LayerList[l]->PhiLECh->clear();
        processedData.LayerList[l]->PhiFECh->clear();
        processedData.LayerList[l]->EtaLE->clear();
        processedData.LayerList[l]->EtaFE->clear();
        processedData.LayerList[l]->EtaLECh->clear();
        processedData.LayerList[l]->EtaFECh->clear();
        processedData.PhiSignalCh[l]->clear();
        processedData.PhiSignalLE[l]->clear();
        processedData.PhiSignalFE[l]->clear();
        processedData.PhiClusterSize[l]->clear();
        processedData.EtaSignalCh[l]->clear();
        processedData.EtaSignalLE[l]->clear();
        processedData.EtaSignalFE[l]->clear();
        processedData.EtaClusterSize[l]->clear();
    }
}

void hitReconstructor::temp_clear()
{
    delete temp_PhiLECh;
    delete temp_PhiLE;
    delete temp_PhiFECh;
    delete temp_PhiFE;

    delete temp_EtaLECh;
    delete temp_EtaLE;
    delete temp_EtaFECh;
    delete temp_EtaFE;

    delete temp_PhiPaired;
    delete temp_EtaPaired;


    temp_PhiLECh = new vector<int>;
    temp_PhiLE = new vector<float>;
    temp_PhiFECh = new vector<int>;
    temp_PhiFE = new vector<float>;

    temp_EtaLECh = new vector<int>;
    temp_EtaLE = new vector<float>;
    temp_EtaFECh = new vector<int>;
    temp_EtaFE = new vector<float>;

    temp_PhiPaired = new vector<bool>;
    temp_EtaPaired = new vector<bool>;

    temp_PhiLECh->clear();
    temp_PhiFECh->clear();
    temp_PhiLE->clear();
    temp_PhiFE->clear();

    temp_EtaLECh->clear();
    temp_EtaFECh->clear();
    temp_EtaLE->clear();
    temp_EtaFE->clear();

    temp_PhiPaired->clear();
    temp_EtaPaired->clear();
}
