// *************************************************************************************************************
// *   IniFile
// *   Added & edited by
// *   Alexis Fagot
// *   from QC3 DAQ by
// *   Stefano Colafranceschi
// *   29/01/2015
// *************************************************************************************************************

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <QDebug>
#include <stdio.h>

#include "../include/IniFile.h"
#include "../include/MsgSvc.h"

using namespace std;

// *************************************************************************************************************

IniFile::IniFile(QWidget* parent):
    QWidget(parent)
{

}

// *************************************************************************************************************

IniFile::IniFile(string filename){
    SetFileName(filename);
}

// *************************************************************************************************************

IniFile::~IniFile(){

}

// *************************************************************************************************************

bool IniFile::CheckIfComment(string line){
    return ( line[0] == '#' );
}

// *************************************************************************************************************

bool IniFile::CheckIfGroup(string line,string& group){
    if( line[0] == '[' ){
        if( line[line.length()-1] == ']' ){ // The format is right
                group = line.substr(1,line.length()-2); //for linux environment, -2 and -3
                return true;
        } else {
            Error = INI_ERROR_WRONG_GROUP_FORMAT;
            return true;
        }
    }
    return false;
}

// *************************************************************************************************************

bool IniFile::CheckIfToken(string line,string& key,string& value){
    size_t p0 = 0;

    size_t p1 = string::npos;
    p1 = line.find_first_of('=',p0);

    if(p1 != p0){
        key = line.substr(p0,(p1-p0));
        p0 = line.find_first_not_of('=',p1);
        if(p0 != string::npos){
            value = line.substr(p0,(line.size()-p0));
        } else {
            Error = INI_ERROR_MISSING_VALUE;
            return true;
        }
    } else {
        Error = INI_ERROR_WRONG_FORMAT;
        return false;
    }
    return true;
}

// *************************************************************************************************************

void IniFile::SetFileName(const string filename){
    FileName = filename;
}

// *************************************************************************************************************

int IniFile::Read(){
    ifstream ini("daqgifpp.ini");//(FileName.c_str());
    stringstream parser;
    string token, value, line, group;

    Error = INI_OK;

    // Loading the file into the parser
    if(ini){
        MSG_INFO("[IniFile]: Opening configuration file %s\n",FileName.c_str());
        parser << ini.rdbuf();
        ini.close();
    } else {
        Error = INI_ERROR_CANNOT_OPEN_READ_FILE;
        qDebug()<<"Inifile Failed" << endl;
        MSG_ERROR("[IniFile]: Cannot open configuration file.\n");
        return Error;
    }

    group = "";

    while(getline(parser,line) && (Error == INI_OK)){
        // Check if the line is comment
        if(!CheckIfComment(line) ){
            // Check for group
            if(!CheckIfGroup(line,group)){
                // Check for token
                if(CheckIfToken(line,token,value))
                {
                    // Make the key in format group.key if the group is not empty
                    if(group.size() > 1)
                        token = group + "." + token;
                    FileData[token] = value;
                }
                else {
                    Error = INI_ERROR_WRONG_FORMAT;
                    return Error;
                }
            }
        }
    }
    IniFileDataIter Iter;
    QString temp ;
    {
        Iter = FileData.find("PositionSettings.X0");
        temp = Iter->second.c_str();
        X0 = temp.toFloat();
        //qDebug() << X0;
        Iter = FileData.find("PositionSettings.Y0");
        temp = Iter->second.c_str();
        Y0 = temp.toFloat();
        //qDebug() << Y0;

    }
    for(Iter = FileData.begin(); Iter != FileData.end(); Iter++)
    {
        MSG_INFO("[IniFile]: %s = %s\n", Iter->first.c_str(), Iter->second.c_str());
    }

    return Error;
}

// *************************************************************************************************************

int IniFile::Write(){
    Error = INI_OK;

    return Error;
}

// *************************************************************************************************************


Data32 IniFile::addressType(const string groupname, const string keyname, const Data32 defaultvalue ){
    stringstream sstr;
    string key;
    long addressValue = defaultvalue;

    IniFileDataIter Iter;

    if(groupname.size() > 0)
        key = groupname + "." + keyname;

    Iter = FileData.find(key);

    if(Iter != FileData.end())
        addressValue = strtoul(Iter->second.c_str(),NULL,16);

    return addressValue;
}

// *************************************************************************************************************


long IniFile::intType(const string groupname, const string keyname, long defaultvalue ){
    string key;
    long intValue = defaultvalue;
    string fileValue;

    IniFileDataIter Iter;

    if(groupname.size() > 0)
        key = groupname + "." + keyname;

    Iter = FileData.find(key);

    if(Iter != FileData.end()){
        int base = 0;
        fileValue = Iter->second;
        if(fileValue[1] == 'x')
            base = 16;
        else if(fileValue[1] == 'b'){
            base = 2;                   //In the case of binary values, the function doesn't
            fileValue.erase(0,2);       //understand 0b as a integer literals -> erase it
        }

        intValue = strtol(fileValue.c_str(),NULL,base);
    }

    return intValue;
}

// *************************************************************************************************************

long long IniFile::longType(const string groupname, const string keyname, const long long defaultvalue ){
    string key;
    long long longValue = defaultvalue;
    string fileValue;

    IniFileDataIter Iter;

    if(groupname.size() > 0)
        key = groupname + "." + keyname;

    Iter = FileData.find(key);

    if(Iter != FileData.end()){
        int base = 0;
        fileValue = Iter->second;
        if(fileValue[1] == 'x')
            base = 16;
        else if(fileValue[1] == 'b'){
            base = 2;                   //In the case of binary values, the function doesn't
            fileValue.erase(0,2);       //understand 0b as a integer literals -> erase it
        }

        longValue = strtoll(fileValue.c_str(),NULL,base);
    }

    return longValue;
}

// *************************************************************************************************************

string IniFile::stringType( const string groupname, const string keyname, const string defaultvalue ){
    string key;
    string stringChain = defaultvalue;

    IniFileDataIter Iter;

    if(groupname.size() > 0)
        key = groupname + "." + keyname;

    Iter = FileData.find(key);

    if(Iter != FileData.end())
        stringChain = Iter->second;

    return stringChain;
}

// *************************************************************************************************************

float IniFile::floatType( const string groupname, const string keyname, const float defaultvalue ){
    string key;
    float floatValue = defaultvalue;

    IniFileDataIter Iter;

    if(groupname.size() > 0)
        key = groupname + "." + keyname;

    Iter = FileData.find(key);

    if(Iter != FileData.end())
        floatValue = strtof(Iter->second.c_str(),NULL);

    return floatValue;
}

// *************************************************************************************************************

string IniFile::GetErrorMsg(){
    return "";
}

