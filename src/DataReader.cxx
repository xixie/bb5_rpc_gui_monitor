// ****************************************************************************************************
// *   DataReader
// *   Alexis Fagot
// *   23/01/2015
// ****************************************************************************************************

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <unistd.h>
#include <QDebug>
#include <random>

#include "../include/DataReader.h"
#include "../include/MsgSvc.h"

using namespace std;

// ****************************************************************************************************

DataReader::DataReader(QObject *parent):
    QObject(parent)
{
    VME = new v1718();
    TDCs = new v1190a();    
    connect(TDCs, SIGNAL(SendContext(QString)), this, SLOT(RelayContext(QString)));
    connect(VME, SIGNAL(SendContext(QString)), this, SLOT(RelayContext(QString)));

    //Initialize for IniFile configuration
    iniFile = new IniFile("gifdaqpp.ini");
    connect(iniFile, SIGNAL(SendContext(QString)), this, SLOT(RelayContext(QString)));

    hitRecon = new hitReconstructor();
    hitRecon->iniFile = iniFile;

    connect(hitRecon, SIGNAL(SendContext(QString)), this, SLOT(RelayContext(QString)));
    connect(hitRecon, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), this, SLOT(RelaySendProcessedData(PROCESSEDData*, int, int)));
    //Initialization of the RAWData vectors
    TDCData.EventList = new vector<int>;
    TDCData.NHitsList = new vector<int>;
    TDCData.ChannelList = new vector< vector<int> >;
    TDCData.TimeStampList = new vector< vector<float> >;
    TDCData.StyleStampList = new vector< vector<int> >;
    //EventNo =   0;
    //Cleaning all the vectors
    TDCData.EventList->clear();
    TDCData.NHitsList->clear();
    TDCData.ChannelList->clear();
    TDCData.TimeStampList->clear();
    TDCData.StyleStampList->clear();


    StopFlag = false;
    TriggerCount = 0;
    RunFlag = false;
    ImportFlag = false;
    ImportFinishedFlag = false;
    Set = 100;
    EventSets = 0;
    TFile *unused = new TFile("Ignore_this.root", "RECREATE");
    unused->Close();
    temp_time = 0;
    outFile.open("origindata.csv", ios::out); // 打开模式可省略
}

// ****************************************************************************************************

DataReader::~DataReader()
{
}

// ****************************************************************************************************

void DataReader::SetIniFile(){
    iniFile->Read();
}

// ****************************************************************************************************

void DataReader::SetMaxTriggers(){
    MaxTriggers = iniFile->intType("General","MaxTriggers",MAXTRIGGERS_V1190A);
}

// ****************************************************************************************************

Data32 DataReader::GetMaxTriggers(){
    return MaxTriggers;
}

// ****************************************************************************************************

void DataReader::SetVME()
{
    VME->init(iniFile);
}

// ****************************************************************************************************

void DataReader::SetTDC(){
    nTDCs = iniFile->intType("General","Tdcs",MINNTDC);
    TDCs->init(VME->GetHandle(),iniFile,nTDCs);

    /*********** initialize the TDC 1190a ***************************/
    TDCs->Set(iniFile,nTDCs);
}

// ****************************************************************************************************

void DataReader::Init(string inifilename) //Init VME and TDCs
{
    SetIniFile();
    SetMaxTriggers();
    SetVME();
    SetTDC();
}

// ****************************************************************************************************

void DataReader::Update(){
    iniFile->Read();
    SetMaxTriggers();
}

// ****************************************************************************************************

string DataReader::GetRunNumber(string runregistry){
    //Get the date first and then write it in the run registry along with the run number
    stringstream stream;
    //Get time information
    time_t t = time(nullptr);
    struct tm *Time = localtime(&t);
    int Y = Time->tm_year + 1900;
    int M = Time->tm_mon + 1;
    int D = Time->tm_mday;
    int h = Time->tm_hour;
    int m = Time->tm_min;
    int s = Time->tm_sec;

    //Set the Date
    string Date;

    stream << setfill('0') << setw(4) << Y << "/"
           << setfill('0') << setw(2) << M << "/"
           << setfill('0') << setw(2) << D << "-"
           << setfill('0') << setw(2) << h << ":"
           << setfill('0') << setw(2) << m << ":"
           << setfill('0') << setw(2) << s;

    stream >> Date;
    stream.clear();

    //Set the run number
    string RunNumber;

    stream << "run"
           << setfill('0') << setw(4) << Y
           << setfill('0') << setw(2) << M
           << setfill('0') << setw(2) << D
           << setfill('0') << setw(2) << h
           << setfill('0') << setw(2) << m
           << setfill('0') << setw(2) << s;

    stream >> RunNumber;
    stream.clear();

    //qDebug()<< RunNumber.c_str() << "into " << runregistry.c_str() <<endl;
    MSG_INFO("[DAQ]: Adding %s into %s\n",RunNumber.c_str(),runregistry.c_str());

    //Write this new run number in the run registry
    ofstream runregWrite(runregistry.c_str(),ios::app);
    runregWrite << RunNumber << '\t' << Date << '\t';

    return RunNumber;
}

// ****************************************************************************************************

string DataReader::GetFileName(string runregistry){
    //Write the run info in the run registry
    string RunNumber = GetRunNumber(runregistry.c_str());

    ofstream runregWrite(runregistry.c_str(),ios::app);
    runregWrite << iniFile->stringType("General","RunType","") << '\t'
                << iniFile->stringType("General","MaxTriggers","") << '\t'
                << iniFile->stringType("General","TriggerType","") << '\t'
                << iniFile->stringType("General","Threshold","") << '\t'
                << iniFile->stringType("General","Voltage","") << '\n';

    //Set the output filename
    string fNameParts[9];
    fNameParts[0] = iniFile->stringType("General","RunType","");
    fNameParts[1] = iniFile->stringType("General","ChamberType","");
    fNameParts[2] = iniFile->stringType("General","Mode","");
    fNameParts[3] = iniFile->stringType("General","Partition","");
    fNameParts[4] = iniFile->stringType("General","MaxTriggers","");
    fNameParts[5] = iniFile->stringType("General","TriggerType","");
    fNameParts[6] = iniFile->stringType("General","ElectronicsType","");
    fNameParts[7] = iniFile->stringType("General","Threshold","");
    fNameParts[8] = iniFile->stringType("General","Voltage","");

    for(int i=0; i<9;i++)
        if(fNameParts[i] != "")
            fNameParts[i] += "_";

    stringstream fNameStream;

    fNameStream << "datarun/";                                      //destination
    for(int i=0; i<9;i++)
        fNameStream << fNameParts[i];                               //run info
    fNameStream << RunNumber                                        //run number
                << ".root";                                         //extension

    string outputfName;
    fNameStream >> outputfName;

    return outputfName;
}

// ****************************************************************************************************

Uint DataReader::FraudDataGenerator(RAWData* DataList)
{
    /*//Cleaning all the vectors
    DataList->EventList->clear();
    DataList->NHitsList->clear();
    DataList->ChannelList->clear();
    DataList->TimeStampList->clear();
    DataList->StyleStampList->clear();*/

    uint               EventCount = 10;// int(rand() % 10);
    int               nHits = -8;
    vector<int>       TDCCh;
    vector<float>     TDCTS;
    vector<int>       TDCSS;


    for(uint i = 0; i < EventCount; i++)
    {
        DataList->EventList->push_back(TriggerCount + i);
        nHits = int(rand() % 10 / 4.0);  //Each Event only has one hit
        DataList->NHitsList->push_back(nHits);
        TDCCh.clear();
        TDCTS.clear();
        TDCSS.clear();
        for(int j = 0; j < nHits; j ++)
        {
            TDCCh.push_back(int(rand()%8));
            TDCCh.push_back(int(rand()%8) + 8);
            TDCTS.push_back(rand()%40 + 390.0);
            TDCTS.push_back(rand()%40 + 395.0);
            TDCSS.push_back(1); //Only generate leading edge signals
            TDCSS.push_back(1);
        }
        DataList->ChannelList->push_back(TDCCh);
        DataList->TimeStampList->push_back(TDCTS);
        DataList->StyleStampList->push_back(TDCSS);
    }
    TriggerCount += EventCount;
    return TriggerCount;
}

void DataReader::AquireData()
{
    if(RunFlag == true)
    {
        if(TriggerCount <= GetMaxTriggers())
        {
            if(VME->CheckIRQ())
             {
                 //Stop data acquisition with BUSY as VETO
                 VME->SendBUSY(ON);
                 usleep(2000);

                 Uint TempCount = TriggerCount;
                 //Read the data
                 TriggerCount = TDCs->Read(&TDCData,nTDCs);
                 if(TriggerCount != 0)
                 {
                     MSG_INFO("[DAQ]: %d / %d taken\n", TriggerCount, GetMaxTriggers());

                     hitRecon->process(&TDCData, TempCount, TriggerCount);

                     //hitRecon->process(&TDCData, 0, TriggerCount - TempCount);
                     }
                 else
                     MSG_INFO(".");

                 //Resume data taking
                 VME->SendBUSY(OFF);
             }
             else
                 MSG_INFO(".");
        }
        else
        {
            StopAndSaved();//Automatic stop when max trigger number aquired
        }
    }
}
void DataReader::ImportData()
{
    //uint temp_triggerCounts;
    if(ImportFlag == true)
    {
        //temp_triggerCounts = TriggerCount;
        for(uint i =0; i < EventNOOnceRead; i++)
        {
            if(importReader->Next())
            {
                TriggerCount++;
                TDCData.EventList->push_back(**im_EventNumber);
                TDCData.NHitsList->push_back(**im_NHits);
                TDCData.ChannelList->push_back(**im_Channel);
                TDCData.TimeStampList->push_back(**im_TimeStamp);
                TDCData.StyleStampList->push_back(**im_StyleStamp);
             }
            else
            {
                ImportFinishedFlag = true;
                ImportFlag = false;
                break;
            }
            outFile << "Event " << **im_EventNumber <<endl;
            outFile << "Hit,Ch,TS,SS,Layer,PhiorEta"<<endl;
            for(int j = 0; j<TDCData.NHitsList->at(i); j++)
            {
                outFile << j << ',';
                outFile << TDCData.ChannelList->at(i).at(j) << ',';
                outFile << TDCData.TimeStampList->at(i).at(j)<< ',';
                outFile << TDCData.StyleStampList->at(i).at(j)<< ',';
                outFile << int(TDCData.ChannelList->at(i).at(j)/32) << ',';
                if (int(TDCData.ChannelList->at(i).at(j)/16) % 2 == 0)
                    outFile << "Phi" << endl;
                else
                    outFile << "Eta" << endl;
            }
            outFile << endl;
            //outFile.close();
        }
        MSG_INFO("[DAQ]: %d / %lld taken. Time gap = %f\n", TriggerCount, (*importReader).GetEntries(true), mainClock.elapsed()/1000.0 - temp_time);
        if(ImportFinishedFlag)
        {
            hitRecon->process(&TDCData, 0, (*importReader).GetEntries(true)%EventNOOnceRead);
            //emit SendProcessedData(&(hitRecon->processedData), 0, int((*importReader).GetEntries(true)%EventNOOnceRead));
        }
        else
        {
            hitRecon->process(&TDCData, 0, EventNOOnceRead);
            //emit SendProcessedData(&(hitRecon->processedData), 0, int(EventNOOnceRead));
        }
        if(ImportFinishedFlag)
        {
            MSG_INFO("[DAQ]: All data have been read\n");
            emit DAQFinished(); // Send a signal to see the time cost
        }
        TDCData.EventList->clear();
        TDCData.NHitsList->clear();
        TDCData.ChannelList->clear();
        TDCData.TimeStampList->clear();
        TDCData.StyleStampList->clear();
        //timeCost[3][importLoopNO] = mainClock.elapsed();
    }
    importLoopNO ++;
    temp_time = mainClock.elapsed()/1000.0;
}
void DataReader::RunInit(bool importInifile)
{
    if(importInifile){
        Init("daqgifpp.ini");
    }
    MSG_INFO("[DAQ]: Starting data acquisition\n");
    MSG_INFO("[DAQ]: %d triggers will be taken\n", GetMaxTriggers());

    double temptime = mainClock.elapsed();
    //Cleaning all the vectors
    TDCData.EventList->clear();
    TDCData.NHitsList->clear();
    TDCData.ChannelList->clear();
    TDCData.TimeStampList->clear();
    TDCData.StyleStampList->clear();
    TDCCh.clear();
    TDCTS.clear();
    TDCSS.clear();
    MSG_INFO("[TimeRecorder]: Stage0, to clear vectors; Time cost: \f[ms]\n", mainClock.elapsed()-temptime);
    temptime = mainClock.elapsed();

    //Clear all the buffers while stopping data collection
    VME->SendBUSY(ON);
    TDCs->Clear(nTDCs);
    VME->SendBUSY(OFF);
    MSG_INFO("[TimeRecorder]: Stage1, TDC clearing RAM; Time cost: \f[ms]\n", mainClock.elapsed()-temptime);
    temptime = mainClock.elapsed();


    //Get the output file name and initialise the branches
    TriggerCount = 0;
    iniFile->SetFileName("daqgifpp.ini");
    MSG_INFO("[TimeRecorder]: Stage3, Init inifile; Time cost: \f[ms]\n", mainClock.elapsed()-temptime);
    temptime = mainClock.elapsed();

    outputFileName = GetFileName(".RunRegistry/RunRegistry.csv"); //Record Run的信息在csv文件，并且返回输出文件名
    outputFile = new TFile(outputFileName.c_str(), "RECREATE");
    RAWDataTree = new TTree("RAWData","RAWData");
    RAWDataTree->Branch("EventNumber",    &EventCount,  "EventNumber/I");
    RAWDataTree->Branch("number_of_hits", &nHits,       "number_of_hits/I");
    RAWDataTree->Branch("TDC_channel",    &TDCCh);
    RAWDataTree->Branch("TDC_TimeStamp",  &TDCTS);
    RAWDataTree->Branch("TDC_StyleStamp",  &TDCSS);
//    MSG_INFO("[TimeRecorder]: Stage4, Init Root file and TTrees; Time cost: \f[ms]\n", mainClock.elapsed()-temptime);
    temptime = mainClock.elapsed();

    //Read the output buffer until the min number of trigger is achieved
    //This part has been implemented by SIGNAL and SLOTs
}
void DataReader::ImportInit()
{
    SetIniFile();

    //Cleaning all the vectors
    TDCData.EventList->clear();
    TDCData.NHitsList->clear();
    TDCData.ChannelList->clear();
    TDCData.TimeStampList->clear();
    TDCData.StyleStampList->clear();

    ImportFlag = true;
    ImportFinishedFlag = false;
    TriggerCount = 0;
    importFile = new TFile(fileName.toStdString().c_str());
    importReader = new TTreeReader("RAWData", importFile);
    im_EventNumber = new TTreeReaderValue<Int_t> (*importReader, "EventNumber");
    im_NHits = new TTreeReaderValue<Int_t>(*importReader, "number_of_hits");
    im_Channel = new TTreeReaderValue<std::vector<Int_t>> (*importReader, "TDC_channel");
    im_TimeStamp = new TTreeReaderValue<std::vector<Float_t>> (*importReader, "TDC_TimeStamp");
    im_StyleStamp = new TTreeReaderValue<std::vector<Int_t>> (*importReader, "TDC_StyleStamp");
    MSG_INFO("[DAQ]: Starting data import\n");
    MSG_INFO("[DAQ]: %lld triggers will be imported\n", (*importReader).GetEntries(true));
}

void DataReader::RunDAQ(bool importInifile)
{
    Update();
    RunInit(importInifile);
    RunFlag = true;
}
void DataReader::ImportFile(QString filename) //slot
{
    fileName = filename;
    ImportInit();
}
void DataReader::StopAndSaved()
{
    //keep hanging on the VME, make it stop DAQing while waiting for next DAQ
    if(VME->CheckIRQ()){
        VME->SendBUSY(ON);
        usleep(2000);
        TDCs->Clear(nTDCs);
    }
    RunFlag = false;
    TriggerCount = 0;

    for(uint i=0; i<TDCData.EventList->size(); i++)
    {
        EventCount  = TDCData.EventList->at(i);
        nHits       = TDCData.NHitsList->at(i);
        TDCCh       = TDCData.ChannelList->at(i);
        TDCTS       = TDCData.TimeStampList->at(i);
        TDCSS       = TDCData.StyleStampList->at(i);

        RAWDataTree->Fill();
    }
//    RAWDataTree->Print();
    outputFile = RAWDataTree->GetCurrentFile();
    outputFile->Write(nullptr, TObject::kWriteDelete);

    //delete outputFile;
    MSG_INFO("Data saved in rootfile\n");

    TDCData.EventList->clear();
    TDCData.NHitsList->clear();
    TDCData.ChannelList->clear();
    TDCData.TimeStampList->clear();
    TDCData.StyleStampList->clear();

    emit DAQFinished(); // Send a signal to stop timer
}
void DataReader::RelayContext(QString context)
{
    emit SendContext(context);
}
void DataReader::RelaySendProcessedData(PROCESSEDData* processedData, int EventStartNo, int EventEndNo)
{
    emit SendProcessedData(processedData, EventStartNo, EventEndNo);
}

void DataReader::ClearVectors()
{

    TDCData.EventList->clear();
    TDCData.NHitsList->clear();
    TDCData.ChannelList->clear();
    TDCData.TimeStampList->clear();
    TDCData.StyleStampList->clear();
    TDCCh.clear();
    TDCTS.clear();
    TDCSS.clear();
}
