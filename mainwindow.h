﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QTime>
#include <QThread>
#include <QDialog>
#include "include/DataReader.h"
#include "hitreconstructor.h"
#include "mapsettings.h"
#include "layercontent.h"
#include <iomanip>
#include <QMessageBox>
#include "tcpserver.h"
#include <QPrinter>
#include <customhtml.h>
#include "tcpclient.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    QString Version = "V2.00D";
    DataReader *DR;
    QTimer *m_timer;
    explicit MainWindow(int argc, char** argv, QWidget *parent = nullptr);
    void perfInit();
    void DoubletPerfInit();

    void pdfCreate();
    void pdfCover();
    void pdfPage1(QCustomPlot** plotview, QString Title);
    void pdfPage2(QCustomPlot** plotviewEta, QCustomPlot** plotviewPhi, QString Title);
    void pdfBackCover();
    void bindShortCuts();
    int pdf_x = 0;
    int pdf_y = 0;
    int pdfHeight = 20;
    QTime *time;
    ~MainWindow();

signals:
    void SendContext(QString context);
    void SendRunDAQ(bool importInifile);
    void SendStopAndSave();
    void SendImportFile(QString filename);
    void SelfQuit();

private slots:
    void JudgeIfQuit();
    void ShowContext(QString context);
    void finishingDAQ();
    void layerInit(uint layerNO);
    void resetDisplay(uint layerNO);
    void resetDataReader();
    void on_importButton_clicked();
    void on_RefreshInterval_textEdited(const QString &arg1);
    void on_B_SaveChamberSetting_clicked();
    void on_B_AddMapSetting_clicked();
    void on_B_RunDAQ_clicked();
    void on_B_SaveMapSetting_clicked();
    void on_CB_PresetMapping_currentIndexChanged(int index);
    void on_SB_LESt_valueChanged(int arg1);
    void on_CB_EventsOnceImport_currentTextChanged(const QString &arg1);
    void on_CB_LEFEFlag_currentIndexChanged(int index);
    void on_B_Pause_clicked();
    void on_SB_LEEd_valueChanged(int arg1);
    void on_B_CSV_import_clicked();

private:
    Ui::MainWindow *ui;
    QThread *DRThread;
    QThread *TCPClientThread;
    layercontent *L[3];
    layercontent *P = nullptr;        //Performanc tab
    QThread *LThread[3];
    bool m_enslavedMode = false;
    bool m_runFlag;
    bool m_importFlag;
    bool m_firstRunFlag = true;
    bool m_firstImportFlag = true;
    bool m_firstInitFlag = true;
    int refreshInterval;
    QTextBrowser* UI_TB[3];
    QCustomPlot* UI_ChamberEfficiencyView[3];
    QCustomPlot* UI_GapEfficiencyView[3];
    QCustomPlot* UI_EtaEfficiencyView[3];
    QCustomPlot* UI_EtaRefEfficiencyView[3];
    QCustomPlot* UI_EtaHitView[3];
    QCustomPlot* UI_EtaChRefEffView[3];
    QCustomPlot* UI_PhiEfficiencyView[3];
    QCustomPlot* UI_PhiRefEfficiencyView[3];
    QCustomPlot* UI_PhiHitView[3];
    QCustomPlot* UI_PhiChRefEffView[3];
    QCustomPlot* UI_HitMap[3];
    QCustomPlot* UI_NoiseMap[3];
    QCustomPlot* UI_TOTMap[3];
    QCustomPlot* UI_PhiLEView[3];
    QCustomPlot* UI_EtaLEView[3];
    QCustomPlot* UI_PhiFEView[3];
    QCustomPlot* UI_EtaFEView[3];
    QCustomPlot* UI_PhiTOTView[3];
    QCustomPlot* UI_EtaTOTView[3];

    QCustomPlot* UI_PhiChTOTView[3];
    QCustomPlot* UI_EtaChTOTView[3];
    QCustomPlot* UI_PhiChTOFView[3];
    QCustomPlot* UI_EtaChTOFView[3];

    QCustomPlot* UI_PhiCSView[3];
    QCustomPlot* UI_EtaCSView[3];
    TCPServer* tcpServer;
    tcpClient* m_tcpClient;
    QPrinter* m_printer;
    QPainter* m_painter;
    QStringList splitContent;
    QString CSVFileName;
};

#endif // MAINWINDOW_H
