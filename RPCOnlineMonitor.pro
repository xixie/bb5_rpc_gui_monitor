#-------------------------------------------------
#
# Project created by QtCreator 2018-11-08T15:18:41
#
#-------------------------------------------------

QT       += core gui widgets charts printsupport network
TARGET = RPC_Online_Monitor
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#QMAKE_LFLAGS += "-Wl,-rpath=/usr/lib -Wl,-Bsymbolic"
win32:INCLUDEPATH += $$(ROOTSYS)/include

else: INCLUDEPATH += /home/xixie/root/include
      INCLUDEPATH += /home/shift/root/include


win32:LIBS += -L/home/xixie/Downloads/root/lib -llibCore -llibRIO -llibNet \
        -llibHist -llibGraf -llibGraf3d -llibGpad -llibTree \
        -llibRint -llibPostscript -llibMatrix -llibPhysics \
        -llibGui -llibRGL -llibTreePlayer
else:LIBS += -L/home/xixie/root/lib
     LIBS += -L/home/shift/root/lib -lCore -lRIO -lNet \
        -lHist -lGraf -lGraf3d -lGpad -lTree \
        -lRint -lPostscript -lMatrix -lPhysics \
        -lGui -lRGL -lMathCore -lThread -lImt -lTreePlayer -lMultiProc
     LIBS += -L/usr/lib/ -lCAENVME
    LIBS += -ltbb
    LIBS += -lstdc++

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        barChart.cpp \
        splineChart.cpp \
        splineChartView.cpp \
    src/DataReader.cxx \
    src/IniFile.cxx \
    src/qcustomplot.cpp \
    src/v1190a.cxx \
    src/v1718.cxx \
    hitmap.cpp \
    histogram.cpp \
    hitreconstructor.cpp \
    layercontent.cpp \
    tcpserver.cpp \
    mainwindow_slots.cpp \
    mainwindow_pdf.cpp \
    customhtml.cpp \
    tcpclient.cpp

HEADERS += \
        mainwindow.h \
    splineChart.h \
    splineChartView.h \
    barchart.h \
    include/MsgSvc.h \
    include/DataReader.h \
    include/IniFile.h \
    include/CAENVMEtypes.h \
    include/qcustomplot.h \
    hitmap.h \
    histogram.h \
    include/v1190a.h \
    include/v1718.h \
    hitreconstructor.h \
    mapsettings.h \
    layercontent.h \
    tcpserver.h \
    customhtml.h \
    tcpclient.h

FORMS += \
        mainwindow.ui



INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

RCC_DIR += $$PWD/tmp
MOC_DIR += $$PWD/tmp
OBJECTS_DIR += $$PWD/tmp

DISTFILES += \
    datarun/Mod2Int_Spydertest1_2000_5700V_run20190618163655.root \
    Mapping/Mod2_test.csv \
    daqgifpp.ini

