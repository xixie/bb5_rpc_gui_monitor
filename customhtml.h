#ifndef CUSTOMHTML_H
#define CUSTOMHTML_H
#include <QString>
#include <QDebug>
class customHTML : public QString
{
public:
    customHTML();
    ~customHTML();
    void InserText(QString fix, QString context, QString style="");
    void InsertPic(QString plotName);
    void Insert2Pic(QString plotName1, QString plotName2);
};

#endif // CUSTOMHTML_H
