#ifndef LAYERCONTENT_H
#define LAYERCONTENT_H
#include<QObject>
#include<QThread>
#include "histogram.h"
#include "hitmap.h"
#include "splineChart.h"

class layercontent : public QObject
{
    Q_OBJECT
public:
    explicit layercontent(QObject *parent = nullptr);
    hitMap* colorMap;
    hitMap* noiseMap;
    hitMap* TOTMap;
    histogram* m_PhiLE;
    histogram* m_EtaLE;
    histogram* m_PhiFE;
    histogram* m_EtaFE;
    histogram* m_PhiTOT;
    histogram* m_EtaTOT;
    histogram* m_PhiChTOT;
    histogram* m_EtaChTOT;
    histogram* m_PhiChTOF;
    histogram* m_EtaChTOF;
    histogram* m_PhiCS;
    histogram* m_EtaCS;
    histogram* m_ChamberEfficiency;
    histogram* m_GapEfficiency;
    histogram* m_EtaTriggerEfficiency;
    histogram* m_EtaRefTriggerEfficiency;
    histogram* m_EtaHit;
    histogram* m_EtaChRefEff;
    histogram* m_PhiTriggerEfficiency;
    histogram* m_PhiRefTriggerEfficiency;
    histogram* m_PhiHit;
    histogram* m_PhiChRefEff;

    histogram* p_PhiEff;
    histogram* p_EtaEff;
    histogram* p_PhiTriLE;
    histogram* p_EtaTriLE;
    histogram* p_PhiDouLE;
    histogram* p_EtaDouLE;
signals:

public slots:
};

#endif // LAYERCONTENT_H
