#ifndef LEADINGEDGE_H
#define LEADINGEDGE_H
#include "qcustomplot.h"
#include "hitreconstructor.h"
#include "QVector"
#include "v1190a.h"
#include <vector>
#include <sstream>
#include <TH1F.h>
#include <TF1.h>
#include <TMath.h>
#define TOP 0
#define MID 1
#define BOT 2
#define TYPE_LE 0
#define TYPE_FE 1
#define TYPE_CS 2
#define TYPE_TOT 3
#define TYPE_EFF 4
#define TYPE_PEFF 5
#define TYPE_PTriLE 6
#define TYPE_PDouLE 7
#define TYPE_RefEFF 8
#define TYPE_Hit 9
#define TYPE_ChRefEff 10
#define TYPE_ChTOT 11
#define TYPE_ChTOF 12
#define TYPE_PHI false
#define TYPE_ETA true
class histogram : public QWidget
{
    Q_OBJECT

public:
    histogram(int type, QCustomPlot *customPlot0, QWidget *parent = nullptr);
    void init(double st, double ed, double binwidth, size_t layerno, bool PhiorEta);
    void init(double st, double ed, double binwidth, size_t layerno, bool PhiorEta, size_t totallayerno);
    void init(double st, double ed, double binwidth, size_t layerno); //For Eff only
    bool trippleTriggered(PROCESSEDData *Data, int eventNO);
    bool doubleTriggered(PROCESSEDData *Data, int eventNO, int layerno1, int layerno2);
    bool singleTriggered(PROCESSEDData *Data, int eventNO, int layerno);
    float TripletLE(PROCESSEDData *Data, int eventNO);
    float DoubletLE(PROCESSEDData *Data, int eventNO, int layer1, int layer2);
    void clearDisplay();

public slots:
    void refreshLE(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshFE(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshTOT(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshCS(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshTriEff(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshEtaOrPhiEff(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshEtaOrPhiRefEff(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshChamberRefEff(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshGapRefEff(PROCESSEDData* Data, int EventStartNo, int EventEndNo);
    void refreshPerfEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo);
    void refreshPerfTriLE(PROCESSEDData *Data, int EventStartNo, int EventEndNo);
    void refreshPerfDouLE(PROCESSEDData *Data, int EventStartNo, int EventEndNo);
    void refreshEtaOrPhiHit(PROCESSEDData *Data, int EventStartNo, int EventEndNo);
    void refreshEtaOrPhiChTOT(PROCESSEDData *Data, int EventStartNo, int EventEndNo);
    void refreshEtaOrPhiChTOF(PROCESSEDData *Data, int EventStartNo, int EventEndNo);
    void refreshEtaOrPhiChannelRefEff(PROCESSEDData *Data, int EventStartNo, int EventEndNo);
private:
    QCustomPlot *customPlot;
    QVector<double> x1, y1;
    QVector<double> x2, y2, y3;
    QVector<QVector<double>> stat;
    QVector<double> error;
    double y1Error = 0;
    QCPGraph *graph1;
    QCPBars *bars1;
    QCPTextElement* title;
    std::stringstream titleText;
    double startPoint;
    double endPoint;
    int binNumber;
    double binGap;
    bool binSettedFlag;
    int totalHits;
    int signalHits;
    size_t layerNO;
    size_t totalLayerNO;
    void setTitle(double mean, double sigma);
    void setTitle(double mean);
    void setTitle();
    void setEffTitle(double eff);
    void setTriEffTitle(double eff);
    void setChamberRefEffTitle(double eff);
    void setGapRefEffTitle(double eff);
    bool PHIorETA;
    int TYPE;
    TH1F *hist;
    TF1 *fit;
    int totalEventNumber;
    int triggeredEventNumber;
    int EffBinNO;
    int EffShowNO;
    QCPErrorBars *errorBars = nullptr;
signals:
    void sendReplot(uint l, bool phioreta);
    void sendReplot(uint l);
};

#endif // LEADINGEDGE_H
