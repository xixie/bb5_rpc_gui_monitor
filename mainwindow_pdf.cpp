#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::pdfCreate()
{
    QString QAfolder = "QuickAnalysis";
    QDir dir;
    if(!dir.exists(QAfolder)){
        if(!dir.mkdir(QAfolder)){
            qDebug()<<"QuickAnalysis folder creating failed.";
            return;
        }
    }
    dir.setCurrent(QAfolder);

    m_printer = new QPrinter;
    m_painter = new QPainter;
    m_printer->setPageSize(QPrinter::A4);
    m_printer->setResolution(300);
    m_printer->setOutputFormat(QPrinter::PdfFormat);
    QStringList inputname =(QString::fromStdString(DR->outputFileName)).split("/");
    QString pdfName =".pdf";
    if(inputname.at(inputname.size()-1).size()>0)
          pdfName.insert(0,inputname.at(inputname.size()-1));
    else
        pdfName.insert(0,"blankName");
    m_printer->setOutputFileName(pdfName);

    m_painter->begin(m_printer);
    pdfCover();
    pdfPage1(UI_ChamberEfficiencyView,"Chamber Efficiency");
    pdfPage1(UI_GapEfficiencyView,"Gas Gap Efficiency");
    pdfPage1(UI_HitMap,"2D Hit Map");
    pdfPage1(UI_NoiseMap,"2D Noise Map");
    pdfPage1(UI_TOTMap,"2D TOT Map");
    pdfPage2(UI_EtaRefEfficiencyView, UI_PhiRefEfficiencyView, "Eta/Phi Reference Efficiency");
    pdfPage2(UI_EtaEfficiencyView, UI_PhiEfficiencyView, "Eta/Phi Efficiency");
    pdfPage2(UI_EtaCSView, UI_PhiCSView, "Eta/Phi CS");
    pdfPage2(UI_EtaHitView, UI_PhiHitView, "Eta/Phi Hit Counting");
    pdfPage2(UI_EtaChRefEffView, UI_PhiChRefEffView, "Eta/Phi Channel Reference Efficiency");
    pdfPage2(UI_EtaChTOTView, UI_PhiChTOTView, "Eta/Phi Channel TOT distribution");
    pdfPage2(UI_EtaChTOFView, UI_PhiChTOFView, "Eta/Phi Channel TOF distribution (comparing to reference layer)");
    pdfPage2(UI_EtaLEView, UI_PhiLEView, "Eta/Phi LE Distribution");
    pdfPage2(UI_EtaFEView, UI_PhiFEView, "Eta/Phi FE Distribution");
    pdfPage2(UI_EtaTOTView, UI_PhiTOTView, "Eta/Phi TOT Distribution");
    pdfBackCover();
    m_painter->end();
    dir.setCurrent("../");
    MSG_INFO("[Monitor]: Quick Analysis Report generated: %s", pdfName.toStdString().c_str());
}

void MainWindow:: pdfCover()
{
    QTextDocument document;
    document.documentLayout()->setPaintDevice(m_printer);

    QString html = "";
    html += "<html>"
            "<body>"
            "<table width=100% border=1 cellspacing=1 cellpadding=4 align=center>"
            "<tr>"
            "<td align=center valign=bottom>"
            "<strong style=\"font-size: 150px;\">Quick Analysis Report</strong><br />"
            "</td>"
            "</tr>"
            "</table>"

            "<table width=100% border=1 cellspacing=1 cellpadding=4 align=center>"
            "<tr>"
            "<td align=center valign=middle>"
            "<strong style=\"font-size: 60px;\">"
            "Data file: "
            "</strong><br />"
            "</td>"

            "<td valign=middle>";
    QStringList inputname =(QString::fromStdString(DR->outputFileName)).split("/");
            html+="<strong style=\"font-size: 60px;\">";
            if(inputname.size()>0)
                html.append(inputname.at(inputname.size()-1));
            html += "</strong><br />"
            "</td>"
            "</tr>"

            "<tr>"
            "<td align=center width=120 valign=middle>"
            "<strong style=\"font-size: 60px;\">"
            "Monitor Version: "
            "</strong><br />"
            "</td>"
            "<td align=middle valign=middle>"
            "<strong style=\"font-size: 60px;\">";
            html+=Version;
            html+=
            "</strong><br />"
            "</td>"
            "</tr>"

            "<tr>"
            "<td align=center valign=middle>"
            "<strong style=\"font-size: 60px;\">"
            "GitLab: "
            "</strong><br />"
            "</td>"
            "<td align=left valign=middle>"
            "<strong style=\"font-size: 60px;\">"
            "https://gitlab.cern.ch/xixie/bb5_rpc_gui_monitor"
            "</strong><br />"
            "</td>"
            "</tr>"

            "</table>"
            "</body>"
            "</html>";
    document.setHtml(html);
    QRectF titleRect(0.1 *m_printer->width(), 0.2*m_printer->height(), 0.8* m_printer->width(), 0.6*m_printer->height());
    document.setPageSize(titleRect.size());
    m_painter->translate(titleRect.topLeft());
    document.drawContents(m_painter);
    document.end();
    m_painter->resetTransform();
}

void MainWindow::pdfBackCover()
{
    m_printer->newPage();
    QTextDocument document;
    document.documentLayout()->setPaintDevice(m_printer);
    customHTML html;
    html.clear();
    html += "<html>"
            "<body>";
    html += "<table width=100% border=1 cellspacing=1 cellpadding=4 align=center>" ;
    QStringList temp = CSVFileName.split("/");
    QString title = "The imported Mapping CSV file: " + temp.at(temp.size()-1);
    html.append("<tr>");
    html.InserText("<th>", "<strong style=\"font-size: 60px;\">"+title +"</strong><br />", " align=center valign=bottom");
    html.append("</tr>"
                "</tr>"
                "</table>"

                "<table width=100% border=1 cellspacing=1 cellpadding=1 align=center>");
    for(int i = 0; i < splitContent.count(); i++){
        html.append("<tr>");
        QStringList lineContent = splitContent.at(i).split(",");
        for(int j =0; j < lineContent.size(); j++){
            html.InserText("<td>", "<strong style=\"font-size: 35px;\">"+lineContent.at(j)+"</strong><br />");
        }
        for(int j = lineContent.size(); j < 8; j++){
            html.InserText("<td>", "<strong style=\"font-size: 35px;\">"" ""</strong><br />");
        }
        html.append("</tr>");
    }
    html +=
            "</body>"
            "</html>";
    document.setHtml(html);
    QRectF titleRect(0.0*m_printer->width(), 0.0*m_printer->height(), 1* m_printer->width(), 1.0*m_printer->height());
    m_painter->translate(titleRect.topLeft());
    document.drawContents(m_painter);
    document.end();
    m_painter->resetTransform();
}

void MainWindow::pdfPage1(QCustomPlot** plotview, QString Title)
{
    m_printer->newPage();
    QTextDocument document;

    document.documentLayout()->setPaintDevice(m_printer);
    document.setPageSize(m_printer->pageRect().size());
    QStringList plotNameList = {"./0.jpg", "./1.jpg", "./2.jpg"};
    for(uint l = 0; l < LAYERNOS; l++){
        plotview[l]->saveJpg(plotNameList.at(l),600*1.5, 300*1.5, 1.0, 100);
    }
    customHTML html;
    html.clear();
    html += "<html>"
            "<body>";
    html += "<table width=80% border=1 cellspacing=1 cellpadding=4 align=center>" ;
    html.append("<tr>");
    html.InserText("<th>", "<strong style=\"font-size: 60px;\">"+Title +"</strong><br />", " align=center valign=bottom");
    html.append("</tr>");
    for(int l = 0; l < LAYERNOS; l++){
        html.append("<tr>");
        html.InserText("<td>", "Layer" + QString::number(l) + ": ");
        html.append("</tr>");
        html.InsertPic(plotNameList[l]);
    }
    html +=
            "</body>"
            "</html>";
    document.setHtml(html);
    QRectF titleRect(0.0*m_printer->width(), 0.02*m_printer->height(), 1* m_printer->width(), m_printer->height());
    m_painter->translate(titleRect.topLeft());
    document.drawContents(m_painter);
    document.end();
    m_painter->resetTransform();
    for(int l=0; l < 3; l++){
        QFile temp(plotNameList[l]);
        if(temp.exists()){
            temp.remove();
        }
    }
}
void MainWindow::pdfPage2(QCustomPlot** plotviewEta, QCustomPlot** plotviewPhi, QString Title)
{
    m_printer->newPage();
    QTextDocument document;

    document.documentLayout()->setPaintDevice(m_printer);
    document.setPageSize(m_printer->pageRect().size());
    QStringList plotNameListEta = {"./Eta0.jpg", "./Eta1.jpg", "./Eta2.jpg"};
    QStringList plotNameListPhi = {"./Phi0.jpg", "./Phi1.jpg", "./Phi2.jpg"};
    for(uint l = 0; l < LAYERNOS; l++){
        plotviewEta[l]->saveJpg(plotNameListEta.at(l),370*1.5, 300*1.5, 1.0, 100);
        plotviewPhi[l]->saveJpg(plotNameListPhi.at(l),370*1.5, 300*1.5, 1.0, 100);
    }
    customHTML html;
    html.clear();
    html += "<html>"
            "<body>";
    //html.InserText("<h2>", Title, " align=center font-size: 200%");
    html += //"<hr size=1 width=100%/>"
            "<table width=100% border=1 cellspacing=1 cellpadding=4 align=center>" ;
    html.append("<tr>");
    html.InserText("<th>", "<strong style=\"font-size: 60px;\">"+Title +"</strong><br />", " align=center valign=bottom");
    html.append("</tr>");
    html+="</table>"
          "<table width=100% border=1 cellspacing=1 cellpadding=4 align=center>";
    for(int l = 0; l < LAYERNOS; l++){
        html.append("<tr>");
        html.InserText("<td>", "Layer" + QString::number(l) + " Eta: ");
        html.InserText("<td>", "Layer" + QString::number(l) + " Phi: ");
        html.append("</tr>");
        html.Insert2Pic(plotNameListEta[l], plotNameListPhi[l]);
    }
    html +=
            "</table>"
            "</body>"
            "</html>";
    document.setHtml(html);
    QRectF titleRect(0.0*m_printer->width(), 0.02*m_printer->height(), 1* m_printer->width(), m_printer->height());
    m_painter->translate(titleRect.topLeft());
    document.drawContents(m_painter);
    document.end();
    m_painter->resetTransform();
    for(int l=0; l < 3; l++){
        QFile temp1(plotNameListEta[l]);
        QFile temp2(plotNameListPhi[l]);
        if(temp1.exists()){
            temp1.remove();
        }
        if(temp2.exists()){
            temp2.remove();
        }
    }
}
