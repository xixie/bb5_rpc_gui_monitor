#include "mainwindow.h"
#include <iostream>
#include <QApplication>
#include <stdio.h>
#include <QThread>
#include <TROOT.h>

//argv[1] indicates if works at enslaved mode (After DAQ finished, the program automatically quit)
int main(int argc, char **argv)
{
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(true);
    MainWindow *w = new MainWindow(argc, argv);
    QObject::connect(w, SIGNAL(SelfQuit()), &a, SLOT(quit()));

    w->show();
    return a.exec();
}
