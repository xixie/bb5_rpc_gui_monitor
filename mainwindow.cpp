#include "mainwindow.h"
#include "ui_mainwindow.h"

//Please don't adjust these four vectors
vector<size_t>         Phi_st_list = { 0, 32, 64};
vector<size_t>         Phi_ed_list = {15, 47, 79};
vector<size_t>         Eta_st_list = {16, 48, 80};
vector<size_t>         Eta_ed_list = {31, 63, 95};
size_t LAYERNOS;
size_t PHISTRIPNOS;
size_t ETASTRIPNOS;
size_t PHISTARTSTRIPNO = 1;
size_t ETASTARTSTRIPNO = 1;
std::vector<std::map<size_t, size_t>> PHITDCMAP; //Contain all layers PhiMap
std::vector<std::map<size_t, size_t>> ETATDCMAP; //Contain all layers EtaMap
std::vector<std::map<size_t, size_t>> PHISTRIPMAP; //Contain all layers PhiMap
std::vector<std::map<size_t, size_t>> ETASTRIPMAP; //Contain all layers EtaMap
std::vector<double>                              PHISTRIPWIDTH;
std::vector<double>                              ETASTRIPWIDTH;
std::vector<double>                              PHIGAPWIDTH;
std::vector<double>                              ETAGAPWIDTH;

bool MAP_ADD_STATUS;
bool MAP_BEEN_SET = false;   //Flag to judge if it's set
double LeadginEdgeSt = 0.0;
double LeadingEdgeEd = 50.0;
uint EventNOOnceRead = 200;
int LEFlag = 1;
int FEFlag = 0;
QTime mainClock = *new QTime();
//float timeCost[10][500];
int     importLoopNO = 0;
float X0 = 0.0;
float Y0 = 0.0;
float Z0 = 0.0;
float SingletHeight = 30.0;
float StripWidth = 25.0;
float StripGap = 2.0;

int LAYERREFERENCE[3] = {0};

MainWindow::MainWindow(int argc, char **argv, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("RPC Online Monitor " + Version);
    m_runFlag = false;
    m_importFlag = false;
    refreshInterval = 2000;
    ui->RefreshInterval->setText("2000");
    ui->B_AddMapSetting->setEnabled(false);
    ui->B_SaveMapSetting->setEnabled(false);
    ui->B_RunDAQ->setEnabled(false);
    ui->importButton->setEnabled(false);
    MAP_ADD_STATUS = false;
    MAP_BEEN_SET = false;
    connect(this, SIGNAL(SendContext(QString)), this, SLOT(ShowContext(QString)));

    bindShortCuts();

    //Global Refresh Clock
    m_timer = new QTimer(this);
    time = new QTime();
    time->start();
    mainClock.start();

    DR = new DataReader();
    DRThread = new QThread();
    DR->moveToThread(DRThread);
    DRThread->start();

    tcpServer = new TCPServer(ui->LE_LocalIP, ui->LE_ListeningPort, ui->B_TCPConnect, ui->B_TCPSend);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), tcpServer, SLOT(SendTracks(PROCESSEDData*, int, int)));

    //Connect DAQ signals and slots
    connect(this, SIGNAL(SendRunDAQ(bool)), DR, SLOT(RunDAQ(bool)));
    connect(m_timer, SIGNAL(timeout()), DR, SLOT(AquireData()));
    connect(this, SIGNAL(SendStopAndSave()), DR, SLOT(StopAndSaved()));
    connect(DR, SIGNAL(DAQFinished()), this, SLOT(finishingDAQ()));

    //Connect Import signals and files
    connect(this, SIGNAL(SendImportFile(QString)), DR, SLOT(ImportFile(QString)));
    connect(m_timer, SIGNAL(timeout()), DR, SLOT(ImportData()));
    connect(DR, SIGNAL(SendContext(QString)), this, SLOT(ShowContext(QString)));

//    connect(this->m_timer, SIGNAL(timeout()), this, SLOT(JudgeIfQuit()));



    if(argc > 1){
        m_enslavedMode = true;
        char test[] = "enslaved";
        for(int i = 0; i < 8; i++){
            if(argv[1][i]!=test[i]) m_enslavedMode = false;
        }
    }

    // CAEN TDC mode
//    m_enslavedMode = true;
    if(m_enslavedMode&&argc==2){
        on_B_CSV_import_clicked();
        ui->B_CSV_import->setEnabled(false);
        ui->B_RunDAQ->setEnabled(false);
        ui->importButton->setEnabled(false);
        SendRunDAQ(true);
    }

    m_timer->start(refreshInterval);
    // TCP clien mode, for HP TDC
    if(/* DISABLES CODE */ (true)||(m_enslavedMode&&argc==4)){
        m_timer->stop();
        m_tcpClient = new tcpClient();
        connect(m_tcpClient, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), DR, SLOT(RelaySendProcessedData(PROCESSEDData*, int, int)));
        connect(m_tcpClient, SIGNAL(SendContext(QString)), this, SLOT(ShowContext(QString)));
        connect(m_tcpClient, SIGNAL(ReceiveFinished()), this, SLOT(finishingDAQ()));
        m_tcpClient->Init();
        layerInit(LAYERNOS);

        ui->CB_PresetMapping->setEnabled(false);
        ui->B_CSV_import->setEnabled(false);
        ui->B_RunDAQ->setEnabled(false);
        ui->importButton->setEnabled(false);
        if(m_tcpClient->ConnectServer(QString(argv[2]), quint16(QString(argv[3]).toUInt()))){

        }
        else{
//            emit SelfQuit();
//            exit(-1);
        }
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::JudgeIfQuit()
{
    //Automatically quit if this programs running longer than a certain time
//    std::cout << "[Debug]: time->elapsed() = " << time->elapsed()/1000. << std::endl;
//    if(time->elapsed()/1000>=1200&&m_enslavedMode){
//        emit SelfQuit();
//    }
}

void MainWindow::ShowContext(QString context)
{
    ui->L0_TB_MSG->insertPlainText(context);
    ui->L0_TB_MSG->moveCursor(QTextCursor::End);
    ui->L1_TB_MSG->insertPlainText(context);
    ui->L1_TB_MSG->moveCursor(QTextCursor::End);
    ui->L2_TB_MSG->insertPlainText(context);
    ui->L2_TB_MSG->moveCursor(QTextCursor::End);
    ui->P_TB_MSG->insertPlainText(context);
    ui->P_TB_MSG->moveCursor(QTextCursor::End);
}

void MainWindow::finishingDAQ()
{
    m_timer->stop();
    if(m_firstRunFlag == true){
        m_firstRunFlag = false;
    }
    ui->importButton->setEnabled(true);
    m_runFlag = false;
    ui->B_RunDAQ->setText("Reset&DAQ");
//    qDebug()<<time->elapsed()/1000.0<<"s";
    pdfCreate();
    if(m_enslavedMode == true){
        //Only run once
        emit SelfQuit();
    }
}

void MainWindow::layerInit(uint layerNOs)
{
    for(uint i = 0; i <layerNOs; i++)
    {
        L[i] = new layercontent();
        LThread[i] = new QThread();

        //Trigger Efficiency Algorithm: Eta&&Phi
        L[i]->m_ChamberEfficiency = new histogram(TYPE_EFF, UI_ChamberEfficiencyView[i], this);
        L[i]->m_ChamberEfficiency->init(1, 120, 1, i);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_ChamberEfficiency, SLOT(refreshChamberRefEff(PROCESSEDData*, int, int)));

        L[i]->m_GapEfficiency = new histogram(TYPE_EFF, UI_GapEfficiencyView[i], this);
        L[i]->m_GapEfficiency->init(1, 120, 1, i);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_GapEfficiency, SLOT(refreshGapRefEff(PROCESSEDData*, int, int)));

        L[i]->m_EtaTriggerEfficiency = new histogram(TYPE_EFF, UI_EtaEfficiencyView[i], this);
        L[i]->m_EtaTriggerEfficiency->init(1, 120, 1, i, TYPE_ETA);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_EtaTriggerEfficiency, SLOT(refreshEtaOrPhiEff(PROCESSEDData*, int, int)));

        L[i]->m_EtaRefTriggerEfficiency = new histogram(TYPE_RefEFF, UI_EtaRefEfficiencyView[i], this);
        L[i]->m_EtaRefTriggerEfficiency->init(1, 120, 1, i, TYPE_ETA, layerNOs);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_EtaRefTriggerEfficiency, SLOT(refreshEtaOrPhiRefEff(PROCESSEDData*, int, int)));

        L[i]->m_PhiTriggerEfficiency = new histogram(TYPE_EFF, UI_PhiEfficiencyView[i], this);
        L[i]->m_PhiTriggerEfficiency->init(1, 120, 1, i, TYPE_PHI);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_PhiTriggerEfficiency, SLOT(refreshEtaOrPhiEff(PROCESSEDData*, int, int)));

        L[i]->m_PhiRefTriggerEfficiency = new histogram(TYPE_RefEFF, UI_PhiRefEfficiencyView[i], this);
        L[i]->m_PhiRefTriggerEfficiency->init(1, 120, 1, i, TYPE_PHI, layerNOs);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_PhiRefTriggerEfficiency, SLOT(refreshEtaOrPhiRefEff(PROCESSEDData*, int, int)));

        L[i]->m_PhiHit = new histogram(TYPE_Hit, UI_PhiHitView[i], this);
        L[i]->m_EtaHit = new histogram(TYPE_Hit, UI_EtaHitView[i], this);
        L[i]->m_PhiHit->init(PHISTARTSTRIPNO - 1, PHISTRIPNOS+PHISTARTSTRIPNO, 0.2, i, TYPE_PHI);
        L[i]->m_EtaHit->init(ETASTARTSTRIPNO - 1, ETASTRIPNOS+ETASTARTSTRIPNO, 0.2, i, TYPE_ETA);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_EtaHit, SLOT(refreshEtaOrPhiHit(PROCESSEDData*, int, int)));
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_PhiHit, SLOT(refreshEtaOrPhiHit(PROCESSEDData*, int, int)));

        L[i]->m_EtaChTOT = new histogram(TYPE_ChTOT, UI_EtaChTOTView[i], this);
        L[i]->m_PhiChTOT = new histogram(TYPE_ChTOT, UI_PhiChTOTView[i], this);
        L[i]->m_EtaChTOT->init(ETASTARTSTRIPNO - 1, ETASTRIPNOS+ETASTARTSTRIPNO, 0.2, i, TYPE_ETA);
        L[i]->m_PhiChTOT->init(PHISTARTSTRIPNO - 1, PHISTRIPNOS+PHISTARTSTRIPNO, 0.2, i, TYPE_PHI);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_EtaChTOT, SLOT(refreshEtaOrPhiChTOT(PROCESSEDData*, int, int)));
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_PhiChTOT, SLOT(refreshEtaOrPhiChTOT(PROCESSEDData*, int, int)));

        L[i]->m_EtaChTOF = new histogram(TYPE_ChTOF, UI_EtaChTOFView[i], this);
        L[i]->m_PhiChTOF = new histogram(TYPE_ChTOF, UI_PhiChTOFView[i], this);
        L[i]->m_EtaChTOF->init(ETASTARTSTRIPNO - 1, ETASTRIPNOS+ETASTARTSTRIPNO, 0.2, i, TYPE_ETA);
        L[i]->m_PhiChTOF->init(PHISTARTSTRIPNO - 1, PHISTRIPNOS+PHISTARTSTRIPNO, 0.2, i, TYPE_PHI);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_EtaChTOF, SLOT(refreshEtaOrPhiChTOF(PROCESSEDData*, int, int)));
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_PhiChTOF, SLOT(refreshEtaOrPhiChTOF(PROCESSEDData*, int, int)));

        L[i]->m_PhiChRefEff = new histogram(TYPE_ChRefEff, UI_PhiChRefEffView[i], this);
        L[i]->m_EtaChRefEff = new histogram(TYPE_ChRefEff, UI_EtaChRefEffView[i], this);
        L[i]->m_PhiChRefEff->init(PHISTARTSTRIPNO - 1, PHISTRIPNOS+PHISTARTSTRIPNO, 0.2, i, TYPE_PHI);
        L[i]->m_EtaChRefEff->init(ETASTARTSTRIPNO - 1, ETASTRIPNOS+ETASTARTSTRIPNO, 0.2, i, TYPE_ETA);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_EtaChRefEff, SLOT(refreshEtaOrPhiChannelRefEff(PROCESSEDData*, int, int)));
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->m_PhiChRefEff, SLOT(refreshEtaOrPhiChannelRefEff(PROCESSEDData*, int, int)));

        //hitmap
        QCPColorScale *colorScale = new QCPColorScale(UI_HitMap[i]);
        UI_HitMap[i]->xAxis->setLabel("Hit Map: Eta");
        UI_HitMap[i]->yAxis->setLabel("Hit Map: Phi");
        UI_HitMap[i]->plotLayout()->addElement(0, 1, colorScale);
        L[i]->colorMap = new hitMap(UI_HitMap[i], UI_HitMap[i]->xAxis, UI_HitMap[i]->yAxis);
        L[i]->colorMap->setColorScale(colorScale);
        L[i]->colorMap->init(i);
        UI_HitMap[i]->rescaleAxes();
        UI_HitMap[i]->replot();
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->colorMap, SLOT(refreshHitMap(PROCESSEDData*, int, int)));

        //noisemap
        QCPColorScale *colorScale1 = new QCPColorScale(UI_NoiseMap[i]);
        UI_NoiseMap[i]->xAxis->setLabel("Noise Map: Eta direction");
        UI_NoiseMap[i]->yAxis->setLabel("Noise Map: Phi direction");
        UI_NoiseMap[i]->plotLayout()->addElement(0, 1, colorScale1);
        L[i]->noiseMap = new hitMap(UI_NoiseMap[i], UI_NoiseMap[i]->xAxis, UI_NoiseMap[i]->yAxis);
        L[i]->noiseMap->setColorScale(colorScale1);
        L[i]->noiseMap->init(i);
        UI_NoiseMap[i]->rescaleAxes();
        UI_NoiseMap[i]->replot();
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->noiseMap, SLOT(refreshNoiseMap(PROCESSEDData*, int, int)));

        //TOT map
        QCPColorScale *colorScale2 = new QCPColorScale(UI_TOTMap[i]);
        UI_TOTMap[i]->xAxis->setLabel("TOT Map: Eta direction");
        UI_TOTMap[i]->yAxis->setLabel("TOT Map: Phi direction");
        UI_TOTMap[i]->plotLayout()->addElement(0, 1, colorScale2);
        L[i]->TOTMap = new hitMap(UI_TOTMap[i], UI_TOTMap[i]->xAxis, UI_TOTMap[i]->yAxis);
        L[i]->TOTMap->setColorScale(colorScale2);
        L[i]->TOTMap->init(i);
        UI_TOTMap[i]->rescaleAxes();
        UI_TOTMap[i]->replot();
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), L[i]->TOTMap, SLOT(refreshTOTMap(PROCESSEDData*, int, int)));

        //leading edge
        L[i]->m_PhiLE = new histogram(TYPE_LE, UI_PhiLEView[i]);
        L[i]->m_EtaLE = new histogram(TYPE_LE, UI_EtaLEView[i]);
        L[i]->m_PhiLE->init(LeadginEdgeSt, LeadingEdgeEd, 2, i, TYPE_PHI);
        L[i]->m_EtaLE->init(LeadginEdgeSt, LeadingEdgeEd, 2, i, TYPE_ETA);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*,int,int)), L[i]->m_PhiLE, SLOT(refreshLE(PROCESSEDData*, int, int)));
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*,int,int)), L[i]->m_EtaLE, SLOT(refreshLE(PROCESSEDData*, int, int)));

        //falling edge
        L[i]->m_PhiFE = new histogram(TYPE_FE, UI_PhiFEView[i]);
        L[i]->m_EtaFE = new histogram(TYPE_FE, UI_EtaFEView[i]);
        L[i]->m_PhiFE->init(LeadginEdgeSt, LeadingEdgeEd, 2, i, TYPE_PHI);
        L[i]->m_EtaFE->init(LeadginEdgeSt, LeadingEdgeEd, 2, i, TYPE_ETA);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*,int,int)), L[i]->m_PhiFE, SLOT(refreshFE(PROCESSEDData*, int, int)));
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*,int,int)), L[i]->m_EtaFE, SLOT(refreshFE(PROCESSEDData*, int, int)));

        //time over threshold
        L[i]->m_PhiTOT = new histogram(TYPE_TOT, UI_PhiTOTView[i]);
        L[i]->m_PhiTOT->init(0.0, 100.0, 2, i, TYPE_PHI);
        L[i]->m_EtaTOT = new histogram(TYPE_TOT, UI_EtaTOTView[i]);
        L[i]->m_EtaTOT->init(0.0, 100.0, 2, i, TYPE_ETA);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*,int,int)), L[i]->m_PhiTOT, SLOT(refreshTOT(PROCESSEDData*, int, int)));
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*,int,int)), L[i]->m_EtaTOT, SLOT(refreshTOT(PROCESSEDData*, int, int)));


        //Cluster size
        L[i]->m_PhiCS = new histogram(TYPE_CS, UI_PhiCSView[i]);
        L[i]->m_EtaCS = new histogram(TYPE_CS, UI_EtaCSView[i]);
        L[i]->m_PhiCS->init(0, PHISTRIPNOS + 1, 1, i, TYPE_PHI);
        L[i]->m_EtaCS->init(0, ETASTRIPNOS + 1, 1, i, TYPE_ETA);
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*,int,int)), L[i]->m_PhiCS, SLOT(refreshCS(PROCESSEDData*, int, int)));
        connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*,int,int)), L[i]->m_EtaCS, SLOT(refreshCS(PROCESSEDData*, int, int)));

        L[i]->moveToThread(LThread[i]);
        LThread[i]->start();
    }
    if(layerNOs < 3)
        ui->tabWidget->setTabEnabled(2, false);
    if(layerNOs < 2)
        ui->tabWidget->setTabEnabled(1, false);
    if(layerNOs == 3){
        perfInit();
    }
    if(layerNOs == 2){
        DoubletPerfInit();
    }
}

void MainWindow::perfInit()
{
    int LErange = 5;
    P = new layercontent();
    P->p_PhiEff = new histogram(TYPE_PEFF, ui->P_PhiEffView);
    P->p_PhiEff->init(0, 120, 1, 100, TYPE_PHI);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), P->p_PhiEff, SLOT(refreshPerfEff(PROCESSEDData*, int, int)));

    P->p_EtaEff = new histogram(TYPE_PEFF, ui->P_EtaEffView);
    P->p_EtaEff->init(0, 120, 1, 100, TYPE_ETA);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), P->p_EtaEff, SLOT(refreshPerfEff(PROCESSEDData*, int, int)));

    P->p_PhiTriLE = new histogram(TYPE_PTriLE, ui->P_PhiTriLEView);
    P->p_PhiTriLE->init(-LErange, LErange, 0.2, 200, TYPE_PHI);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), P->p_PhiTriLE, SLOT(refreshPerfTriLE(PROCESSEDData*, int, int)));

    P->p_EtaTriLE = new histogram(TYPE_PTriLE, ui->P_EtaTriLEView);
    P->p_EtaTriLE->init(-LErange, LErange, 0.2, 200, TYPE_ETA);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), P->p_EtaTriLE, SLOT(refreshPerfTriLE(PROCESSEDData*, int, int)));

    P->p_PhiDouLE = new histogram(TYPE_PDouLE, ui->P_PhiDouLEView);
    P->p_PhiDouLE->init(-LErange, LErange, 0.2, 200, TYPE_PHI);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), P->p_PhiDouLE, SLOT(refreshPerfDouLE(PROCESSEDData*, int, int)));

    P->p_EtaDouLE = new histogram(TYPE_PDouLE, ui->P_EtaDouLEView);
    P->p_EtaDouLE->init(-LErange, LErange, 0.2, 200, TYPE_ETA);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), P->p_EtaDouLE, SLOT(refreshPerfDouLE(PROCESSEDData*, int, int)));
}

void MainWindow::DoubletPerfInit()
{
    int LErange = 5;
    P = new layercontent();

    P->p_PhiDouLE = new histogram(TYPE_PDouLE, ui->P_PhiDouLEView);
    P->p_PhiDouLE->init(-LErange, LErange, 0.2, 200, TYPE_PHI);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), P->p_PhiDouLE, SLOT(refreshPerfDouLE(PROCESSEDData*, int, int)));

    P->p_EtaDouLE = new histogram(TYPE_PDouLE, ui->P_EtaDouLEView);
    P->p_EtaDouLE->init(-LErange, LErange, 0.2, 200, TYPE_ETA);
    connect(DR, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), P->p_EtaDouLE, SLOT(refreshPerfDouLE(PROCESSEDData*, int, int)));
}
void MainWindow::resetDisplay(uint layerNOs)
{
    for(uint i = 0; i <layerNOs; i++){
        L[i]->m_ChamberEfficiency->clearDisplay();
        L[i]->m_GapEfficiency->clearDisplay();
        L[i]->m_EtaTriggerEfficiency->clearDisplay();
        L[i]->m_EtaRefTriggerEfficiency->clearDisplay();
        L[i]->m_PhiTriggerEfficiency->clearDisplay();
        L[i]->m_PhiRefTriggerEfficiency->clearDisplay();
        L[i]->m_EtaHit->clearDisplay();
        L[i]->m_PhiHit->clearDisplay();
        L[i]->m_EtaChRefEff->clearDisplay();
        L[i]->m_PhiChRefEff->clearDisplay();


        L[i]->colorMap->init(i);
        L[i]->noiseMap->init(i);
        L[i]->TOTMap->init(i);

        L[i]->m_PhiLE->clearDisplay();
        L[i]->m_EtaLE->clearDisplay();
        L[i]->m_PhiFE->clearDisplay();
        L[i]->m_EtaFE->clearDisplay();
        L[i]->m_PhiTOT->clearDisplay();
        L[i]->m_EtaTOT->clearDisplay();

        L[i]->m_PhiChTOT->clearDisplay();
        L[i]->m_EtaChTOT->clearDisplay();
        L[i]->m_PhiChTOF->clearDisplay();
        L[i]->m_EtaChTOF->clearDisplay();

        L[i]->m_PhiCS->clearDisplay();
        L[i]->m_EtaCS->clearDisplay();

        UI_TB[i]->clear();
    }
    if(P!=nullptr&&layerNOs == 3){
        P->p_PhiEff->clearDisplay();
        P->p_EtaEff->clearDisplay();
        P->p_PhiTriLE->clearDisplay();
        P->p_EtaTriLE->clearDisplay();
        P->p_PhiDouLE->clearDisplay();
        P->p_EtaDouLE->clearDisplay();
        ui->P_TB_MSG->clear();
    }
    else if(P!=nullptr&&layerNOs == 2){
        P->p_PhiDouLE->clearDisplay();
        P->p_EtaDouLE->clearDisplay();
        ui->P_TB_MSG->clear();
    }
}

void MainWindow::resetDataReader()
{
    //Cleaning all the vectors
    DR->ClearVectors();
    DR->TriggerCount = 0;

    //DR->outputFileName = DR->GetFileName(".RunRegistry/RunRegistry.csv"); //Record Run的信息在csv文件，并且返回输出文件名
    //DR->outputFile = new TFile(DR->outputFileName.c_str(), "RECREATE");
}

void MainWindow::bindShortCuts()
{
    //Shortcuts for widgets
    UI_ChamberEfficiencyView[0] = ui->L0_ChamberEfficiencyView;
    UI_ChamberEfficiencyView[1] = ui->L1_ChamberEfficiencyView;
    UI_ChamberEfficiencyView[2] = ui->L2_ChamberEfficiencyView;

    UI_GapEfficiencyView[0] = ui->L0_GapEfficiencyView;
    UI_GapEfficiencyView[1] = ui->L1_GapEfficiencyView;
    UI_GapEfficiencyView[2] = ui->L2_GapEfficiencyView;

    UI_EtaEfficiencyView[0] = ui->L0_EtaEfficiencyView;
    UI_EtaEfficiencyView[1] = ui->L1_EtaEfficiencyView;
    UI_EtaEfficiencyView[2] = ui->L2_EtaEfficiencyView;
    UI_PhiEfficiencyView[0] = ui->L0_PhiEfficiencyView;
    UI_PhiEfficiencyView[1] = ui->L1_PhiEfficiencyView;
    UI_PhiEfficiencyView[2] = ui->L2_PhiEfficiencyView;

    UI_EtaRefEfficiencyView[0] = ui->L0_EtaRefEfficiencyView;
    UI_EtaRefEfficiencyView[1] = ui->L1_EtaRefEfficiencyView;
    UI_EtaRefEfficiencyView[2] = ui->L2_EtaRefEfficiencyView;
    UI_PhiRefEfficiencyView[0] = ui->L0_PhiRefEfficiencyView;
    UI_PhiRefEfficiencyView[1] = ui->L1_PhiRefEfficiencyView;
    UI_PhiRefEfficiencyView[2] = ui->L2_PhiRefEfficiencyView;

    UI_EtaHitView[0] = ui->L0_EtaHitDistribution;
    UI_EtaHitView[1] = ui->L1_EtaHitDistribution;
    UI_EtaHitView[2] = ui->L2_EtaHitDistribution;
    UI_PhiHitView[0] = ui->L0_PhiHitDistribution;
    UI_PhiHitView[1] = ui->L1_PhiHitDistribution;
    UI_PhiHitView[2] = ui->L2_PhiHitDistribution;

    UI_EtaChRefEffView[0] = ui->L0_EtaChRefEfficiencyView;
    UI_EtaChRefEffView[1] = ui->L1_EtaChRefEfficiencyView;
    UI_EtaChRefEffView[2] = ui->L2_EtaChRefEfficiencyView;
    UI_PhiChRefEffView[0] = ui->L0_PhiChRefEfficiencyView;
    UI_PhiChRefEffView[1] = ui->L1_PhiChRefEfficiencyView;
    UI_PhiChRefEffView[2] = ui->L2_PhiChRefEfficiencyView;

    UI_HitMap[0] = ui->L0_Hit_map;
    UI_HitMap[1] = ui->L1_Hit_map;
    UI_HitMap[2] = ui->L2_Hit_map;

    UI_NoiseMap[0] = ui->L0_Noise_map;
    UI_NoiseMap[1] = ui->L1_Noise_map;
    UI_NoiseMap[2] = ui->L2_Noise_map;

    UI_TOTMap[0] = ui->L0_TOT_map;
    UI_TOTMap[1] = ui->L1_TOT_map;
    UI_TOTMap[2] = ui->L2_TOT_map;

    UI_EtaLEView[0] = ui->L0_EtaLEView;
    UI_EtaLEView[1] = ui->L1_EtaLEView;
    UI_EtaLEView[2] = ui->L2_EtaLEView;
    UI_PhiLEView[0] = ui->L0_PhiLEView;
    UI_PhiLEView[1] = ui->L1_PhiLEView;
    UI_PhiLEView[2] = ui->L2_PhiLEView;

    UI_PhiFEView[0] = ui->L0_PhiFEView;
    UI_PhiFEView[1] = ui->L1_PhiFEView;
    UI_PhiFEView[2] = ui->L2_PhiFEView;
    UI_EtaFEView[0] = ui->L0_EtaFEView;
    UI_EtaFEView[1] = ui->L1_EtaFEView;
    UI_EtaFEView[2] = ui->L2_EtaFEView;

    UI_PhiTOTView[0] = ui->L0_PhiTOTView;
    UI_PhiTOTView[1] = ui->L1_PhiTOTView;
    UI_PhiTOTView[2] = ui->L2_PhiTOTView;
    UI_EtaTOTView[0] = ui->L0_EtaTOTView;
    UI_EtaTOTView[1] = ui->L1_EtaTOTView;
    UI_EtaTOTView[2] = ui->L2_EtaTOTView;

    UI_PhiChTOTView[0] = ui->L0_PhiChTOTView;
    UI_PhiChTOTView[1] = ui->L1_PhiChTOTView;
    UI_PhiChTOTView[2] = ui->L2_PhiChTOTView;
    UI_EtaChTOTView[0] = ui->L0_EtaChTOTView;
    UI_EtaChTOTView[1] = ui->L1_EtaChTOTView;
    UI_EtaChTOTView[2] = ui->L2_EtaChTOTView;

    UI_PhiChTOFView[0] = ui->L0_PhiChTOFView;
    UI_PhiChTOFView[1] = ui->L1_PhiChTOFView;
    UI_PhiChTOFView[2] = ui->L2_PhiChTOFView;
    UI_EtaChTOFView[0] = ui->L0_EtaChTOFView;
    UI_EtaChTOFView[1] = ui->L1_EtaChTOFView;
    UI_EtaChTOFView[2] = ui->L2_EtaChTOFView;

    UI_PhiCSView[0] = ui->L0_PhiCSView;
    UI_PhiCSView[1] = ui->L1_PhiCSView;
    UI_PhiCSView[2] = ui->L2_PhiCSView;
    UI_EtaCSView[0] = ui->L0_EtaCSView;
    UI_EtaCSView[1] = ui->L1_EtaCSView;
    UI_EtaCSView[2] = ui->L2_EtaCSView;

    UI_TB[0] = ui->L0_TB_MSG;
    UI_TB[1] = ui->L1_TB_MSG;
    UI_TB[2] = ui->L2_TB_MSG;
}
