#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QObject>
#include <QLineEdit>
#include <QPushButton>
#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QNetworkInterface>
#include <QMessageBox>
#include <QTimer>
#include "hitreconstructor.h"

const int Event_in_One_Pack = 1000;
typedef struct packedData
{
    int EventsContained;
    int EventNo[Event_in_One_Pack];
    bool is_track[Event_in_One_Pack];
    float K1[Event_in_One_Pack];
    float K2[Event_in_One_Pack];
    float C1[Event_in_One_Pack];
    float C2[Event_in_One_Pack];
} trackInfo;

class TCPServer : public QWidget
{
    Q_OBJECT
public:
    TCPServer(QLineEdit* LocalIP, QLineEdit* ListeningPort, QPushButton *Connect, QPushButton *Send);
signals:

private:
    QTcpServer *m_Server;
    QList<QTcpSocket*> m_ClientList;
    QTcpSocket* m_CurrentClient;
    QLineEdit* LE_LocalIP;
    QLineEdit* LE_ListeningPort;
    QPushButton *B_TCPConnect, *B_TCPSend;
    trackInfo Pack;


public slots:
    void NewConnectionSlot();
    void DisconnectedSlot();
    void B_ConnectSlot();
    void B_SendSlot();
    void SendTracks(PROCESSEDData* processedData, int EventStartNo, int EventEndNo);
};

#endif // TCPSERVER_H
