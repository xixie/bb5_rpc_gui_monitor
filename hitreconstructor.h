#ifndef HITRECONSTRUCTOR_H
#define HITRECONSTRUCTOR_H
#include <vector>
#include <QDebug>
#include <v1190a.h>
#include <QObject>
#include "mapsettings.h"
#include "MsgSvc.h"
#include "fstream"
#include "IniFile.h"

using namespace std;
struct SINGLETData
{
    //Phi
    vector<vector<int>>             *PhiLECh;
    vector<vector<float>>           *PhiLE;
    vector<vector<int>>             *PhiFECh;
    vector<vector<float>>           *PhiFE;
    //Eta
    vector<vector<int>>             *EtaLECh;
    vector<vector<float>>           *EtaLE;
    vector<vector<int>>             *EtaFECh;
    vector<vector<float>>           *EtaFE;
    //Paired flag
    vector<vector<bool>>            *PhiPaired; //If paired, FE would be offered, otherwise only le info would be plotted
    vector<vector<bool>>            *EtaPaired;
};

struct PROCESSEDData
{
    vector<int>            *EventNo;
    vector<int>            *NHits;
    vector<int>            *PhiSignalCh[3];
    vector<float>          *PhiSignalLE[3];
    vector<float>          *PhiSignalFE[3];
    vector<int>            *PhiClusterSize[3];
    vector<int>            *EtaSignalCh[3];
    vector<float>          *EtaSignalLE[3];
    vector<float>          *EtaSignalFE[3];
    vector<int>            *EtaClusterSize[3];
    SINGLETData            *LayerList[3]; //0 = TOP, 1 = MIDDLE, 2 = BOTTOM

    //For muon tracks
    vector<bool>            isTrack;
    vector<float>           K1, K2, C1, C2;
};


class hitReconstructor : public QObject
{
    Q_OBJECT

private:
    uint                EventNumber;
    uint                TriggeredNumber;
    size_t              layerNO;
    size_t              Event_ST;
    size_t              Event_ED;


    vector<int>         *temp_PhiLECh, *SC_LECh;
    vector<float>       *temp_PhiLE, *SC_LE;
    vector<int>         *temp_PhiFECh, *SC_FECh;
    vector<float>       *temp_PhiFE, *SC_FE;

    vector<int>         *temp_EtaLECh, *SC_EtaLECh;
    vector<float>       *temp_EtaLE, *SC_EtaLE;
    vector<int>         *temp_EtaFECh, *SC_EtaFECh;
    vector<float>       *temp_EtaFE, *SC_EtaFE;

    vector<bool>        *temp_PhiPaired;
    vector<bool>        *temp_EtaPaired;
    std::ofstream outFile;

public:
    explicit            hitReconstructor(QObject *parent = nullptr);
    void                mapinit();
    void                clear();
    void                process(RAWData* TDCData, size_t event_st, size_t event_ed);
    void                reconstruct(RAWData* TDCData);
    void                calculate();
    void                tracking();
    void                temp_clear();
    float               SumOfProduct(std::vector<float> Var1, std::vector<float> Var2);
    float               SumOfProduct(std::vector<float> Var1, float C = 1);
    PROCESSEDData       processedData;
    IniFile            *iniFile;

signals:
    void                SendContext(QString context);
    void                SendProcessedData(PROCESSEDData* processedData, int EventStartNo, int EventEndNo);

public slots:
};

#endif // HITRECONSTRUCTOR_H
