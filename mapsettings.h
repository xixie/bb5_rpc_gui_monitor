#ifndef _mapsettings
#define _mapsettings
#include <iostream>
#include <vector>
#include <map>
#include <QTime>

    extern std::vector<size_t>                              Phi_st_list;
    extern std::vector<size_t>                              Phi_ed_list;
    extern std::vector<size_t>                              Eta_st_list;
    extern std::vector<size_t>                              Eta_ed_list;
    extern size_t                                           ETASTARTSTRIPNO;
    extern size_t                                           PHISTARTSTRIPNO;
    extern size_t                                           LAYERNOS;
    extern size_t                                           PHISTRIPNOS;
    extern size_t                                           ETASTRIPNOS;
    extern std::vector<std::map<size_t, size_t>>            PHITDCMAP; //Contain all layers PhiMap
    extern std::vector<std::map<size_t, size_t>>            ETATDCMAP; //Contain all layers EtaMap
    extern std::vector<std::map<size_t, size_t>>            PHISTRIPMAP; //Contain all layers PhiMap
    extern std::vector<std::map<size_t, size_t>>            ETASTRIPMAP; //Contain all layers EtaMap
    extern std::vector<double>                              PHISTRIPWIDTH;
    extern std::vector<double>                              ETASTRIPWIDTH;
    extern std::vector<double>                              PHIGAPWIDTH;
    extern std::vector<double>                              ETAGAPWIDTH;
    extern bool                                             MAP_ADD_STATUS;
    extern bool                                             MAP_BEEN_SET;   //Flag to judge if it's set
    extern double LeadginEdgeSt;
    extern double LeadingEdgeEd;
    extern uint EventNOOnceRead; //How many events would be read during each loop
    extern int LEFlag;
    extern int FEFlag;
    extern int LAYERREFERENCE[3];
    extern QTime mainClock;
    extern int importLoopNO;
    //extern float timeCost[10][500];

    extern float X0;
    extern float Y0;
    extern float Z0;
    extern float SingletHeight;
    extern float StripWidth;
    extern float StripGap;

#endif
