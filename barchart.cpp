#include "barchart.h"
#include <QPushButton>
#include <iostream>

barChart::barChart(QWidget *parent) :
    QWidget(parent)
{

}
void barChart::initialX()
{
    set0 = new QBarSet("RPC_0_Eta");
    series = new QBarSeries(this);
    for(int i = 0; i < 16; i ++)
    {
        set0->append(0);
        QString s = QString::number(i);
        categories << s;
        ChHitCount[i] = 0;
    }
    series->append(set0);
    chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("RPC_0_Eta hitting number");
    chart->setAnimationOptions(QChart::SeriesAnimations);
    axis = new QBarCategoryAxis(this);
    setXaxis();
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    YAxisRange = 1;
    autorefresh_flag = false;
}
void barChart::setXaxis()
{
    chart->createDefaultAxes();
    axis->append(categories);
    chart->setAxisX(axis, series);
}
barChart::~barChart()
{
}
void barChart::refresh(RAWData* TDCData,  Uint EventStartNo, Uint EventEndNo)
{
    for (Uint i = EventStartNo; i < EventEndNo; i++)
    {
        for(uint j = 0; j < TDCData->ChannelList->at(i).size(); j++)
        {
            int ch= TDCData->ChannelList->at(i).at(j);
            ChHitCount[ch] += 1;
            if( ChHitCount[ch] > YAxisRange )
            {
                YAxisRange = ChHitCount[ch];
                chart->axisY()->setRange(0, int(YAxisRange * 1.1));
            }
        }
    }
    for(int i = 0; i < 16 ; i ++)
        set0->replace(i,ChHitCount[i]);
}
