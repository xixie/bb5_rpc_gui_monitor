#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QObject>
#include <QMainWindow>
#include <QTcpSocket>
#include <QHostAddress>
#include <QMessageBox>
#include <sstream>
#include <QList>
#include <QByteArray>
#include "hitreconstructor.h"
#include "IniFile.h"
#include "v1190a.h"
#include "QTimer"

class tcpClient : public QObject
{
    Q_OBJECT

public:
    explicit tcpClient(QObject *parent = nullptr);
    virtual ~tcpClient();
    void Init();
    bool ConnectServer(QString IPAdd, quint16 port);
public slots:
    void ReceiveData();
    void ReadError(QAbstractSocket::SocketError);
    void RelaySendProcessedData(PROCESSEDData* processedData, int EventStartNo, int EventEndNo);
    void RelayContext(QString context);
    void StopAndSaved();

signals:
    // need to be connected with DR.RelaySendProcessedData, since all plots/hists are connected with DR.SendProcessedData
    void SendProcessedData(PROCESSEDData* processedData, int EventStartNo, int EventEndNo);
    void SendContext(QString context);
    void ReceiveFinished();

private:
    QTcpSocket* m_tcpSocket;
    bool DecodeData(QByteArray buffer, RAWData &TDCData);
    hitReconstructor* m_HitRecon;
    IniFile* m_IniFile;
    RAWData TDCData; //decoded from HP-TDC data via DecodeData()
    uint m_TriggerCount=0;
    int m_MaxTriggers = 1000;
    void FileInit();
    std::string GetFileName(string runregistry);
    std::string GetRunNumber(string runregistry);
    void decode(const uint32_t *event, unsigned long int event_size);
    void SetChDat(unsigned inword, short tdcid, short elinkid);
    void initMap();
    void clearTempVar();

    TFile               *outputFile;
    TTree               *RAWDataTree;

    int                 EventCount = 0;
    int                 nHits;
    vector<int>         TDCCh;
    vector<float>       TDCTS;
    vector<int>         TDCSS;

    string              outputFileName;
    ofstream            m_DebugTxt;

    // For offline test
    QTimer              m_timer;
    int                 lineNo = 0;
};

#endif // TCPCLIENT_H
