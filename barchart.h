#ifndef BARCHART_H
#define BARCHART_H

#include <QWidget>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts/QBarSeries>
#include <QtCharts/QBarSet>
#include <QtCharts/QLegend>
#include <QtCharts/QBarCategoryAxis>
#include <random>
#include <QDebug>
#include "v1190a.h"

QT_CHARTS_USE_NAMESPACE


class barChart : public QWidget
{
    Q_OBJECT


public:
    explicit barChart(QWidget *parent = 0);
    ~barChart();
    void setXaxis();
    void initialX();
    QChart *chart;

public slots:
    void refresh(RAWData* TDCData,  Uint EventStartNo, Uint EventEndNo);

signals:
    void SendContext(QString context);
    void SendRunDAQ();

private:
    QBarSeries *series;
    QStringList categories;
    QBarSet *set0;
    QBarCategoryAxis *axis;
    int YAxisRange;
    bool autorefresh_flag;
    int ChHitCount[16];
};

#endif // BARCHART_H
