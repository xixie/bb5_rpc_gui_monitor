// ****************************************************************************************************
// *   DataReader
// *   Alexis Fagot
// *   23/01/2015
// ****************************************************************************************************
#ifndef __LINUX
#define __LINUX
#endif

#ifndef DATAREADER_H
#define DATAREADER_H

#include "hitreconstructor.h"
#include <string>
#include <vector>
#include <QWidget>
#include "v1190a.h"
#include "v1718.h"
#include "MsgSvc.h"
#include <TFile.h>
#include <TCanvas.h>
#include <TVirtualX.h>
#include <TSystem.h>
#include <TFormula.h>
#include <TF1.h>
#include <TH1.h>
#include <TFrame.h>
#include <TTimer.h>
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include <hitreconstructor.h>
#include <mapsettings.h>

//using namespace std;

class DataReader : public QObject
{

    Q_OBJECT

    private:
        bool                StopFlag;
        Data32              MaxTriggers;
        int                 ImportMaxEvents;
        bool                ChHittedMap[96];
        bool                RunFlag;
        bool                ImportFlag;
        bool                ImportFinishedFlag;
        int                 EventSets; //To record how many sets have beet emitted to ChartsViews
        int                 Set;
        QString             fileName;
        TFile               *importFile;
        TTree               *RAWDataTree;
        TTreeReader         *importReader;
        TTreeReaderValue<Int_t>      *im_EventNumber;
        TTreeReaderValue<Int_t>      *im_NHits;
        TTreeReaderValue<std::vector<Int_t>>      *im_Channel;
        TTreeReaderValue<std::vector<Float_t>>      *im_TimeStamp;
        TTreeReaderValue<std::vector<Int_t>>      *im_StyleStamp;
        float temp_time;
        ofstream outFile;

    signals:
        void                SendContext(QString context);
        void                SendTDCData(RAWData* TDCData, int EventStartNo, int EventEndNo);
        void                SendProcessedData(PROCESSEDData* processedData, int EventStartNo, int EventEndNo);
        void                DAQFinished();

    public slots:
        void                RelayContext(QString context);
        void                RelaySendProcessedData(PROCESSEDData* processedData, int EventStartNo, int EventEndNo);
    public:
        RAWData             TDCData;
        int                 nTDCs;
        hitReconstructor    *hitRecon;
        IniFile            *iniFile;
        v1190a             *TDCs;
        v1718              *VME;
        Uint                TriggerCount;
        TFile               *outputFile;
        explicit            DataReader(QObject *parent = nullptr);
        virtual            ~DataReader();
        void                SetIniFile();
        void                SetMaxTriggers();
        Data32              GetMaxTriggers();
        void                Init(string inifilename);
        void                SetVME();
        void                SetTDC();
        void                Update();
        string              GetRunNumber(string runregistry);
        string              GetFileName(string runregistry);
        void                RunInit(bool importInifile);
        void                ImportInit();
        Uint                FraudDataGenerator(RAWData *DataList);
        int                *GetTDCCh();
        int                *GetTDCTS();
        void                ClearVectors();

        int                 EventCount;
        int                 nHits;
        vector<int>         TDCCh;
        vector<float>       TDCTS;
        vector<int>         TDCSS;
        string              outputFileName;

    public slots:
        void                AquireData();
        void                ImportData();
        void                StopAndSaved();
        void                RunDAQ(bool importInifile);
        void                ImportFile(QString filename);
};

#endif // DATAREADER_H
