#include "hitmap.h"

hitMap::hitMap(QCustomPlot *customPlot0, QCPAxis* xAxis, QCPAxis* yAxis) :
    QCPColorMap(xAxis, yAxis)
{
    //setKeyAxis(xAxis);
    //setValueAxis(yAxis);
    customPlot = customPlot0;
    m_ColorBar = new QCPColorGradient();
    m_ColorBar->setColorStopAt(1, QColor(50, 0, 0));
    m_ColorBar->setColorStopAt(0.8, QColor(180, 10, 0));
    m_ColorBar->setColorStopAt(0.6, QColor(245, 50, 0));
    m_ColorBar->setColorStopAt(0.4, QColor(255, 150, 10));
    m_ColorBar->setColorStopAt(0.2, QColor(255, 255, 50));
    m_ColorBar->setColorStopAt(0, QColor(255, 255, 255));
    m_ColorBar->setColorInterpolation(QCPColorGradient::ciRGB);
}

void hitMap::init(uint l)
{
    setName("Htting map");
    setTightBoundary(false);
    setInterpolate(false);
    layerNO = l;
    data()->setRange(QCPRange(ETASTARTSTRIPNO - 1, ETASTARTSTRIPNO + ETASTRIPNOS), QCPRange(PHISTARTSTRIPNO - 1, PHISTARTSTRIPNO + PHISTRIPNOS));
    data()->setSize(int(ETASTRIPNOS + 1)*5+1, int(PHISTRIPNOS + 1)*5+1);
    for (int y= 0; y<int(PHISTRIPNOS + 1)*5+1; ++y){
      for (int x= 0; x<int(ETASTRIPNOS + 1)*5+1; ++x){
        data()->setCell(x, y, 0.0);
      }
    }
    setGradient(*m_ColorBar);
    rescaleDataRange(true);

    //aux data container
    auxNo = new QCPColorMapData(*this->data());
    auxSum = new QCPColorMapData(*this->data());

    rescaleDataRange(true);
    customPlot->replot();
}

void hitMap::refreshHitMap(PROCESSEDData* Data,  int EventStartNo, int EventEndNo)
{
    for (int i = EventStartNo; i < EventEndNo; i++)
    {
        //x, y are the relative position in the map.
        std::map<size_t, size_t>::iterator ETA_it = ETASTRIPMAP.at(layerNO).find(Data->EtaSignalCh[layerNO]->at(i));
        std::map<size_t, size_t>::iterator PHI_it = PHISTRIPMAP.at(layerNO).find(Data->PhiSignalCh[layerNO]->at(i));
        if(ETA_it != ETASTRIPMAP.at(layerNO).end() && PHI_it != PHISTRIPMAP.at(layerNO).end()){
            int x = ((*ETA_it).first-ETASTARTSTRIPNO+1) * 5-1;
            int y = ((*PHI_it).first-PHISTARTSTRIPNO+1) * 5-1;
            for(int i = 0; i < 3; i++){
                for(int j = 0; j < 3; j++){
                    data()->setCell(x+i, y+j, data()->cell(x+i,y+j) + 1.0);
                }
            }
        }
    }
    rescaleDataRange(true);
    customPlot->replot();
}

void hitMap::refreshNoiseMap(PROCESSEDData* Data,  int EventStartNo, int EventEndNo)
{
    for (int i = EventStartNo; i < EventEndNo; i++)
    {
        for(ulong j = 0; j < Data->LayerList[layerNO]->EtaLE->at(i).size(); j++)
        {
            int etaCh = Data->LayerList[layerNO]->EtaLECh->at(i).at(j);
            float etaLE = Data->LayerList[layerNO]->EtaLE->at(i).at(j);
            if(etaLE < LeadingEdgeEd && etaLE > LeadginEdgeSt){
                break;
            }
            for(ulong k = 0; k < Data->LayerList[layerNO]->PhiLE->at(i).size(); k++)
            {
                int phiCh = Data->LayerList[layerNO]->PhiLECh->at(i).at(k);
                float phiLE = Data->LayerList[layerNO]->PhiLE->at(i).at(k);
                if(abs(etaLE-phiLE) < 5){
                    int x = (etaCh-ETASTARTSTRIPNO+1) * 5-1;
                    int y = (phiCh-PHISTARTSTRIPNO+1) * 5-1;
                    for(int i = 0; i < 3; i++){
                        for(int j = 0; j < 3; j++){
                            data()->setCell(x+i, y+j, data()->cell(x+i,y+j) + 1.0);
                        }
                    }
                }
            }
        }
    }
    rescaleDataRange(true);
    customPlot->replot();
}

void hitMap::refreshTOTMap(PROCESSEDData* Data,  int EventStartNo, int EventEndNo)
{
    for (uint i = EventStartNo; i < EventEndNo; i++)
    {
        //x, y are the relative position in the map.
        double tempPhiTOT = (Data->PhiSignalFE[layerNO]->at(i)) - (Data->PhiSignalLE[layerNO]->at(i));
        double tempEtaTOT = (Data->EtaSignalFE[layerNO]->at(i)) - (Data->EtaSignalLE[layerNO]->at(i));

        if(tempEtaTOT >0 && tempEtaTOT <=100 && tempPhiTOT >0 && tempPhiTOT <=100){
            double TOT = (tempEtaTOT + tempPhiTOT)/2;
            int x = (Data->EtaSignalCh[layerNO]->at(i)-ETASTARTSTRIPNO+1) * 5-1;
            int y = (Data->PhiSignalCh[layerNO]->at(i)-PHISTARTSTRIPNO+1) * 5-1;
            if( x > 0 && y >0){
                for(int i = 0; i < 3; i++){
                    for(int j = 0; j < 3; j++){
                        auxNo->setCell(x+i, y+j, auxNo->cell(x+i,y+j) + 1.0);
                        auxSum->setCell(x+i, y+j, auxSum->cell(x+i,y+j) + TOT);
                        double avg = auxSum->cell(x+i,y+j) / auxNo->cell(x+i,y+j);
                        data()->setCell(x+i, y+j, avg);
                    }
                }
            }
        }
    }
    rescaleDataRange(true);
    customPlot->replot();
}

hitMap::~hitMap()
{
    delete  auxNo;
    delete  auxSum;
}
