#include "tcpserver.h"

TCPServer::TCPServer(QLineEdit* LocalIP, QLineEdit* ListeningPort, QPushButton* Connect, QPushButton* Send)
{
    m_Server = new QTcpServer(this);
    LE_LocalIP = LocalIP;
    LE_ListeningPort = ListeningPort;
    B_TCPConnect = Connect;
    B_TCPSend = Send;
    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
        {
            LE_LocalIP->setText(address.toString());
            break;
        }
    }
    connect(m_Server, SIGNAL(newConnection()), this, SLOT(NewConnectionSlot()));
    connect(B_TCPConnect, SIGNAL(clicked()), this, SLOT(B_ConnectSlot()));
    connect(B_TCPSend, SIGNAL(clicked()), this, SLOT(B_SendSlot()));
}

void TCPServer::NewConnectionSlot()
{
    m_CurrentClient = m_Server->nextPendingConnection();
    m_ClientList.append(m_CurrentClient);
    connect(m_CurrentClient, SIGNAL(disconnected()), this, SLOT(DisconnectedSlot()));
}

void TCPServer::DisconnectedSlot()
{
    for(int i=0; i<m_ClientList.length(); i++)
    {
        if(m_ClientList[i]->state() == QAbstractSocket::UnconnectedState)
        {
            m_ClientList[i]->destroyed();
            m_ClientList.removeAt(i);
        }
    }
}

void TCPServer::B_ConnectSlot()
{
    if(B_TCPConnect->text()=="Listen")
    {
        bool ok = m_Server->listen(QHostAddress::Any, LE_ListeningPort->text().toInt());
        qDebug() << ok;
        if(ok)
        {
            B_TCPConnect->setText("Disconnect");
            B_TCPSend->setEnabled(true);
        }
    }
    else
    {
        for(int i=0; i<m_ClientList.length(); i++)
        {
            m_ClientList[i]->disconnectFromHost();
            m_ClientList.removeAt(i);
        }
        m_Server->close();
        B_TCPConnect->setText("Listen");
        B_TCPSend->setEnabled(false);
    }
}

void TCPServer::B_SendSlot()
{
    QString data = "Hello TCP";
    for(int i=0; i<m_ClientList.length(); i++)
    {
        m_ClientList[i]->write(data.toLatin1());
    }
}

void TCPServer::SendTracks(PROCESSEDData* processedData, int EventStartNo, int EventEndNo)
{
    /*if(EventEndNo - EventStartNo > Event_in_One_Pack)
    {
        Pack.EventsContained = Event_in_One_Pack;
        for(uint i = 0; i < Event_in_One_Pack; i++)
        {
            Pack.EventNo[i] = processedData->EventNo->at(i + EventStartNo);
            Pack.is_track[i] = processedData->isTrack.at(i + EventStartNo);
            Pack.C1[i] = processedData->C1.at(i + EventStartNo);
            Pack.K1[i] = processedData->K1.at(i + EventStartNo);
            Pack.C2[i] = processedData->C2.at(i + EventStartNo);
            Pack.K2[i] = processedData->K2.at(i + EventStartNo);
        }
    }
    else
    {
        Pack.EventsContained = EventEndNo - EventStartNo;
        for(uint i = 0; i < EventEndNo - EventStartNo; i++)
        {
            Pack.EventNo[i] = processedData->EventNo->at(i + EventStartNo);
            Pack.is_track[i] = processedData->isTrack.at(i + EventStartNo);
            Pack.C1[i] = processedData->C1.at(i + EventStartNo);
            Pack.K1[i] = processedData->K1.at(i + EventStartNo);
            Pack.C2[i] = processedData->C2.at(i + EventStartNo);
            Pack.K2[i] = processedData->K2.at(i + EventStartNo);
        }
        for(uint i = EventEndNo - EventStartNo; i < Event_in_One_Pack; i++)
        {
            Pack.EventNo[i] = -1;
            Pack.is_track[i] = false;
            Pack.C1[i] = 0.0;
            Pack.K1[i] = 0.0;
            Pack.C2[i] = 0.0;
            Pack.K2[i] = 0.0;
        }
    }

    //send data to client
    for(int i=0; i<m_ClientList.length(); i++)
    {
        m_ClientList[i]->write((char*)&Pack, sizeof(trackInfo));
    }
    qDebug() << "Send Packs. Size = " << sizeof(trackInfo);*/

    QString data;
    data.clear();
    for(uint i = EventStartNo; i < EventEndNo; i++)
    {
        if(processedData->isTrack.at(i)){
            data.append(QString::number(processedData->EventNo->at(i)));
            data.append(';');
            data.append(QString("%1").arg(processedData->K1.at(i)));
            data.append(';');
            data.append(QString("%1").arg(processedData->C1.at(i)));
            data.append(';');
            data.append(QString("%1").arg(processedData->K2.at(i)));
            data.append(';');
            data.append(QString("%1").arg(processedData->C2.at(i)));
            data.append(';');
            //qDebug() << "Event " << processedData->EventNo->at(i) << " " << sizeof(data);
            //qDebug()<<data;
        }
    }
    for(int i=0; i<m_ClientList.length(); i++)
    {
        m_ClientList[i]->write(data.toLatin1());
    }
}
