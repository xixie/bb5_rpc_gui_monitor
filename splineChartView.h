#ifndef SPLINECHARTVIEW_H
#define SPLINECHARTVIEW_H

#include <QtCharts/QChartView>
#include <QtWidgets/QRubberBand>
#include <QScrollBar>
#include <QAbstractScrollArea>

QT_CHARTS_USE_NAMESPACE

//![1]
class splineChartView : public QChartView
//![1]
{
    Q_OBJECT
public:
    splineChartView(QWidget *parent = 0);
    splineChartView(QChart *chart, QWidget *parent = 0);
    virtual ~splineChartView();

signals:
    void sendMoveFlag(bool moveFlag);

//![2]
protected:
    bool viewportEvent(QEvent *event);
    //void mousePressEvent(QMouseEvent *event);
    //void mouseMoveEvent(QMouseEvent *event);
    //void mouseReleaseEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
   // void mouseScrollEvent(QWheelEvent *event);
//![2]

private:
    bool m_isTouching;
};

#endif
