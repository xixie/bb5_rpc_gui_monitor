#include "tcpclient.h"

tcpClient::tcpClient(QObject *parent):
    QObject(parent)
{

}

tcpClient::~tcpClient()
{

}

void tcpClient::Init()
{
    initMap();
    MSG_INFO("Test");
    m_tcpSocket = new QTcpSocket(this);
    m_tcpSocket->abort();
    //Initialize for IniFile configuration
    m_IniFile = new IniFile("gifdaqpp.ini");
    connect(m_IniFile, SIGNAL(SendContext(QString)), this, SLOT(RelayContext(QString)));
    m_MaxTriggers = m_IniFile->intType("General","MaxTriggers",MAXTRIGGERS_V1190A);

    m_HitRecon = new hitReconstructor();
    m_HitRecon->iniFile = m_IniFile;
    // to relay the m_HitRecon.SendProcessedData to be sent to external (to be connected to DR->Relay which is connected to all plots and hists)
    connect(m_HitRecon, SIGNAL(SendProcessedData(PROCESSEDData*, int, int)), this, SLOT(RelaySendProcessedData(PROCESSEDData*, int, int)));

    connect(m_tcpSocket, SIGNAL(readyRead()), this, SLOT(ReceiveData()));
    connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(ReadError(QAbstractSocket::SocketError)));

    FileInit();
    //Initialization of the RAWData vectors
    TDCData.EventList = new vector<int>;
    TDCData.NHitsList = new vector<int>;
    TDCData.ChannelList = new vector< vector<int> >;
    TDCData.TimeStampList = new vector< vector<float> >;
    TDCData.StyleStampList = new vector< vector<int> >;
    //EventNo =   0;
    //Cleaning all the vectors
    TDCData.EventList->clear();
    TDCData.NHitsList->clear();
    TDCData.ChannelList->clear();
    TDCData.TimeStampList->clear();
    TDCData.StyleStampList->clear();

    m_timer.start(1000);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(ReceiveData()));
}

bool tcpClient::ConnectServer(QString IPAdd, quint16 port)
{
    m_tcpSocket->connectToHost(IPAdd, port);
    if (m_tcpSocket->waitForConnected(1000))
    {
        std::cout <<"[TCPClient] Connected to " << IPAdd.toStdString() << " via port "<< port << std::endl;
        return true;
    }
    else{
        std::cout <<"[TCPClient Error] Cannot connect to " << IPAdd.toStdString() << " via port "<< port << std::endl;
        return false;
    }
}

bool tcpClient::DecodeData(QByteArray buffer, RAWData &TDCData)
{
    if(!buffer.isEmpty())
    {
        std::cout << "[TCPClient][Dev]: recevied data " << std::endl;
        m_DebugTxt << "=================================================" << std::endl;
        m_DebugTxt << "[TCPClient][Dev]: recevied data " << std::endl;
        m_DebugTxt << buffer.toHex().toStdString() << std::endl;

//        QString qstrBuffer = QString(buffer);
//        std::stringstream ss ;
//        ss << qstrBuffer.toStdString();
//        auto a = buffer.toHex();


        //decode part

        int j = 0;
        QList<QByteArray> EventList;
        // Split the buffer into EventList
        while(j!=buffer.size()){
            int eventLength = buffer[j]&0xFF;
//            qDebug() << "j=" <<j << "; eventLength = " << eventLength;
            QByteArray tempEvent;
            tempEvent.clear();
            for(int i = 0; i < eventLength; i++){
                tempEvent.append(buffer.at(j+i));
            }
            EventList.push_back(tempEvent);
            j+=eventLength;
            if(j>buffer.size()){
                m_DebugTxt<<"error in splitting events" << std::endl;
                break;
            }
        }
        for(int j = 0; j < EventList.size();j++){
            uint32_t event[EventList[j].size()/4]; // one byte = 8 bit = sizeof(0x00), buffer.size in [byte]
            for(int i = 0; i < EventList[j].size()/4; i++){
                unsigned int temp = 0x00000000;
                //Though the transforming here is inverted, the data would be inverted again in 'decode'
                temp |= ((EventList[j][0+4*i] << 0) & 0x000000FF );
                temp |= ((EventList[j][1+4*i] << 8) & 0x0000FF00);
                temp |= ((EventList[j][2+4*i] << 16)& 0x00FF0000);
                temp |= ((EventList[j][3+4*i] << 24)& 0xFF000000);
                event[i] = temp;
            }
            decode(event, EventList[j].size()/4);
        }

        return true;
    }
    else{
        return false;
    }
}

void tcpClient::FileInit()
{
    outputFileName = GetFileName(".RunRegistry/RunRegistry.csv"); //Record Run的信息在csv文件，并且返回输出文件名
    outputFile = new TFile(outputFileName.c_str(), "RECREATE");
    RAWDataTree = new TTree("RAWData","RAWData");
    RAWDataTree->Branch("EventNumber",    &EventCount,  "EventNumber/I");
    RAWDataTree->Branch("number_of_hits", &nHits,       "number_of_hits/I");
    RAWDataTree->Branch("TDC_channel",    &TDCCh);
    RAWDataTree->Branch("TDC_TimeStamp",  &TDCTS);
    RAWDataTree->Branch("TDC_StyleStamp",  &TDCSS);
    m_DebugTxt.open("TCPClientDebug.txt");
}

std::string tcpClient::GetFileName(string runregistry)
{
    //Write the run info in the run registry
    string RunNumber = GetRunNumber(runregistry.c_str());

    ofstream runregWrite(runregistry.c_str(),ios::app);
    runregWrite << m_IniFile->stringType("General","RunType","") << '\t'
                << m_IniFile->stringType("General","MaxTriggers","") << '\t'
                << m_IniFile->stringType("General","TriggerType","") << '\t'
                << m_IniFile->stringType("General","Threshold","") << '\t'
                << m_IniFile->stringType("General","Voltage","") << '\n';

    //Set the output filename
    string fNameParts[9];
    fNameParts[0] = m_IniFile->stringType("General","RunType","");
    fNameParts[1] = m_IniFile->stringType("General","ChamberType","");
    fNameParts[2] = m_IniFile->stringType("General","Mode","");
    fNameParts[3] = m_IniFile->stringType("General","Partition","");
    fNameParts[4] = m_IniFile->stringType("General","MaxTriggers","");
    fNameParts[5] = m_IniFile->stringType("General","TriggerType","");
    fNameParts[6] = m_IniFile->stringType("General","ElectronicsType","");
    fNameParts[7] = m_IniFile->stringType("General","Threshold","");
    fNameParts[8] = m_IniFile->stringType("General","Voltage","");

    for(int i=0; i<9;i++)
        if(fNameParts[i] != "")
            fNameParts[i] += "_";

    stringstream fNameStream;

    fNameStream << "datarun/";                                      //destination
    for(int i=0; i<9;i++)
        fNameStream << fNameParts[i];                               //run info
    fNameStream << RunNumber                                        //run number
                << ".root";                                         //extension

    string outputfName;
    fNameStream >> outputfName;

    return outputfName;
}

string tcpClient::GetRunNumber(string runregistry)
{
    //Get the date first and then write it in the run registry along with the run number
    stringstream stream;
    //Get time information
    time_t t = time(nullptr);
    struct tm *Time = localtime(&t);
    int Y = Time->tm_year + 1900;
    int M = Time->tm_mon + 1;
    int D = Time->tm_mday;
    int h = Time->tm_hour;
    int m = Time->tm_min;
    int s = Time->tm_sec;

    //Set the Date
    string Date;

    stream << setfill('0') << setw(4) << Y << "/"
           << setfill('0') << setw(2) << M << "/"
           << setfill('0') << setw(2) << D << "-"
           << setfill('0') << setw(2) << h << ":"
           << setfill('0') << setw(2) << m << ":"
           << setfill('0') << setw(2) << s;

    stream >> Date;
    stream.clear();

    //Set the run number
    string RunNumber;

    stream << "run"
           << setfill('0') << setw(4) << Y
           << setfill('0') << setw(2) << M
           << setfill('0') << setw(2) << D
           << setfill('0') << setw(2) << h
           << setfill('0') << setw(2) << m
           << setfill('0') << setw(2) << s;

    stream >> RunNumber;
    stream.clear();

    //qDebug()<< RunNumber.c_str() << "into " << runregistry.c_str() <<endl;
    MSG_INFO("[DAQ]: Adding %s into %s\n",RunNumber.c_str(),runregistry.c_str());

    //Write this new run number in the run registry
    ofstream runregWrite(runregistry.c_str(),ios::app);
    runregWrite << RunNumber << '\t' << Date << '\t';

    return RunNumber;
}

void tcpClient::decode(const uint32_t *event, unsigned long event_size)
{
    m_DebugTxt << "total: " << std::dec << (int)event_size  << " [byte]" << std::endl;
    int ev_start = 0;  // one event = 4 byte
    for (int i=0; i<event_size; i++){
//        std::cout << std::hex << event[i] << "\t";
        if (ev_start == 0 && event[i] == 0xee1234ee) ev_start = i+1;
    }
    m_DebugTxt << std::endl;

    const uint8_t *data = reinterpret_cast <const uint8_t *>(event); // data in [byte]
    for(uint i = 0; i < 24; i++) m_DebugTxt << std::hex << (data[i]&0xff) << "\t";
    m_DebugTxt << std::endl;
    int counterstart = 45-36 , locallinkpos = 40-36 , datastart = 49-36 ; // [byte]

    m_DebugTxt<<"==================="<<std::endl;
    int data_start = ev_start*4+datastart;
    int byte_start = ev_start*4+counterstart;
    int ev_end = 4*(event_size);
    uint32_t data_word = 0, old_data, real_data;

    uint8_t counter = 4, end_chan = 0, counted = 0;
    short elinkid = data[ev_start*4+locallinkpos]& 0xff;
    m_DebugTxt << "elinkid = " << elinkid << std::endl;
    m_DebugTxt<<"data0, data1 =  "<<std::hex<<(unsigned)data[0]<<"; "<<(unsigned)data[1]<<std::endl;
    short tdcid = short(data[data_start] & 0xf);
    m_DebugTxt << "tdcid = " << tdcid << std::endl;
    uint32_t id_counter = uint32_t(data[byte_start] & 0xff);
    uint16_t loc_word_count = 0;
    for (int byte=0; byte<ev_end; byte++){
        unsigned byte_value = (unsigned)data[byte];
//        m_DebugTxt << std::hex << byte_value << "   ";//c1 11000001 c9 11001001
        if (byte<data_start) continue;
//        m_DebugTxt << "test" << std::endl;
        // save 4 byte into data_word, from the point of data_start
        counter--; data_word |= (byte_value <<(8*counter));
        if (!counter) {

            //channel = (byte-16) /m_data_size;
            counter = 4;
            real_data = data_word;
            old_data = data_word;
            data_word=0;
            if (real_data == 0xbc50bc50 || real_data == 0x50bc50bc || real_data == 0x50bcc5bc) continue;
            //word testing

            if (!end_chan) {
                // 0x6 means bad data
                if (((real_data>>24)&0xf0) == 0x60) {
                    if (!loc_word_count){
                        tdcid = (real_data>>24) & 0xf;
                        loc_word_count++;
                    } else if (((real_data>>24)&0xf) != tdcid){
                        end_chan = 1;
                    }
                    else {
                        loc_word_count++;
                    }
                }

                // 0x4 means good data
                if (((real_data>>24)&0xf0) == 0x40) {
                    if (!loc_word_count){
                        tdcid = (real_data>>24) & 0xf;
                        loc_word_count++;// MyObject->g_word_count++;
                        SetChDat(real_data, tdcid, elinkid);
                    } else if (((real_data>>24)&0xf) != tdcid) {
                        end_chan = 1;
                    }
                    else {
                        loc_word_count++;//, MyObject->g_word_count++;

                        SetChDat(real_data, tdcid, elinkid);
                    }
                }else end_chan = 1;
            }

            if (end_chan ==1) {
                if ((loc_word_count != (real_data>>24)) && !counted) {
                    //m_DebugTxt<<std::hex<<"Errors "<<(MyObject->g_WCerr_c++)<<" ID="<<id_counter<<" wc="<<loc_word_count<<" D="<<real_data<<" tot_count="<<(MyObject->g_word_count)<<"\n";
//                    MyObject->g_WCerr_c++,
                    end_chan = 0;
                }else{
                    counted = 1;
                }
                if (((real_data>>8) & 0xffff) == 0xaaaa){
                    end_chan = 2;
                }

            } else if (end_chan == 2) {
                elinkid = real_data&0xffff;
                end_chan = 0;
                byte+=8;
                loc_word_count = 0;
            }

        }
    }
}

void tcpClient::SetChDat(unsigned inword, short tdcid, short elinkid)
{
    unsigned mapkey = elinkid+(tdcid&0x1);
//	unsigned TDCnum = HPTDCMap[mapkey];
    unsigned location_index = (inword&0xf80000)>>19;//print all

//	if (HPTDCChannel1[TDCnum].size()<=location_index) return;
//	Tdcid.push_back(TDCnum);//temporary ??
//	Ltime.push_back(inword&0x7f); //0.195 ns
//	Width.push_back((inword&0x7f000)>>12); //0.4 ns
//	Channel.push_back(location_index);
//	BCID.push_back((inword&0xf80)>>7);
    //m_DebugTxt<<mapkey<<"  "<<location<<std::endl;

//    qDebug() << "Ltime:\t"  << (inword&0x7f) * 0.195;
//    qDebug() << "\tWidth:\t" << ((inword&0x7f000)>>12)*0.4 ;
//    qDebug() << "\tChannel:\t" << location_index ;
//    qDebug() << " \tBCID:\t" << ((inword&0xf80)>>7) ;

    nHits++;
    TDCCh.push_back(location_index + 32*tdcid);
    TDCTS.push_back((inword&0x7f) * 0.195);
    TDCSS.push_back(LEFlag);
    TDCCh.push_back(location_index + 32*tdcid);
    TDCTS.push_back((inword&0x7f) * 0.195 + ((inword&0x7f000)>>12)*0.4);
    TDCSS.push_back(FEFlag);
}

void tcpClient::initMap()
{
    std::cout << "[TCPClient]: reading HDTDCMapping.xml" << std::endl;

    PHITDCMAP.clear();
    ETATDCMAP.clear();
    PHISTRIPMAP.clear();
    ETASTRIPMAP.clear();

    LAYERNOS = 3;
    ETASTRIPNOS = 64;
    ETASTARTSTRIPNO = 1;
    PHISTRIPNOS = 128;
    PHISTARTSTRIPNO = 1;

    PHITDCMAP.resize(LAYERNOS);
    ETATDCMAP.resize(LAYERNOS);
    PHISTRIPMAP.resize(LAYERNOS);
    ETASTRIPMAP.resize(LAYERNOS);

    LAYERREFERENCE[0] = 1;
    LAYERREFERENCE[1] = 0;
    LAYERREFERENCE[2] = 1;

    QFile file("./Mapping/HPTDCMapping.xml");
    if(!file.open(QFile::ReadOnly)){
        std::cout << "[TCPClient]: Cannot open /Mapping/HPTDCMapping.xml" << std::endl;
        return;
    }
    QTextStream mapping(&file);
    QString lineStr;
    uint tdcNo = 0;
    while(!mapping.atEnd()){
        lineStr = mapping.readLine();
        auto temp = lineStr.split('"');
        auto tdcMap = temp.at(temp.length()-2).split(" ");
        uint tdcIndex = 0;
        for(auto map : tdcMap){
            uint eta_phi = map.mid(2, 1).toUInt(); // eta = 0, phi = 1
            uint layer = map.mid(3, 1).toUInt();
            if(layer > 2){
                std::cout << "[TCPClient] Wrong mapping RPC layers greater than 3!" << std::endl;
                return;
            }
            uint strip = map.right(2).toUInt(nullptr, 16);
            uint tdcCh = tdcIndex + tdcNo * 32; // tdcCh are encoded with tdcNo
            qDebug() << map <<layer << eta_phi << strip << tdcCh;
            if(eta_phi == 0){
                ETATDCMAP.at(layer).insert(pair<uint, uint>(tdcCh, strip));
                ETASTRIPMAP.at(layer).insert(pair<uint, uint>(strip, tdcCh));
            }else{
                PHITDCMAP.at(layer).insert(pair<uint, uint>(tdcCh, strip));
                PHISTRIPMAP.at(layer).insert(pair<uint, uint>(strip, tdcCh));
            }
            tdcIndex++;
        }
        tdcNo +=1;
    }
    file.close();

}

void tcpClient::clearTempVar()
{
    nHits = 0;
    TDCCh.clear();
    TDCTS.clear();
    TDCSS.clear();
}

void tcpClient::ReadError(QAbstractSocket::SocketError)
{
    m_tcpSocket->disconnectFromHost();
    std::cout<<"[TCPClientError]: Failded to connect sever because " << m_tcpSocket->errorString().toStdString() << std::endl;
    m_DebugTxt.close();
}

void tcpClient::RelaySendProcessedData(PROCESSEDData *processedData, int EventStartNo, int EventEndNo)
{
    emit SendProcessedData(processedData, EventStartNo, EventEndNo);
}

void tcpClient::RelayContext(QString context)
{
    emit SendContext(context);
}

void tcpClient::StopAndSaved()
{
    m_tcpSocket->abort();
    for(uint i=0; i<TDCData.EventList->size(); i++)
    {
        EventCount  = TDCData.EventList->at(i);
        nHits       = TDCData.NHitsList->at(i);
        TDCCh       = TDCData.ChannelList->at(i);
        TDCTS       = TDCData.TimeStampList->at(i);
        TDCSS       = TDCData.StyleStampList->at(i);

        RAWDataTree->Fill();
    }
    outputFile = RAWDataTree->GetCurrentFile();
    outputFile->Write(nullptr, TObject::kWriteDelete);
    MSG_INFO("Data saved in rootfile\n");
    TDCData.EventList->clear();
    TDCData.NHitsList->clear();
    TDCData.ChannelList->clear();
    TDCData.TimeStampList->clear();
    TDCData.StyleStampList->clear();
    emit ReceiveFinished();
}

void tcpClient::ReceiveData()
{
    QFile file("tdcData.txt");
    int tmpLineNo = 0;
    QByteArray line;
    QString strLine;
    if(!file.open(QIODevice::ReadOnly)){
        qDebug()<<("Error in opening \n");
    }
    while(!file.atEnd()){
        line = file.readLine();
        tmpLineNo++;
        strLine = QString(line);
        if(strLine.left(8) == "38000000" && tmpLineNo > lineNo){
//            qDebug()<<(strLine.left(strLine.length()-1));
            lineNo = tmpLineNo;
            break;
        }
    }
    QByteArray buffer = m_tcpSocket->readAll();
    buffer = QByteArray::fromHex(line);
//    qDebug()<<buffer;
    uint tempCount = m_TriggerCount;
    clearTempVar();
    if(DecodeData(buffer, TDCData)){
        EventCount++;
        TDCData.EventList->push_back(EventCount);
        TDCData.NHitsList->push_back(nHits);
        TDCData.ChannelList->push_back(TDCCh);
        TDCData.StyleStampList->push_back(TDCSS);
        TDCData.TimeStampList->push_back(TDCTS);
    }

    if(TDCData.EventList->size() != tempCount){
        qDebug() << *TDCData.EventList << tempCount;
        m_TriggerCount = TDCData.EventList->size();
        MSG_INFO("[DAQ]: %d / %d taken\n", m_TriggerCount, m_MaxTriggers);
        m_HitRecon->process(&TDCData, tempCount, m_TriggerCount);
        // after m_HitRecon->process, the m_HitRecon.SendProcessedData will be sent which is connected with
        if(m_TriggerCount > m_MaxTriggers){
            StopAndSaved();
        }
    }
    else{
        MSG_INFO(".");
    }
}
