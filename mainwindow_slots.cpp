#include "mainwindow.h"
#include "ui_mainwindow.h"

void MainWindow::on_B_RunDAQ_clicked()
{
    if(m_runFlag == false)
    {
        if(m_firstRunFlag == false) //if not the first-time-run, reset the display and DR, hang off the VME
        {
            DR->Update();
            resetDisplay(LAYERNOS);
            resetDataReader();
            if(DR->VME->CheckIRQ()){
                DR->VME->SendBUSY(OFF);
                usleep(2000);
            }
        }
        ui->importButton->setEnabled(false);
        emit SendRunDAQ(m_firstRunFlag);
        m_runFlag = true;
        ui->B_RunDAQ->setText("Stop&save");
        time->start();
        m_timer->start(refreshInterval);
    }
    else
    {
        emit SendStopAndSave();
        ui->B_RunDAQ->setText("Reset and DAQ");
    }
}

void MainWindow::on_importButton_clicked()
{
    if(m_firstImportFlag == false){
        m_timer->stop();
    }
    QString fileName = QFileDialog::getOpenFileName(this, tr("Choose the root file you want to import"), "./datarun",  tr("ROOTfile(*.root)"));
    if(fileName.isEmpty() == false)
    {
        resetDisplay(LAYERNOS);
        ui->B_RunDAQ->setEnabled(false);
        emit SendImportFile(fileName);
        DR->outputFileName = fileName.toStdString();
        //m_importFlag = true;
        m_firstImportFlag = false;
        time->start();
        m_timer->start(refreshInterval);
        ui->importButton->setText("ReImport");
        ui->B_Pause->setEnabled(true);
        ui->importButton->setEnabled(true);
    }
}

void MainWindow::on_B_Pause_clicked()
{
    if(m_timer->isActive())
        m_timer->stop();
    else{
        m_timer->start(refreshInterval);
    }
}
void MainWindow::on_RefreshInterval_textEdited(const QString &arg1)
{
    refreshInterval =  arg1.toInt();
    m_timer->start(refreshInterval);
}

void MainWindow::on_B_SaveChamberSetting_clicked()
{
    ui->L0_TB_MSG->clear();
    PHITDCMAP.clear();
    ETATDCMAP.clear();
    PHISTRIPMAP.clear();
    ETASTRIPMAP.clear();
    LAYERNOS = size_t(ui->SB_LayerNOs->value());
    PHISTRIPNOS = size_t(ui->SB_PhiStripNOs->value());
    ETASTRIPNOS = size_t(ui->SB_EtaStripNOs->value());
    PHITDCMAP.resize(LAYERNOS);
    ETATDCMAP.resize(LAYERNOS);
    PHISTRIPMAP.resize(LAYERNOS);
    ETASTRIPMAP.resize(LAYERNOS);

    PHISTRIPWIDTH.resize(LAYERNOS);
    ETASTRIPWIDTH.resize(LAYERNOS);
    PHIGAPWIDTH.resize(LAYERNOS);
    ETAGAPWIDTH.resize(LAYERNOS);

    MSG_INFO("[MAP]:Total layer:%d, Phi strips:%d, Eta strips:%d\n", int(LAYERNOS), int(PHISTRIPNOS), int(ETASTRIPNOS));
    ui->B_AddMapSetting->setEnabled(true);
}
void MainWindow::on_B_AddMapSetting_clicked()
{
    MAP_ADD_STATUS = false;
    int size = 0;
    size_t layer = size_t(ui->SB_LayerNO->value());
    QString phioreta = ui->CB_PhiOrEta->currentText();
    if(phioreta == "Phi")
    {
        if( ui->SB_StripED->value() - ui->SB_StripST->value() + 1 <= ui->SB_PhiStripNOs->value())
            MAP_ADD_STATUS = true;
        else
            MSG_INFO("[MAP]:Error!Phi channels over range\n");
    }
    else if(phioreta == "Eta")
    {
        if( ui->SB_StripED->value() - ui->SB_StripST->value() + 1 <= ui->SB_EtaStripNOs->value())
            MAP_ADD_STATUS = true;
        else
            MSG_INFO("[MAP]:Error!Eta channels over range\n");
    }
    else
        MAP_ADD_STATUS = false;

    if(MAP_ADD_STATUS)
    {
        if(phioreta == "Phi")
        {
            for(int i = ui->SB_StripST->value(); i <= ui->SB_StripED->value(); i++)
            {
                PHITDCMAP.at(layer).insert(pair<int, int>(ui->SB_TDCStCh->value()+size, i));
                PHISTRIPMAP.at(layer).insert(pair<int, int>(i, ui->SB_TDCStCh->value()+size));

                PHISTRIPWIDTH.at(layer)= ui->DSB_StripWidth->value();
                PHIGAPWIDTH.at(layer)= ui->DSB_GapWidth->value();
                size ++;
            }
            qDebug()<<"Layer"<<layer << " Phi:";
            qDebug()<<PHITDCMAP.at(layer);
        }
        else if(phioreta == "Eta")
        {
            for(int i = ui->SB_StripST->value(); i <= ui->SB_StripED->value(); i++)
            {
                ETATDCMAP.at(layer).insert(pair<int, int>(ui->SB_TDCStCh->value()+size, i));
                ETASTRIPMAP.at(layer).insert(pair<int, int>(i, ui->SB_TDCStCh->value()+size));

                ETASTRIPWIDTH.at(layer)= ui->DSB_StripWidth->value();
                ETAGAPWIDTH.at(layer)= ui->DSB_GapWidth->value();
                size ++;
            }
            qDebug()<<"Layer"<<layer << " Eta:";
            qDebug()<<ETATDCMAP.at(layer);
        }
        MSG_INFO("[MAP]:Layer[%zu]: %s", layer, ui->CB_PhiOrEta->currentText().toStdString().c_str());
        MSG_INFO("Panel: %2d:%2d -> TDC:%2d:%2d; strip width = %.1f, gap width = %.1f\n", ui->SB_StripST->value(), ui->SB_StripED->value(),
                 ui->SB_TDCStCh->value(), ui->SB_TDCStCh->value() + size -1, ui->DSB_StripWidth->value(), ui->DSB_GapWidth->value());
    }
    ui->B_SaveMapSetting->setEnabled(true);
}


void MainWindow::on_B_SaveMapSetting_clicked()
{
    /*for(size_t i = 0; i<LAYERNOS; i++)
    {
        Phi_st_list[i] = (PHITDCMAP[i].begin()->second);
        Phi_ed_list[i] = (PHITDCMAP[i].begin()->second + PHISTRIPNOS - 1);
        Eta_st_list[i] = (ETATDCMAP[i].begin()->second);
        Eta_ed_list[i] = (ETATDCMAP[i].begin()->second + ETASTRIPNOS - 1);
    }*/


    size_t flag = 0;
    for(size_t i = 0; i < LAYERNOS; i++)
    {
        if(PHITDCMAP.at(i).size() == PHISTRIPNOS && ETATDCMAP.at(i).size() == ETASTRIPNOS)
        {
            flag ++;
        }
        else
            MSG_INFO("[MAP]:Error! Layer[%zu] setting not completed.\n", i);
    }

    if(flag == LAYERNOS)
    {
        MSG_INFO("[MAP]:setting complete.\n");
        ui->B_RunDAQ->setEnabled(true);
        ui->importButton->setEnabled(true);
    }
    ETASTARTSTRIPNO = 1;
    PHISTARTSTRIPNO = 1;
    layerInit(LAYERNOS);
}


void MainWindow::on_CB_PresetMapping_currentIndexChanged(int index)
{
    ui->L0_TB_MSG->clear();
    PHITDCMAP.clear();
    ETATDCMAP.clear();
    PHISTRIPMAP.clear();
    ETASTRIPMAP.clear();
    /*if(index < 4){
        LAYERNOS = size_t(index);
        PHISTRIPNOS = 16;
        ETASTRIPNOS = 16;
        PHITDCMAP.resize(LAYERNOS);
        ETATDCMAP.resize(LAYERNOS);
        PHISTRIPMAP.resize(LAYERNOS);
        ETASTRIPMAP.resize(LAYERNOS);
        PHISTRIPWIDTH.resize(LAYERNOS);
        ETASTRIPWIDTH.resize(LAYERNOS);
        PHIGAPWIDTH.resize(LAYERNOS);
        ETAGAPWIDTH.resize(LAYERNOS);

        for(uint layer = 0; layer < index; layer++) //index from 1 to 3, corrosponding to 1-3 RPC singlets.
        {
            uint size = 0;
            for(int i = ui->SB_StripST->value(); i <= ui->SB_StripED->value(); i++)
            {
                PHITDCMAP.at(layer).insert(pair<int, int>(Phi_st_list[layer]+size, i+1));
                PHISTRIPMAP.at(layer).insert(pair<int, int>(i+1, Phi_st_list[layer]+size));
                PHISTRIPWIDTH.at(layer)= 25.0;
                PHIGAPWIDTH.at(layer)= 2.0;
                size ++;
            }

            size = 0;
            for(int i = ui->SB_StripST->value(); i <= ui->SB_StripED->value(); i++)
            {
                ETATDCMAP.at(layer).insert(pair<int, int>(Eta_st_list[layer]+size, i+1));
                ETASTRIPMAP.at(layer).insert(pair<int, int>(i+1, Eta_st_list[layer]+size));
                ETASTRIPWIDTH.at(layer)= 25.0;
                ETAGAPWIDTH.at(layer)= 2.0;
                size ++;
            }
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Phi");
            MSG_INFO("Panel: %2d:%2d -> TDC:%2lu:%2lu; Strip width = %.1f, gap width = %.1f\n",0, 15, Phi_st_list[layer], Phi_ed_list[layer], PHISTRIPWIDTH.at(layer), PHIGAPWIDTH.at(layer));
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Eta");
            MSG_INFO("Panel: %2d:%2d -> TDC:%2lu:%2lu; Strip width = %.1f, gap width = %.1f\n",0, 15, Eta_st_list[layer], Eta_ed_list[layer], ETASTRIPWIDTH.at(layer), ETAGAPWIDTH.at(layer));
        }
    }*/
    if(index == 1){
        LAYERNOS = 2;
        PHISTRIPNOS = 32;
        ETASTRIPNOS = 32;
        PHITDCMAP.resize(LAYERNOS);
        ETATDCMAP.resize(LAYERNOS);
        PHISTRIPMAP.resize(LAYERNOS);
        ETASTRIPMAP.resize(LAYERNOS);
        PHISTRIPWIDTH.resize(LAYERNOS);
        ETASTRIPWIDTH.resize(LAYERNOS);
        PHIGAPWIDTH.resize(LAYERNOS);
        ETAGAPWIDTH.resize(LAYERNOS);
        int eta_st_list[2] = {0, 32};
        int eta_ed_list[2] = {31, 63};
        int phi_st_list[2] = {64, 96};
        int phi_ed_list[2] = {95, 127};
        for(uint layer = 0; layer < LAYERNOS; layer++)
        {
            uint size = 0;
            for(int i = 0; i < 32; i++)
            {
                PHITDCMAP.at(layer).insert(pair<int, int>(phi_st_list[layer]+size, i+1));
                PHISTRIPMAP.at(layer).insert(pair<int, int>(i+1, phi_st_list[layer]+size));
                PHISTRIPWIDTH.at(layer)= 25.0;
                PHIGAPWIDTH.at(layer)= 2.0;
                size ++;
            }

            size = 0;
            for(int i = 0; i < 32; i++)
            {
                ETATDCMAP.at(layer).insert(pair<int, int>(eta_st_list[layer]+size, i+1));
                ETASTRIPMAP.at(layer).insert(pair<int, int>(i+1, eta_st_list[layer]+size));
                ETASTRIPWIDTH.at(layer)= 25.0;
                ETAGAPWIDTH.at(layer)= 2.0;
                size ++;
            }
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Phi");
            MSG_INFO("Panel: %2d:%2d -> TDC:%2d:%2d; Strip width = %.1f, gap width = %.1f\n",1, 32, phi_st_list[layer], phi_ed_list[layer], PHISTRIPWIDTH.at(layer), PHIGAPWIDTH.at(layer));
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Eta");
            MSG_INFO("Panel: %2d:%2d -> TDC:%2d:%2d; Strip width = %.1f, gap width = %.1f\n",1, 32, eta_st_list[layer], eta_ed_list[layer], ETASTRIPWIDTH.at(layer), ETAGAPWIDTH.at(layer));
        }
    }
    if(index == 2){
        LAYERNOS = 3;
        PHISTRIPNOS = 16;
        ETASTRIPNOS = 24;
        PHITDCMAP.resize(LAYERNOS);
        ETATDCMAP.resize(LAYERNOS);
        PHISTRIPMAP.resize(LAYERNOS);
        ETASTRIPMAP.resize(LAYERNOS);
        PHISTRIPWIDTH.resize(LAYERNOS);
        ETASTRIPWIDTH.resize(LAYERNOS);
        PHIGAPWIDTH.resize(LAYERNOS);
        ETAGAPWIDTH.resize(LAYERNOS);
        int eta_st_list[3] = {0, 24, 48};
        int eta_ed_list[3] = {23, 47, 79};
        int phi_st_list[3] = {80, 96, 112};
        int phi_ed_list[3] = {95, 111, 127};
        for(uint layer = 0; layer < LAYERNOS; layer++)
        {
            uint size = 0;
            for(int i = 0; i < PHISTRIPNOS; i++)
            {
                PHITDCMAP.at(layer).insert(pair<int, int>(phi_st_list[layer]+size, i+1));
                PHISTRIPMAP.at(layer).insert(pair<int, int>(i+1, phi_st_list[layer]+size));
                PHISTRIPWIDTH.at(layer)= 25.0;
                PHIGAPWIDTH.at(layer)= 2.0;
                size ++;
            }

            size = 0;
            for(int i = 0; i < ETASTRIPNOS; i++)
            {
                ETATDCMAP.at(layer).insert(pair<int, int>(eta_st_list[layer]+size, i+1));
                ETASTRIPMAP.at(layer).insert(pair<int, int>(i+1, eta_st_list[layer]+size));
                ETASTRIPWIDTH.at(layer)= 25.0;
                ETAGAPWIDTH.at(layer)= 2.0;
                size ++;
            }
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Eta");
            MSG_INFO("Panel: %2d:%2d -> TDC:%2d:%2d; Strip width = %.1f, gap width = %.1f\n",1, ETASTRIPNOS, eta_st_list[layer], eta_ed_list[layer], ETASTRIPWIDTH.at(layer), ETAGAPWIDTH.at(layer));
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Phi");
            MSG_INFO("Panel: %2d:%2d -> TDC:%2d:%2d; Strip width = %.1f, gap width = %.1f\n",1, PHISTRIPNOS, phi_st_list[layer], phi_ed_list[layer], PHISTRIPWIDTH.at(layer), PHIGAPWIDTH.at(layer));
        }

    }

    if(index == 3){
        LAYERNOS = 3;
        PHISTRIPNOS = 16;
        ETASTRIPNOS = 24;
        PHITDCMAP.resize(LAYERNOS);
        ETATDCMAP.resize(LAYERNOS);
        PHISTRIPMAP.resize(LAYERNOS);
        ETASTRIPMAP.resize(LAYERNOS);
        PHISTRIPWIDTH.resize(LAYERNOS);
        ETASTRIPWIDTH.resize(LAYERNOS);
        PHIGAPWIDTH.resize(LAYERNOS);
        ETAGAPWIDTH.resize(LAYERNOS);
        int eta_st_list[3] = {0, 24, 48};
        int eta_ed_list[3] = {23, 47, 79};
        int phi_st_list[3] = {80, 96, 112};
        int phi_ed_list[3] = {95, 111, 127};
        for(uint layer = 0; layer < LAYERNOS; layer++)
        {
            uint size = 0;
            for(int i = 0; i < PHISTRIPNOS; i++)
            {
                PHITDCMAP.at(layer).insert(pair<int, int>(phi_st_list[layer]+size, i+1));
                PHISTRIPMAP.at(layer).insert(pair<int, int>(i+1, phi_st_list[layer]+size));
                PHISTRIPWIDTH.at(layer)= 25.0;
                PHIGAPWIDTH.at(layer)= 2.0;
                size ++;
            }

            size = 0;
            if(layer == 2){
                for(int i = 0; i < 8; i++){
                    ETATDCMAP.at(layer).insert(pair<int, int>(eta_st_list[layer]+size, i+1));
                    ETASTRIPMAP.at(layer).insert(pair<int, int>(i+1, eta_st_list[layer]+size));
                    ETASTRIPWIDTH.at(layer)= 25.0;
                    ETAGAPWIDTH.at(layer)= 2.0;
                    size ++;
                }
                size+=8;//Channel 56-63 has to be skipped to due to broken cable
                for(int i = 8; i < ETASTRIPNOS; i++){
                    ETATDCMAP.at(layer).insert(pair<int, int>(eta_st_list[layer]+size, i+1));
                    ETASTRIPMAP.at(layer).insert(pair<int, int>(i+1, eta_st_list[layer]+size));
                    ETASTRIPWIDTH.at(layer)= 25.0;
                    ETAGAPWIDTH.at(layer)= 2.0;
                    size ++;
                }
            }
            else{
                for(int i = 0; i < ETASTRIPNOS; i++)
                {
                    ETATDCMAP.at(layer).insert(pair<int, int>(eta_st_list[layer]+size, i+1));
                    ETASTRIPMAP.at(layer).insert(pair<int, int>(i+1, eta_st_list[layer]+size));
                    ETASTRIPWIDTH.at(layer)= 25.0;
                    ETAGAPWIDTH.at(layer)= 2.0;
                    size ++;
                }
            }
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Eta");
            MSG_INFO("Panel: %2d:%2d -> TDC:%2d:%2d; Strip width = %.1f, gap width = %.1f\n",1, ETASTRIPNOS, eta_st_list[layer], eta_ed_list[layer], ETASTRIPWIDTH.at(layer), ETAGAPWIDTH.at(layer));
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Phi");
            MSG_INFO("Panel: %2d:%2d -> TDC:%2d:%2d; Strip width = %.1f, gap width = %.1f\n",1, PHISTRIPNOS, phi_st_list[layer], phi_ed_list[layer], PHISTRIPWIDTH.at(layer), PHIGAPWIDTH.at(layer));
        }
    }
    for(uint l = 0; l < LAYERNOS; l++){
//        qDebug()<<"Layer" << l << " ETATDCMAP: " << ETATDCMAP[l];
//        qDebug()<<"Layer" << l << " PHITDCMAP: " << PHITDCMAP[l];
    }
    ETASTARTSTRIPNO = 1;
    PHISTARTSTRIPNO = 1;
    layerInit(LAYERNOS);


    ui->B_RunDAQ->setEnabled(true);
    ui->importButton->setEnabled(true);
}

void MainWindow::on_SB_LESt_valueChanged(int arg1)
{
    LeadginEdgeSt = double(arg1);
    MSG_INFO("[MAP]:LeadginEdge start @ %f\n", LeadginEdgeSt);
    qDebug() << "LeadginEdge start @ " << LeadginEdgeSt;
    //QString s = QString::number(LeadingEdgeEd);
    //s += " ns";
    //ui->L_LEEd->setText(s);
}

void MainWindow::on_SB_LEEd_valueChanged(int arg1)
{
    LeadingEdgeEd = double(arg1);
    MSG_INFO("[MAP]:LeadginEdge end @ %f\n", LeadingEdgeEd);
    qDebug() << "LeadginEdge end @ " << LeadingEdgeEd;
}


void MainWindow::on_CB_EventsOnceImport_currentTextChanged(const QString &arg1)
{
    EventNOOnceRead = uint(arg1.toInt());
    MSG_INFO("[DAQ]:%u events would be imported during each refresh\n", EventNOOnceRead);
}

void MainWindow::on_CB_LEFEFlag_currentIndexChanged(int index)
{
    if(index == 0)
    {
        LEFlag = 1;
        FEFlag = 0;
    }
    else if(index == 1)
    {
        LEFlag = 0;
        FEFlag = 1;
    }
}

void MainWindow::on_B_CSV_import_clicked()
{
    ui->B_CSV_import->setEnabled(false);
    ui->CB_PresetMapping->setEnabled(false);
    ui->L0_TB_MSG->clear();
    PHITDCMAP.clear();
    ETATDCMAP.clear();
    PHISTRIPMAP.clear();
    ETASTRIPMAP.clear();
    int ChPerConneter = 8;
    QString fileName;
    if(m_enslavedMode == false){
        QFileDialog* fd = new QFileDialog(this);
        fileName = fd->getOpenFileName(this,tr("Open File"),"./Mapping",tr("Excel(*.csv)"));
    }
    else{
        fileName= "default.csv";
    }
    CSVFileName = fileName;
    if(fileName.isEmpty() == false){
        QDir dir = QDir("./Mapping");
        QFile file(dir.filePath(fileName));
        if(!file.open(QIODevice::ReadOnly)){
             MSG_INFO("[MAPPING_IMPORT]: OPEN FILE FAILED\n");
             std::cout << "[Error]: ! cannot open the CSV mapping file!" << std::endl;
        }
        QTextStream * rawContent = new QTextStream(&file);
        splitContent.clear();
        splitContent = rawContent->readAll().split("\n");

        for(int i = 0; i < splitContent.count() ; i++)
        {
             QStringList lineContent = splitContent.at(i).split(",");
             if(lineContent.value(0) == "#"){
                 continue;
             }
             if(lineContent.value(0) == "ChamberConfig"){
                 ChPerConneter = lineContent.value(6).toInt();
                 LAYERNOS = lineContent.value(1).toUInt();
                 ETASTRIPNOS = lineContent.value(2).toUInt();
                 ETASTARTSTRIPNO = (lineContent.value(3).toUInt()-1) * uint(ChPerConneter) + 1; //Recorded in FE NO in the csv file, which starts from 1, stands for the number of the 8-channel-connector.
                 PHISTRIPNOS = lineContent.value(4).toUInt();
                 PHISTARTSTRIPNO = (lineContent.value(5).toUInt()-1) * uint(ChPerConneter) + 1;

                 PHITDCMAP.resize(LAYERNOS);
                 ETATDCMAP.resize(LAYERNOS);
                 PHISTRIPMAP.resize(LAYERNOS);
                 ETASTRIPMAP.resize(LAYERNOS);
                 PHISTRIPWIDTH.resize(LAYERNOS);
                 ETASTRIPWIDTH.resize(LAYERNOS);
                 PHIGAPWIDTH.resize(LAYERNOS);
                 ETAGAPWIDTH.resize(LAYERNOS);
                 continue;
             }
             if(lineContent.value(0) == "SectionConfig"){
                 int TDCStCh = lineContent.value(1).toInt();
                 uint layer = lineContent.value(4).toUInt();
                 int localStStripNo = (lineContent.value(3).toInt() - 1) * 8 + 1;
                 double stripWidth = lineContent.value(6).toDouble();
                 double stripGap = lineContent.value(7).toDouble();
                 if(lineContent.value(5) == "ETA"){
                     for(int i = 0; i < ChPerConneter; i++){
                         //Map is initialized 8(or customized) strips/channels once, as a basic unit
                         ETATDCMAP.at(layer).insert(pair<int, int>(TDCStCh + i, i + localStStripNo)); //Stored in the map is the biased strip NO, i.e. starting from 1
                         ETASTRIPMAP.at(layer).insert(pair<int, int>(i + localStStripNo, TDCStCh+i));
                         ETASTRIPWIDTH.at(layer)= stripWidth;
                         ETAGAPWIDTH.at(layer)= stripGap;
                     }
                 }
                 else if(lineContent.value(5) == "PHI"){
                     for(int i = 0; i < ChPerConneter; i++){
                         PHITDCMAP.at(layer).insert(pair<int, int>(TDCStCh+i, i + localStStripNo));
                         PHISTRIPMAP.at(layer).insert(pair<int, int>(i + localStStripNo, TDCStCh+i));
                         PHISTRIPWIDTH.at(layer)= stripWidth;
                         PHIGAPWIDTH.at(layer)= stripGap;
                     }
                 }
                 else{
                     MSG_INFO("[MAPPING_IMPORT]: ETA/PHI error in line %d\n", i);
                 }
             }
             if(lineContent.value(0) == "ReferenceConfig"){
                 LAYERREFERENCE[0] = lineContent.value(1).toInt();
                 LAYERREFERENCE[1] = lineContent.value(2).toInt();
                 LAYERREFERENCE[2] = lineContent.value(3).toInt();
             }
        }
        for(uint layer = 0; layer < LAYERNOS; layer++){
            std::map<size_t, size_t>::iterator ETATDCed_it = ETASTRIPMAP.at(layer).find(ETASTARTSTRIPNO + ETASTRIPNOS - 1);
            std::map<size_t, size_t>::iterator PHITDCed_it = PHISTRIPMAP.at(layer).find(PHISTARTSTRIPNO + PHISTRIPNOS - 1);
            std::map<size_t, size_t>::iterator ETATDCst_it = ETASTRIPMAP.at(layer).find(ETASTARTSTRIPNO);
            std::map<size_t, size_t>::iterator PHITDCst_it = PHISTRIPMAP.at(layer).find(PHISTARTSTRIPNO);
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Eta");
            MSG_INFO("Panel: %2zu:%2lu -> TDC:%2d:%2d; Strip width = %.1f, gap width = %.1f\n", ETASTARTSTRIPNO, ETASTARTSTRIPNO + ETASTRIPNOS-1, (*ETATDCst_it).second, (*ETATDCed_it).second, ETASTRIPWIDTH.at(layer), ETAGAPWIDTH.at(layer));
            MSG_INFO("[MAP]:Layer[%u]: %s", layer, "Phi");
            MSG_INFO("Panel: %2zu:%2lu -> TDC:%2d:%2d; Strip width = %.1f, gap width = %.1f\n",PHISTARTSTRIPNO, PHISTARTSTRIPNO + PHISTRIPNOS-1, (*PHITDCst_it).second, (*PHITDCed_it).second, PHISTRIPWIDTH.at(layer), PHIGAPWIDTH.at(layer));
//            qDebug()<<"Layer" << layer << " ETATDCMAP: " << ETATDCMAP[layer];
//            qDebug()<<"Layer" << layer << " PHITDCMAP: " << PHITDCMAP[layer];
        }
        if(m_firstInitFlag){
            layerInit(LAYERNOS);
            m_firstInitFlag = false;
        }
        else{
            resetDisplay(LAYERNOS);
        }
        ui->B_RunDAQ->setEnabled(true);
        ui->importButton->setEnabled(true);
    }
    else{
        ui->B_CSV_import->setEnabled(true);
        MSG_INFO("[MAPPING_IMPORT]: File not found\n");
    }
}
